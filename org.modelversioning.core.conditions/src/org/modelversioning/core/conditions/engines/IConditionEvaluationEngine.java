/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines;

import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.ocl.helper.Choice;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.EvaluationResult;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.Template;

/**
 * Engine to validate and evaluate {@link Template}s and their {@link Condition}
 * s within a {@link ConditionsModel}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IConditionEvaluationEngine {

	/**
	 * Specifies whether this engine should works type-aware, i.e., that an
	 * eObject only evaluates positively if it has the same type (eClass) like
	 * the respresentative of the evaluated template.
	 * 
	 * @return whether this engine works type-aware.
	 */
	public boolean isTypeAware();

	/**
	 * Sets whether this engine should works type-aware, i.e., that an eObject
	 * only evaluates positively if it has the same type (eClass) like the
	 * respresentative of the evaluated template.
	 * 
	 * @param isTypeAware
	 *            to set.
	 */
	public void setTypeAware(boolean isTypeAware);

	/**
	 * Specifies whether this engine should work condition-aware, i.e., that an
	 * eObject only matches if the container of the object matches the specified
	 * parent template in the template hierarchy. This only affects "global"
	 * evaluations (when localOnly is false).
	 * 
	 * @return whether this engine works containment-aware.
	 */
	boolean isContainmentAware();

	/**
	 * Sets whether this engine should work condition-aware, i.e., that an
	 * eObject only matches if the container of the object matches the specified
	 * parent template in the template hierarchy. This only affects "global"
	 * evaluations (when localOnly is false).
	 * 
	 * @param isContainmentAware
	 *            to set.
	 */
	void setContainmentAware(boolean isContainmentAware);

	/**
	 * Returns the condition language this generator will use to express
	 * conditions.
	 * 
	 * @return condition language this generator will use to express conditions.
	 */
	public String getConditionLanguage();

	/**
	 * Sets the current default conditions model to evaluate.
	 * 
	 * @param conditionsModel
	 *            the conditions model to check.
	 * @throws UnsupportedConditionLanguage
	 *             if condition language is not supported by the engine.
	 */
	public void setConditionsModel(ConditionsModel conditionsModel)
			throws UnsupportedConditionLanguage;

	/**
	 * Returns the currently set default conditions model.
	 * 
	 * @return the currently set default conditions model.
	 */
	public ConditionsModel getConditionsModel();

	/**
	 * Registers a related {@link ITemplateBinding} to resolve values of related
	 * models for the specified <code>prefix</code>. This is needed, if a
	 * condition refers to {@link Template}s of another {@link ITemplateBinding}
	 * using the following syntax:
	 * 
	 * <pre>
	 * self.name = #{prefix:Class_1}.name
	 * </pre>
	 * 
	 * <p>
	 * Note that only simple values are supported (like Strings, Integers,
	 * etc.).
	 * </p>
	 * 
	 * @param prefix
	 *            to use.
	 * @param iTemplateBindings
	 *            to use to resolve values.
	 */
	public void registerRelatedTemplateBinding(String prefix,
			ITemplateBindings iTemplateBindings);

	/**
	 * Unregisters a previously registered {@link ITemplateBinding}. See
	 * {@link #registerRelatedConditionsModel(String, ITemplateBinding)} .
	 * 
	 * @param prefix
	 *            to unregister.
	 */
	public void unregisterRelatedTemplateBinding(String prefix);

	/**
	 * Evaluates the specified <code>condition</code> as part of the currently
	 * set conditions model ({@link #setCurrentRootTemplate(Template)}) for the
	 * specified <code>eObject</code>.
	 * 
	 * <p>
	 * Note that, if the specified <code>condition</code> involves templates (
	 * {@link Condition#isInvolvesTemplate()}) the evaluation might be
	 * expensive. For each referenced template this method tries to find a (only
	 * locally) valid candidate.
	 * </p>
	 * 
	 * @param condition
	 *            the condition to evaluate.
	 * @param eObject
	 *            the {@link EObject} to evaluate.
	 * @return the result of the check.
	 */
	public EvaluationResult evaluate(Condition condition, EObject eObject);

	/**
	 * Evaluates the specified <code>template</code> as part of the currently
	 * set conditions model ({@link #setCurrentRootTemplate(Template)}) is
	 * satisfied for the specified <code>eObject</code>.
	 * 
	 * <p>
	 * Note that, if <code>localOnly</code> is <code>false</code> this method
	 * has to try to find a complete template binding to truly evaluate the
	 * <code>eObject</code> which might be rather expensive. Consider to call
	 * {@link #findTemplateBinding(Template, EObject)} directly and check
	 * {@link ITemplateBindings#validate()} which is basically the same but you
	 * already have your {@link ITemplateBindings} at hand if you need it later
	 * on.
	 * </p>
	 * 
	 * @param template
	 *            the {@link Template} to check.
	 * @param eObject
	 *            the {@link EObject} to check.
	 * @param localOnly
	 *            specifies whether to evaluate only local conditions or to
	 *            include conditions involving templates
	 *            {@link Condition#isInvolvesTemplate()}) for evaluation.
	 * @return the result of the check.
	 */
	public EvaluationResult evaluate(Template template, EObject eObject,
			boolean localOnly);

	/**
	 * Checks if the specified <code>condition</code> is satisfied with
	 * <code>eObject</code> using the specified <code>candidateMap</code>.
	 * 
	 * @param condition
	 *            condition to check.
	 * @param eObject
	 *            {@link EObject} to check.
	 * @param binding
	 *            binding to use to evaluate the <code>condition</code>.
	 * @return the result of the check.
	 */
	public EvaluationResult evaluate(Condition condition, EObject eObject,
			ITemplateBinding binding);

	/**
	 * Tries to find a suitable value for the {@link EStructuralFeature} for the
	 * specified <code>eObject</code> to satisfy the specified
	 * <code>condition</code>.
	 * 
	 * @param condition
	 *            condition to get valid value for.
	 * @param eObject
	 *            {@link EObject} to use for finding a suitable value.
	 * @param binding
	 *            binding to use to evaluate the <code>condition</code>.
	 * @return the suitable value or <code>null</code>.
	 */
	public Object getValidValue(FeatureCondition condition, EObject eObject,
			ITemplateBinding binding);

	/**
	 * Tries to find a valid template binding for the specified
	 * <code>eObject</code> as candidate object for the specified
	 * <code>template</code>.
	 * 
	 * <p>
	 * This method might be expensive. Use {@link ITemplateBindings#validate()}
	 * to find out, if it was possible to create a successful binding.
	 * </p>
	 * 
	 * @param template
	 *            template to bind to <code>eObject</code>.
	 * @param eObject
	 *            object to bind to <code>template</code>.
	 * @return a template binding.
	 */
	public ITemplateBindings findTemplateBinding(Template template,
			EObject eObject);

	/**
	 * Tries to find a valid template binding for the specified
	 * <code>preBinding</code>. The <code>preBinding</code> contains a map of
	 * {@link Template}s and the preliminary bindings to a {@link Set} of
	 * {@link EObject}s for each {@link Template}.
	 * 
	 * <p>
	 * This method might be expensive. Use {@link ITemplateBindings#validate()}
	 * to find out, if it was possible to create a successful binding.
	 * </p>
	 * 
	 * @param preBinding
	 *            the pre-binding to start with.
	 * @return a template binding.
	 */
	public ITemplateBindings findTemplateBinding(ITemplateBinding preBinding);

	/**
	 * Tries to find a valid template binding for the specified
	 * <code>preBinding</code>. The <code>preBinding</code> contains a map of
	 * {@link Template}s and the preliminary bindings to a {@link Set} of
	 * {@link EObject}s for each {@link Template}.
	 * 
	 * <p>
	 * This method might be expensive. Use {@link ITemplateBindings#validate()}
	 * to find out, if it was possible to create a successful binding.
	 * </p>
	 * 
	 * @param preBinding
	 *            the pre-binding to start with.
	 * @param reEvaluatePreBound
	 *            specifies whether to re-evaluate also pre-bound objects.
	 * @return a template binding.
	 */
	public ITemplateBindings findTemplateBinding(ITemplateBinding preBinding,
			boolean reEvaluatePreBound);

	/**
	 * Tries to find a valid template binding for the specified
	 * <code>preBinding</code>. The <code>preBinding</code> contains a map of
	 * {@link Template}s and the preliminary bindings to a {@link Set} of
	 * {@link EObject}s for each {@link Template}.
	 * 
	 * <p>
	 * This method might be expensive. Use {@link ITemplateBindings#validate()}
	 * to find out, if it was possible to create a successful binding.
	 * </p>
	 * 
	 * @param preBinding
	 *            the pre-binding to start with.
	 * @param candidateBinding
	 *            used to restrict candidate selection to this specified
	 *            binding. Might also be <code>null</code> or empty.
	 * @return a template binding.
	 */
	public ITemplateBindings findTemplateBinding(ITemplateBinding preBinding,
			ITemplateBinding candidateBinding);

	/**
	 * Returns the prefix resulting from the specified <code>condition</code>
	 * (such as for example &quot;self.feature&quot;).
	 * 
	 * This method is just for helping visualizing the complete condition in
	 * GUIs.
	 * 
	 * @param condition
	 *            to get prefix for.
	 * @return the prefix.
	 */
	public String getConditionPrefix(Condition condition);

	/**
	 * Validates the specified <code>template</code>.
	 * 
	 * <p>
	 * The validation result is represented by the returned {@link IStatus}.
	 * </p>
	 * 
	 * @param template
	 *            {@link Template} to validate.
	 * @return the result of the validation.
	 */
	public EvaluationResult validate(Template template);

	/**
	 * Validates the specified <code>condition</code>.
	 * 
	 * <p>
	 * The validation result is represented by the returned {@link IStatus}.
	 * </p>
	 * 
	 * @param condition
	 *            {@link Condition} to validate.
	 * @return the result of the validation.
	 */
	public EvaluationResult validate(Condition condition);

	/**
	 * Replaces template value references in the specified <code>string</code>
	 * with values according to the specified <code>binding</code>.
	 * 
	 * @param string
	 * @param templateBinding
	 * @return the string with replaced values.
	 */
	public String replaceTemplateValues(String string,
			ITemplateBindings templateBinding);

	/**
	 * Returns content proposals for the specified <code>content</code> at the
	 * specified <code>position</code> for the specified <code>condition</code>.
	 * 
	 * This method can be used to offer code completion when editing conditions.
	 * 
	 * @param contents
	 *            the current contents.
	 * @param position
	 *            the position.
	 * @param condition
	 *            the condition.
	 * @return content proposals.
	 */
	public Choice[] getContentProposals(String contents, int position,
			Condition condition);

	/**
	 * Returns <code>true</code> if specified <code>binding</code> is complete,
	 * i.e., every active template there is at least one bound object.
	 * 
	 * @param binding
	 *            to check.
	 * @return <code>true</code> if complete, otherwise <code>false</code>.
	 */
	boolean isComplete(ITemplateBinding binding);

}
