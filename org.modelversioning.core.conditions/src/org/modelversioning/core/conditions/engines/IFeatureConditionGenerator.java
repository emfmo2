/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines;

import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.Template;

/**
 * Generator to generate a {@link FeatureCondition} for a specified
 * {@link EStructuralFeature} and a corresponding value.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IFeatureConditionGenerator {

	/**
	 * String indicating the general use.
	 */
	public final static String GENERAL = "*";

	/**
	 * Returns the condition language this generator will use to express
	 * conditions.
	 * 
	 * @return condition language this generator will use to express conditions.
	 */
	public String getConditionLanguage();

	/**
	 * Returns the namespaces of the meta model this generator is specialized
	 * for. If this is a general purpose generator this method returns a list
	 * containing <code>{@value #GENERAL}</code> ({@link #GENERAL}).
	 * 
	 * @return namespace of the meta model this generator is specialized for.
	 */
	public List<String> getMetaModelNamespace();

	/**
	 * Generates {@link FeatureCondition}s for the specified
	 * <code>feature</code> and the specified <code>value</code>(s). If this
	 * generator decides to omit a condition for the specified
	 * <code>feature</code> this method returns an empty list.
	 * 
	 * @param feature
	 *            {@link EStructuralFeature} to create {@link FeatureCondition}
	 *            for.
	 * @param template
	 *            The {@link Template} for which the {@link FeatureCondition} is
	 *            generated. The {@link Template#getRepresentative()} must be
	 *            set.
	 * @return Generated {@link FeatureCondition} or <code>null</code>.
	 */
	public List<FeatureCondition> generateFeatureCondition(
			EStructuralFeature feature, Template template);

}
