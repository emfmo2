/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.CallOperationAction;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.SendSignalAction;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.ocl.helper.Choice;
import org.eclipse.ocl.helper.ChoiceKind;
import org.eclipse.ocl.helper.ConstraintKind;
import org.eclipse.ocl.helper.OCLHelper;
import org.eclipse.ocl.options.ParsingOptions;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsFactory;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.ConditionsPlugin;
import org.modelversioning.core.conditions.CustomCondition;
import org.modelversioning.core.conditions.EvaluationResult;
import org.modelversioning.core.conditions.EvaluationStatus;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.NonExistenceGroup;
import org.modelversioning.core.conditions.OptionGroup;
import org.modelversioning.core.conditions.State;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.TemplateMaskLiterals;
import org.modelversioning.core.conditions.engines.BindingException;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.ITemplateBindings;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.conditions.util.ConditionsUtil;
import org.modelversioning.core.util.EcoreUtil;

/**
 * Implements the {@link IConditionEvaluationEngine} using {@link OCL}.
 * 
 * This evaluation engine does not regard containment relationships as fix
 * constraints for template evaluation and binding creation. If this is wanted,
 * according conditions have to be added.
 * 
 * {@inheritDoc}
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ConditionsEvaluationEngineImpl implements
		IConditionEvaluationEngine {

	private static final String ERR_UNKOWN_COND_TYPE = "Unkown condition type: ";

	private static final String NO_COMMON_PARENT = "No common parent";

	protected static final String EVALUATOR = "OCL AMOR Condition Evaluator";

	/**
	 * The currently set {@link ConditionsModel}.
	 */
	private ConditionsModel conditionsModel = null;

	/**
	 * Map saving the {@link ITemplateBinding} of related
	 * {@link ConditionsModel}s.
	 */
	private Map<String, ITemplateBindings> relatedBindings = new HashMap<String, ITemplateBindings>();

	/**
	 * The OCL environment.
	 */
	private OCL<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction, Constraint, EClass, EObject> ocl = null;

	/**
	 * A map of {@link OCLHelper} instances. For each {@link Template} there is
	 * an own {@link OCLHelper} instance.
	 */
	private Map<Template, OCLHelper<EClassifier, EOperation, EStructuralFeature, Constraint>> oclHelperMap = new HashMap<Template, OCLHelper<EClassifier, EOperation, EStructuralFeature, Constraint>>();

	/**
	 * the binding creator to use.
	 */
	private TemplateBindingCreator templateBindingCreator;

	/**
	 * Saves the list of all templates of the currently set conditions model.
	 */
	private EList<Template> templates;

	/**
	 * Saves the list of all &quot;normal&quot; (i.e.,
	 * {@link Template#isActive() active}, {@link Template#isExistence()
	 * existence}, and {@link Template#isMandatory() mandatory} templates of the
	 * currently set conditions model.
	 */
	private EList<Template> normalTemplates;

	/**
	 * Saves the list of all explicit and implicit (child of non-existence)
	 * {@link Template#isExistence() non-existence} templates of the currently
	 * set conditions model.
	 */
	private EList<Template> nonExistenceTemplates;

	/**
	 * Saves the list of all explicit and implicit (child of non-existence)
	 * {@link Template#isMandatory() optional} templates of the currently set
	 * conditions model.
	 */
	private EList<Template> optionalTemplates;

	/**
	 * Current option group to evaluate.
	 */
	private OptionGroup currentOptionGroup = null;

	/**
	 * Current non existence group to evaluate.
	 */
	private NonExistenceGroup currentNonExistenceGroup = null;

	/**
	 * Activated option groups for evaluation.
	 */
	private EList<OptionGroup> activatedOptionGroups = new BasicEList<OptionGroup>();

	/**
	 * Specifies whether this engine should work type-aware, i.e., that an
	 * eObject only matches if it has the same type (eClass) like the
	 * respresentative of the evaluated template.
	 */
	private boolean isTypeAware = true;

	/**
	 * Specifies whether this engine should work containment-aware, i.e., that
	 * an eObject only matches if the container of the object matches the
	 * specified parent template in the template hierarchy. This only affects
	 * "global" evaluations (when localOnly is false). Consequently, it is only
	 * regarded by {@link TemplateBindingCreator} which handles global
	 * evaluation.
	 */
	private boolean isContainmentAware = true;

	/**
	 * Specifies whether this engine should throw an exception if a parser
	 * exception occurs (if this field is <code>true</code>) or to just return
	 * <code>false</code> (if this field is <code>false</code>).
	 */
	private boolean isThrowExceptionOnParserError = false;

	/**
	 * The current evaluation run mode.
	 */
	private EvaluationRunMode evaluationRunState = EvaluationRunMode.FIRST_RUN;

	/**
	 * Possible evaluation run modes.
	 * 
	 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
	 * 
	 */
	private enum EvaluationRunMode {
		ALL, FIRST_RUN, CHECK_NONEXISTENCE, CHECK_OPTIONAL;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return the isTypeAware
	 */
	@Override
	public boolean isTypeAware() {
		return isTypeAware;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param isTypeAware
	 *            the isTypeAware to set
	 */
	@Override
	public void setTypeAware(boolean isTypeAware) {
		this.isTypeAware = isTypeAware;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return the isContainmentAware
	 */
	@Override
	public boolean isContainmentAware() {
		return isContainmentAware;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param isContainmentAware
	 *            the isContainmentAware to set
	 */
	@Override
	public void setContainmentAware(boolean isContainmentAware) {
		this.isContainmentAware = isContainmentAware;
	}

	/**
	 * Specifies whether this engine throws an exception if a parser exception
	 * occurs (if this field is <code>true</code>) or to just return
	 * <code>false</code> for the failing condition (if this field is
	 * <code>false</code>).
	 * 
	 * @return <code>true</code> if throwing exception, <code>false</code>
	 *         otherwise.
	 */
	public boolean isThrowExceptionOnParserError() {
		return isThrowExceptionOnParserError;
	}

	/**
	 * Specifies whether this engine throws an exception if a parser exception
	 * occurs (if this field is <code>true</code>) or to just return
	 * <code>false</code> for the failing condition (if this field is
	 * <code>false</code>).
	 * 
	 * @param <code>true</code> if throwing exception, <code>false</code>
	 *        otherwise.
	 */
	public void setThrowExceptionOnParserError(
			boolean isThrowExceptionOnParserError) {
		this.isThrowExceptionOnParserError = isThrowExceptionOnParserError;
	}

	/**
	 * Returns the {@link OCL} instance for the specified <code>template</code>.
	 * 
	 * <p>
	 * This method will create an appropriate {@link OCL} instance if there
	 * currently is not existing one.
	 * </p>
	 * 
	 * @param template
	 *            {@link Template} for which the {@link OCL} instance is
	 *            requested.
	 * @return the {@link OCL} instance.
	 */
	protected OCL<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction, Constraint, EClass, EObject> getOclInstance(
			Template template) {
		if (ocl == null) {
			ocl = OCL.newInstance(EcoreEnvironmentFactory.INSTANCE);
			// set implicit root object option
			ParsingOptions.setOption(ocl.getEnvironment(),
					ParsingOptions.implicitRootClass(ocl.getEnvironment()),
					EcorePackage.Literals.EOBJECT);
		}
		return ocl;
	}

	/**
	 * Returns the {@link OCLHelper} for the specified <code>template</code>.
	 * 
	 * <p>
	 * This method will create an appropriate {@link OCLHelper} instance if
	 * there currently is not existing one.
	 * </p>
	 * 
	 * @param template
	 *            {@link Template} for which the {@link OCLHelper} instance is
	 *            requested.
	 * @return the {@link OCLHelper} instance.
	 */
	protected OCLHelper<EClassifier, EOperation, EStructuralFeature, Constraint> getOclHelperInstance(
			Template template) {
		if (!oclHelperMap.containsKey(template)) {
			OCLHelper<EClassifier, EOperation, EStructuralFeature, Constraint> helper = getOclInstance(
					template).createOCLHelper();
			helper.setContext(template.getRepresentative().eClass());
			oclHelperMap.put(template, helper);
		}
		return oclHelperMap.get(template);
	}

	/**
	 * Creates a {@link OCLExpression} for the specified {@link Condition}
	 * {@code condition}.
	 * 
	 * @param condition
	 *            the {@link Condition} to create a {@link OCLExpression} for.
	 * @param enforceTemplateName
	 *            specifies whether conditions that do not involve a
	 *            {@link Template} (see {@link Condition#isInvolvesTemplate()})
	 *            should use <code>self</code> or the Template name.
	 * @return the created {@link OCLExpression}.
	 * @throws ParserException
	 *             if created {@link OCLExpression} is invalid.
	 */
	private OCLExpression<EClassifier> createQueryForCondition(
			Condition condition) throws ParserException {
		String oclExpression = createOclExpression(condition, OCLLiterals.SELF
				+ OCLLiterals.DOT, "");
		return getOclHelperInstance(condition.getTemplate()).createQuery(
				oclExpression);
	}

	/**
	 * Creates a {@link OCLExpression} for the specified <code>expression</code>
	 * in the context of the specified <code>template</code>.
	 * 
	 * @param template
	 *            context template.
	 * @param expression
	 *            expression to create constraint for.
	 * @return created constraint.
	 * @throws ParserException
	 *             if created {@link OCLExpression} is invalid.
	 */
	private Constraint createQueryForExpression(Template template,
			String expression) throws ParserException {
		try {
			Constraint constraint = getOclHelperInstance(template)
					.createConstraint(ConstraintKind.INVARIANT, expression);
			return constraint;
		} catch (ParserException e) {
			throw e;
		}

	}

	/**
	 * Creates an error {@link EvaluationResult} for the specified
	 * {@code throwable}.
	 * 
	 * @param throwable
	 *            The {@link Throwable} to create {@link EvaluationResult} for.
	 * @return the created {@link EvaluationResult}.
	 */
	private EvaluationResult createEvaluationResult(Throwable throwable) {
		EvaluationResult evaluationResult = ConditionsFactory.eINSTANCE
				.createEvaluationResult();
		evaluationResult.setStatus(EvaluationStatus.ERROR);
		evaluationResult.setException(throwable);
		evaluationResult.setMessage(throwable.getMessage());
		evaluationResult.setEvaluator(EVALUATOR);
		return evaluationResult;
	}

	/**
	 * Creates an {@link EvaluationResult} for the specified
	 * <code>satisfied</code>
	 * 
	 * @param satisfied
	 *            Specifies whether the result represents a satisfied Condition
	 *            or Template.
	 * @return the created {@link EvaluationResult}.
	 */
	protected EvaluationResult createEvaluationResult(boolean satisfied) {
		EvaluationResult evaluationResult = ConditionsFactory.eINSTANCE
				.createEvaluationResult();
		if (satisfied) {
			evaluationResult.setStatus(EvaluationStatus.SATISFIED);
		} else {
			evaluationResult.setStatus(EvaluationStatus.UNSATISFIED);
		}
		evaluationResult.setEvaluator(EVALUATOR);
		return evaluationResult;
	}

	/**
	 * Creates a parent {@link EvaluationResult} for the specified
	 * <code>resultList</code> as children.
	 * 
	 * @param resultList
	 *            The list of child results.
	 * @return the created {@link EvaluationResult}.
	 */
	private EvaluationResult createEvaluationResult(
			EList<EvaluationResult> resultList) {
		EvaluationResult evaluationResult = ConditionsFactory.eINSTANCE
				.createEvaluationResult();

		// check status of specifed children
		boolean isSatisfied = true;
		boolean isError = false;
		for (EvaluationResult subEvaluationResult : resultList) {
			if (!subEvaluationResult.isOK()) {
				isSatisfied = false;
				if (subEvaluationResult.getStatus().equals(
						EvaluationStatus.ERROR)) {
					isError = true;
				}
			}
		}

		// set status
		if (isSatisfied) {
			evaluationResult.setStatus(EvaluationStatus.SATISFIED);
		} else if (isError) {
			evaluationResult.setStatus(EvaluationStatus.ERROR);
		} else {
			evaluationResult.setStatus(EvaluationStatus.UNSATISFIED);
		}

		// set children
		evaluationResult.getSubResults().addAll(resultList);
		evaluationResult.setEvaluator(EVALUATOR);

		return evaluationResult;
	}

	/**
	 * Creates an OCL Expression for the specified <code>template</code>.
	 * 
	 * The condition will look like:
	 * <code>prefix + conditionstring1 + postfix</code>
	 * 
	 * Involved templates remain untouched and are not replaced.
	 * 
	 * @param condition
	 *            condition to create expression for.
	 * @param prefix
	 *            prefix to put before each condition.
	 * @param postfix
	 *            postfix to put after each condition.
	 * @return created condition expression.
	 */
	private String createOclExpression(Condition condition, String prefix,
			String postfix) {
		String oclExpression = "";
		if (condition instanceof FeatureCondition) {
			// condition is a feature condition
			FeatureCondition featureCondition = (FeatureCondition) condition;
			oclExpression += prefix;
			oclExpression += featureCondition.getFeature().getName();
			oclExpression += featureCondition.getExpression();
			oclExpression += postfix;
		} else if (condition instanceof CustomCondition) {
			// condition is a ocl condition
			CustomCondition customOCLCondition = (CustomCondition) condition;
			oclExpression += prefix;
			oclExpression += customOCLCondition.getExpression();
			oclExpression += postfix;
		} else {
			// don't know condition type, so log it and return null
			ConditionsPlugin
					.getDefault()
					.getLog()
					.log(new Status(IStatus.ERROR, ConditionsPlugin.PLUGIN_ID,
							ERR_UNKOWN_COND_TYPE + condition));
		}
		return oclExpression;
	}

	/**
	 * Creates an OCL expression selecting the <code>targetObject</code>
	 * relatively from the <code>baseObject</code>. The resulting expression
	 * looks something like
	 * <code>self.eContainer().eContainer().eContents()->at(2).eContents()->at(1)</code>
	 * . .
	 * 
	 * @param baseObject
	 *            object which is represented by <code>self</code> in the
	 *            expression.
	 * @param targetObject
	 *            object to select relatively from <code>self</code>.
	 * @return created OCL expression.
	 * @throws IllegalArgumentException
	 *             if <code>baseObject</code> and <code>targetObject</code> have
	 *             no common parent.
	 */
	protected String createRelativeOCLExpression(EObject baseObject,
			EObject targetObject) {

		// if they are equal just return self
		if (baseObject.equals(targetObject)) {
			return OCLLiterals.SELF;
		}

		List<EObject> targetsParents = EcoreUtil.createParentList(targetObject);

		EObject commonParent = EcoreUtil.findLeastCommonParentObject(
				baseObject, targetObject);
		if (commonParent == null) {
			throw new IllegalArgumentException(NO_COMMON_PARENT);
		}

		// create ocl string to common parent
		String oclExpression = OCLLiterals.SELF;
		EObject currentParent = baseObject;
		boolean commonParentReached = commonParent.equals(currentParent);
		while (!commonParentReached
				&& (currentParent = currentParent.eContainer()) != null) {
			oclExpression += OCLLiterals.DOT + OCLLiterals.E_CONTAINER;
			if (currentParent.equals(commonParent)) {
				commonParentReached = true;
			}
		}

		// add targetObject to parents list
		targetsParents.add(0, targetObject);
		boolean addedEContents = false;
		// add string down to targetObject
		for (int i = targetsParents.indexOf(commonParent) - 1; i >= 0; i--) {

			oclExpression += OCLLiterals.DOT + OCLLiterals.E_CONTENTS;
			int position = targetsParents.get(i + 1).eContents()
					.indexOf(targetsParents.get(i)) + 1;
			oclExpression += OCLLiterals.AT_START + position
					+ OCLLiterals.AT_END;
			// add type cast
			oclExpression += OCLLiterals.DOT + OCLLiterals.AS_TYPE_START
					+ targetsParents.get(i).eClass().getEPackage().getName()
					+ OCLLiterals.PACKAGE_SEP
					+ targetsParents.get(i).eClass().getName()
					+ OCLLiterals.AS_TYPE_END;
			addedEContents = true;

		}

		if (!addedEContents) {
			oclExpression += OCLLiterals.DOT + OCLLiterals.AS_TYPE_START
					+ targetObject.eClass().getEPackage().getName()
					+ OCLLiterals.PACKAGE_SEP + targetObject.eClass().getName()
					+ OCLLiterals.AS_TYPE_END;
		}

		return oclExpression;
	}

	/**
	 * Checks if the specified <code>eObject</code> fulfills the specified
	 * <code>condition</code>.
	 * 
	 * This method validates also inactive conditions. If the condition involves
	 * other templates it tries to find locally valid objects for the referenced
	 * template, i.e., ignoring conditions involving yet other templates.
	 * 
	 * @param condition
	 *            condition to validate <code>eObject</code> against.
	 * @param eObject
	 *            object to validate.
	 * @return <code>true</code> if <code>eObject</code> fulfills the
	 *         <code>condition</code>
	 * @throws ParserException
	 *             if condition is invalid.
	 * 
	 */
	private boolean isSatisfied(Condition condition, EObject eObject)
			throws ParserException {

		boolean isSatisfied = false;

		if (!isLocal(condition)) {

			ITemplateBinding preBinding = this.templateBindingCreator
					.createEmptyTemplateBinding();
			preBinding.add(condition.getTemplate(), eObject);

			Collection<ITemplateBinding> allBindingCombinations = this.templateBindingCreator
					.calcTemplateObjectPermutation(
							extractInvolvedTemplates(condition), preBinding,
							null, this.templateBindingCreator
									.createEmptyTemplateBinding());

			// check for each binding combination
			for (ITemplateBinding bindingCombination : allBindingCombinations) {
				if (this.isSatisfied(eObject,
						bindingCombination.getSingleBindingMap(), condition)) {
					isSatisfied = true;
					break;
				}
			}
		} else {
			isSatisfied = getOclInstance(condition.getTemplate()).check(
					eObject, createQueryForCondition(condition));
		}
		return isSatisfied;
	}

	/**
	 * Checks if the specified <code>eObject</code> fulfills the specified
	 * <code>constraint</code> in the context of the specified
	 * <code>template</code>.
	 * 
	 * @param constraint
	 *            to check.
	 * @param template
	 *            context template.
	 * @param eObject
	 *            object to check.
	 * @return <code>true</code> if <code>eObject</code> fulfills the
	 *         <code>constraint</code>
	 */
	private boolean isSatisfied(Constraint constraint, Template template,
			EObject eObject) {
		return getOclInstance(template).check(eObject, constraint);
	}

	/**
	 * Checks if the specified <code>condition</code> is satisfied with
	 * <code>eObject</code> using the specified <code>preBinding</code>. Returns
	 * <code>true</code> if there is at least one combination which satisfies
	 * the <code>condition</code>.
	 * 
	 * @param condition
	 *            condition to check.
	 * @param eObject
	 *            {@link EObject} to check.
	 * @param preBinding
	 *            pre-binding to check with.
	 * @return <code>true</code> if the condition is satisfied.
	 *         <code>false</code> otherwise.
	 * @throws ParserException
	 *             if the created expression is invalid.
	 */
	private boolean isSatisfied(Condition condition, EObject eObject,
			ITemplateBinding preBinding) throws ParserException {
		boolean isSatisfied = false;

		if (!isLocal(condition)) {

			// get candidates for each referenced template
			Set<Template> referencedTemplates = extractInvolvedTemplates(condition);

			Collection<ITemplateBinding> permutation = this.templateBindingCreator
					.calcTemplateObjectPermutation(referencedTemplates,
							preBinding, null, this.templateBindingCreator
									.createEmptyTemplateBinding());

			for (ITemplateBinding iTemplateBinding : permutation) {
				if (isSatisfied(eObject,
						iTemplateBinding.getSingleBindingMap(), condition)) {
					return true;
				}
			}

		} else {
			isSatisfied = isSatisfied(condition, eObject);
		}

		return isSatisfied;
	}

	/**
	 * Returns <code>true</code> if the specified <code>condition</code> refers
	 * to another template (prefixed or not).
	 * 
	 * @param condition
	 *            to test.
	 * @return <code>true</code> if it refers to another {@link Template},
	 *         <code>false</code> if not.
	 */
	protected boolean isLocal(Condition condition) {
		String expression = ConditionsUtil.getExpression(condition);
		boolean foundTemplate = ConditionsUtil.singleTemplateNamePattern
				.matcher(expression).find();
		boolean foundPrefixedTemplate = ConditionsUtil.singlePrefixedTemplateNamePattern
				.matcher(expression).find();
		return !foundTemplate && !foundPrefixedTemplate;
	}

	/**
	 * Extracts referenced templates from the specified conditions.
	 * 
	 * @param conditions
	 *            conditions to extract templates from.
	 * @return extracted templates.
	 */
	protected Set<Template> extractInvolvedTemplates(Set<Condition> conditions) {
		Set<Template> templates = new HashSet<Template>();
		for (Condition condition : conditions) {
			templates.addAll(extractInvolvedTemplates(condition));
		}
		return templates;
	}

	/**
	 * Extracts referenced (unprefixed) templates from the specified condition.
	 * 
	 * 
	 * @param condition
	 *            condition to extract templates from.
	 * @return extracted templates.
	 */
	protected Set<Template> extractInvolvedTemplates(Condition condition) {
		return extractInvolvedTemplates(ConditionsUtil.getExpression(condition));
	}

	/**
	 * Extracts referenced (unprefixed) templates from the specified
	 * <code>oclExpression</code>.
	 * 
	 * @param oclExpression
	 *            ocl expression to extract templates from.
	 * @return extracted templates.
	 */
	protected Set<Template> extractInvolvedTemplates(String oclExpression) {
		Set<Template> extractedTemplates = new HashSet<Template>();
		Map<String, Set<String>> referencedTemplateNames = ConditionsUtil
				.getReferencedTemplateNames(oclExpression);
		// only return unprefixed templates
		if (referencedTemplateNames.get("") != null) { //$NON-NLS-1$
			for (String templateName : referencedTemplateNames.get("")) { //$NON-NLS-1$
				Template template = getTemplateByName(templateName);
				if (template != null) {
					extractedTemplates.add(template);
				}
			}
		}
		return extractedTemplates;
	}

	/**
	 * Extracts referenced and prefixed templates from the specified condition.
	 * 
	 * @param condition
	 *            condition to extract templates from.
	 * @return extracted templates.
	 */
	protected Map<String, Set<Template>> extractInvolvedPrefixedTemplates(
			Condition condition) {
		String oclExpression = createOclExpression(condition, "", ""); //$NON-NLS-1$ //$NON-NLS-2$
		return extractInvolvedPrefixedTemplatesFromString(oclExpression);
	}

	/**
	 * Extracts referenced and prefixed templates from the specified string
	 * <code>expression</code>.
	 * 
	 * @param expression
	 *            string to extract templates from.
	 * @return extracted templates.
	 */
	private Map<String, Set<Template>> extractInvolvedPrefixedTemplatesFromString(
			String expression) {
		Map<String, Set<Template>> extractedTemplates = new HashMap<String, Set<Template>>();
		Map<String, Set<String>> referencedTemplateNames = ConditionsUtil
				.getReferencedTemplateNames(expression);

		for (String prefix : referencedTemplateNames.keySet()) {
			// only extract prefixed templates
			if (!"".equals(prefix)) { //$NON-NLS-1$
				for (String templateName : referencedTemplateNames.get(prefix)) {
					Template template = getTemplateByName(prefix, templateName);
					if (template != null) {
						if (extractedTemplates.get(prefix) == null) {
							extractedTemplates.put(prefix,
									new HashSet<Template>());
						}
						extractedTemplates.get(prefix).add(template);
					}
				}
			}
		}
		return extractedTemplates;
	}

	/**
	 * Returns the {@link Template} with a name matching
	 * <code>templateName</code> in the currently set conditions model (see
	 * {@link #setConditionsModel(ConditionsModel)}).
	 * 
	 * @param templateName
	 *            name to search Template.
	 * @return found {@link Template} with a name that equals
	 *         <code>templateName</code>.
	 */
	private Template getTemplateByName(String templateName) {
		return ConditionsUtil.getTemplateByName(templateName,
				getConditionsModel());
	}

	/**
	 * Returns the {@link Template} in the specified <code>prefix</code> with
	 * the specified <code>templateName</code> which is currently registered in
	 * {@link #relatedBindings}.
	 * 
	 * @param prefix
	 *            the prefix of template to find.
	 * @param templateName
	 *            the name of template to find.
	 * @return found template or <code>null</code>.
	 */
	private Template getTemplateByName(String prefix, String templateName) {
		ITemplateBindings binding = this.relatedBindings.get(prefix);
		if (binding != null) {
			for (Template template : binding.getTemplates()) {
				if (templateName.equals(template.getName())) {
					return template;
				}
			}
		}
		return null;
	}

	/**
	 * Checks whether the specified <code>condition</code> is satisfied with
	 * <code>eObject</code> and the referenced objects in
	 * <code>referencedEObjectMap</code>.
	 * 
	 * @param eObject
	 *            object to evaluate.
	 * @param referencedEObjectMap
	 *            referenced objects.
	 * @param condition
	 *            condition to check.
	 * @return <code>true</code> if condition is fulfilled. <code>false</code>
	 *         otherwise.
	 * @throws ParserException
	 *             if condition is invalid.
	 */
	protected boolean isSatisfied(EObject eObject,
			Map<Template, EObject> referencedEObjectMap, Condition condition)
			throws ParserException {
		String oclExpression = createReplacedOCLExpression(eObject,
				referencedEObjectMap, condition);

		try {
			// check condition
			Constraint constraint = createQueryForExpression(
					condition.getTemplate(), oclExpression);
			boolean isSatisfied = isSatisfied(constraint,
					condition.getTemplate(), eObject);
			return isSatisfied;
		} catch (ParserException e) {
			if (isThrowExceptionOnParserError) {
				throw e;
			} else {
				return false;
			}
		}
	}

	/**
	 * Creates an OCL expression for the specified <code>condition</code>.
	 * Referenced {@link Template} will be replaced by direct navigation to the
	 * objects in <code>referencedEObjectMap</code> from <code>eObject</code>.
	 * 
	 * @param eObject
	 *            object to navigate to referenced templates from.
	 * @param referencedEObjectMap
	 *            map to resolve objects for referenced templates.
	 * @param condition
	 *            condition to create OCL expression for.
	 * @return created ocl expression.
	 */
	private String createReplacedOCLExpression(EObject eObject,
			Map<Template, EObject> referencedEObjectMap, Condition condition) {
		// get ocl expression of condition
		String oclExpression = createOclExpression(condition, OCLLiterals.SELF
				+ OCLLiterals.DOT, ""); //$NON-NLS-1$

		oclExpression = createReplacedOCLExpression(eObject,
				referencedEObjectMap, oclExpression);
		return oclExpression;
	}

	/**
	 * Replaces referenced {@link Template} in the specified
	 * <code>oclExpression</code> by direct navigation to the objects in
	 * <code>referencedEObjectMap</code> from <code>eObject</code>.
	 * 
	 * @param eObject
	 *            object to navigate to referenced templates from.
	 * @param referencedEObjectMap
	 *            map to resolve objects for referenced templates.
	 * @param oclExpression
	 *            OCL expression to replace referenced templates.
	 * @return created ocl expression.
	 */
	private String createReplacedOCLExpression(EObject eObject,
			Map<Template, EObject> referencedEObjectMap, String oclExpression) {
		// replace prefixed templates by the values in the prefixed binding
		Map<String, Set<Template>> prefixedTemplates = extractInvolvedPrefixedTemplatesFromString(oclExpression);
		for (String prefix : prefixedTemplates.keySet()) {
			for (Template template : prefixedTemplates.get(prefix)) {
				// replace each occurrence of template
				for (String featureName : ConditionsUtil
						.getReferencedFeatureNames(prefix, template,
								oclExpression)) {
					try {
						// try to extract referenced feature
						EStructuralFeature feature = template
								.getRepresentative().eClass()
								.getEStructuralFeature(featureName);
						if (this.relatedBindings.get(prefix)
								.getBoundObjects(template).size() > 0) {
							// try to extract value of bound object
							EObject object = this.relatedBindings.get(prefix)
									.getBoundObjects(template).iterator()
									.next();
							Object value = object.eGet(feature);
							// put apostrophes around if it is string
							String replacement = "";
							if (feature.getEGenericType().getERawType() instanceof EDataType) {
								EDataType dataType = (EDataType) feature
										.getEGenericType().getERawType();
								if (String.class.equals(dataType
										.getInstanceClass())) {
									replacement = OCLLiterals.APO + value
											+ OCLLiterals.APO;
								} else {
									replacement = value.toString();
								}
								oclExpression = oclExpression
										.replace(
												TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
														+ prefix
														+ TemplateMaskLiterals.PREFIX_SEP
														+ template.getName()
														+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END
														+ OCLLiterals.DOT
														+ featureName,
												replacement);
							}
						}
					} catch (NullPointerException npe) {
						continue;
					}
				}
			}
		}

		Set<Template> templates = extractInvolvedTemplates(oclExpression);

		// replace occurrences of the referenced templates with direct
		// OCL path
		for (Template template : templates) {
			String replacement = createRelativeOCLExpression(eObject,
					referencedEObjectMap.get(template));
			oclExpression = oclExpression.replace(
					TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
							+ template.getName()
							+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END,
					replacement);
		}
		return oclExpression;
	}

	/**
	 * Extracts all referenced feature names for the specified
	 * <code>template</code> in specified <code>prefix</code> in
	 * <code>condition</code>.
	 * 
	 * The condition
	 * 
	 * <pre>
	 * self.name = #{initial:Template}.name + #{initial:Template}.size
	 * </pre>
	 * 
	 * will return <code>name</code> and <code>size</code>.
	 * 
	 * @param prefix
	 *            prefix of <code>template</code>.
	 * @param template
	 *            template to look out for.
	 * @param condition
	 *            condition to extract from.
	 * @return referenced feature names.
	 */
	protected Set<String> getReferencedFeatures(String prefix,
			Template template, Condition condition) {
		String oclExpression = createOclExpression(condition, "", ""); //$NON-NLS-1$ $NON-NLS-2$
		return ConditionsUtil.getReferencedFeatureNames(prefix, template,
				oclExpression);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isComplete(ITemplateBinding binding) {
		for (Template template : templates) {
			if (shouldEvaluate(template)) {
				if (binding.getBoundObjects(template) == null
						|| binding.getBoundObjects(template).size() < 1) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see IConditionEvaluationEngine#getConditionsModel()
	 * 
	 */
	@Override
	public ConditionsModel getConditionsModel() {
		return conditionsModel;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see IConditionEvaluationEngine#setConditionsModel(ConditionsModel)
	 * 
	 */
	@Override
	public void setConditionsModel(ConditionsModel conditionsModel)
			throws UnsupportedConditionLanguage {
		// guard unsupported conditions language
		if (!OCLLiterals.CONDITION_LANGUAGE.equals(conditionsModel
				.getLanguage())) {
			throw new UnsupportedConditionLanguage();
		}
		this.conditionsModel = conditionsModel;
		initialize();
	}

	/**
	 * (Re-)Initializes this evaluation engine.
	 */
	public void initialize() {
		this.evaluationRunState = EvaluationRunMode.FIRST_RUN;
		this.currentOptionGroup = null;
		this.currentNonExistenceGroup = null;
		this.activatedOptionGroups.clear();
		this.templateBindingCreator = new TemplateBindingCreator(this);
		extractTemplates(this.conditionsModel);
		deriveInvolvesTemplateProperty(this.conditionsModel);
	}

	/**
	 * Extracts and classifies the {@link Template}s contained by the specified
	 * <code>conditionsModel</code>.
	 * 
	 * @param conditionsModel
	 *            to extract templates from.
	 */
	private void extractTemplates(ConditionsModel conditionsModel) {
		this.templates = ConditionsUtil.getAllTemplates(conditionsModel);
		this.normalTemplates = new BasicEList<Template>();
		this.nonExistenceTemplates = new BasicEList<Template>();
		this.optionalTemplates = new BasicEList<Template>();
		for (Template template : templates) {
			if (ConditionsUtil.isOptional(template, true)) {
				this.optionalTemplates.add(template);
			} else if (ConditionsUtil.isNonExistence(template, true)) {
				this.nonExistenceTemplates.add(template);
			} else {
				this.normalTemplates.add(template);
			}

		}
	}

	/**
	 * Returns the list of {@link Template Templates} to evaluate regarding the
	 * current {@link #evaluationRunState evaluation run state}.
	 * 
	 * @return the list of {@link Template Templates} to evaluate.
	 */
	protected List<Template> getTemplatesToEvaluate() {
		List<Template> templates = new BasicEList<Template>();
		templates.addAll(this.normalTemplates);
		for (OptionGroup activatedOptGroup : activatedOptionGroups) {
			templates.addAll(activatedOptGroup.getTemplates());
		}
		if (EvaluationRunMode.ALL.equals(evaluationRunState)) {
			templates.addAll(nonExistenceTemplates);
			templates.addAll(optionalTemplates);
			// TODO maybe get rid of nonExistenceTemplates and optionalTemplates
			// list and use getAllTemplates here
		} else if (EvaluationRunMode.CHECK_NONEXISTENCE
				.equals(evaluationRunState)) {
			templates.addAll(currentNonExistenceGroup.getTemplates());
		} else if (EvaluationRunMode.CHECK_OPTIONAL.equals(evaluationRunState)) {
			templates.addAll(currentOptionGroup.getTemplates());
		}
		return templates;
	}

	/**
	 * Specifies whether to evaluate the specified <code>template</code> in the
	 * current {@link #evaluationRunState evaluation run state}.
	 * 
	 * @param template
	 *            to check.
	 * @return <code>true</code> if it should be evaluated, otherwise
	 *         <code>false</code>.
	 */
	protected boolean shouldEvaluate(Template template) {
		return getTemplatesToEvaluate().contains(template);
	}

	/**
	 * Specifies whether to evaluate the specified <code>condition</code> in the
	 * current {@link #evaluationRunState evaluation run state}.
	 * 
	 * @param condition
	 *            to check.
	 * @return <code>true</code> if it should be evaluated, otherwise
	 *         <code>false</code>.
	 */
	protected boolean shouldEvaluate(Condition condition) {
		// if condition is inactive, we don't evaluate it anyway
		if (!condition.isActive()) {
			return false;
		}
		// check if condition involves a template which we shouldn't evaluate
		Set<Template> involvedTemplates = extractInvolvedTemplates(condition);
		for (Template involdedTemplate : involvedTemplates) {
			if (!shouldEvaluate(involdedTemplate)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns all {@link Condition#isActive() active} and
	 * {@link Condition#isInvolvesTemplate() template-involving} conditions to
	 * evaluate.
	 * 
	 * <p>
	 * This method regards the current {@link #evaluationRunState evaluation run
	 * state}.
	 * </p>
	 * 
	 * @param template
	 *            to get conditions.
	 * @return template-involving, active conditions to evaluate.
	 */
	public Set<Condition> getTemplateInvolvingConditionsToEvaluate(
			Template template) {
		Set<Condition> conditions = new HashSet<Condition>();
		for (Condition condition : ConditionsUtil
				.getActiveTemplateInvolvingConditions(template)) {
			if (shouldEvaluate(condition)) {
				conditions.add(condition);
			}
		}
		return conditions;
	}

	/**
	 * Derives and sets the property {@link Condition#isInvolvesTemplate()}
	 * according its condition. This can only be done for
	 * {@link FeatureCondition}s and {@link CustomCondition}s.
	 * 
	 * @param conditionsModel
	 *            to derive and set properties of.
	 */
	private void deriveInvolvesTemplateProperty(ConditionsModel conditionsModel) {
		for (Template template : this.templates) {
			for (Condition condition : template.getSpecifications()) {
				if (isLocal(condition)) {
					condition.setInvolvesTemplate(false);
				} else {
					condition.setInvolvesTemplate(true);
				}
			}
		}
	}

	/**
	 * Generates a containment condition for the specified template.
	 * 
	 * @param template
	 *            to specify containment condition for.
	 * @return containment condition.
	 */
	protected Condition createContainmentCondition(Template template) {
		Assert.isNotNull(template.getParentTemplate());
		CustomCondition condition = ConditionsFactory.eINSTANCE
				.createCustomCondition();
		condition.setActive(true);
		condition.setInvolvesTemplate(true);
		condition.setState(State.GENERATED);
		condition.setExpression(OCLLiterals.E_CONTAINER + OCLLiterals.EQUAL
				+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
				+ template.getParentTemplate().getName()
				+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END);
		template.getSpecifications().add(condition);
		return condition;
	}

	/**
	 * Creates a single binding {@link Map} from {@link Template}s to their
	 * representatives {@link Template#getRepresentative()} in the specified
	 * <code>conditionsModel</code>.
	 * 
	 * @param conditionsModel
	 *            {@link ConditionsModel} to create single binding map from.
	 * @return created Template to Object Map.
	 */
	private Map<Template, EObject> createTemplateToObjectMap(
			ConditionsModel conditionsModel) {
		Map<Template, EObject> map = new HashMap<Template, EObject>();
		map.put(conditionsModel.getRootTemplate(), conditionsModel
				.getRootTemplate().getRepresentative());
		TreeIterator<EObject> treeIter = getConditionsModel().getRootTemplate()
				.eAllContents();
		while (treeIter.hasNext()) {
			EObject content = treeIter.next();
			if (content instanceof Template) {
				Template subTemplate = (Template) content;
				map.put(subTemplate, subTemplate.getRepresentative());
			}
		}
		return map;
	}

	/**
	 * Finds all valid template bindings for the specified
	 * <code>preBinding</code> and <code>candidateBinding</code>.
	 * 
	 * TODO trace results (dropped binding)
	 * 
	 * @param preBinding
	 *            pre-binding to use.
	 * @param candidateBinding
	 *            candidates (may be <code>null</code>).
	 * @return all found template bindings.
	 */
	private ITemplateBindings doFindTemplateBinding(
			ITemplateBinding preBinding, ITemplateBinding candidateBinding) {
		try {
			// be safe, do a fresh initialization
			initialize();

			// initialize returned binding set
			TemplateBindingsImpl validBindings = new TemplateBindingsImpl(
					conditionsModel.getRootTemplate());

			// check for option groups to activate due to pre-binding
			for (Template boundTemplate : preBinding.getTemplates()) {
				if (!boundTemplate.isMandatory()) {
					for (OptionGroup optionGroup : boundTemplate
							.getOptionGroups()) {
						if (!activatedOptionGroups.contains(optionGroup)) {
							activatedOptionGroups.add(optionGroup);
						}
					}
				}
			}

			EList<EvaluationResult> evaluationResults = new BasicEList<EvaluationResult>();

			// do first run with normal templates
			ITemplateBindings firstRunBindings = templateBindingCreator
					.findTemplateBindings(preBinding, candidateBinding);
			if (!firstRunBindings.validate().isOK()) {
				evaluationResults.addAll(firstRunBindings.validate()
						.getSubResults());
			}

			if (conditionsModel.getNonExistenceGroups().size() > 0) {
				// do non-existence run for each non existence group
				evaluationRunState = EvaluationRunMode.CHECK_NONEXISTENCE;
				for (ITemplateBinding templateBinding : firstRunBindings
						.getAllPossibleBindings()) {
					for (NonExistenceGroup nexGroup : conditionsModel
							.getNonExistenceGroups()) {
						currentNonExistenceGroup = nexGroup;
						ITemplateBindings nexBinding = templateBindingCreator
								.findTemplateBindings(templateBinding);
						// preserve all invalid non-existence bindings
						if (!nexBinding.validate().isOK()) {
							validBindings.getAllPossibleBindings().add(
									templateBinding);
						} else {
							EvaluationResult result = ConditionsFactory.eINSTANCE
									.createEvaluationResult();
							result.setEvaluator(EVALUATOR);
							result.setMessage("Non-existence group "
									+ nexGroup.getName() + " matched.");
							result.setStatus(EvaluationStatus.UNSATISFIED);
							evaluationResults.add(result);
						}
					}
				}
				currentNonExistenceGroup = null;
			} else {
				// keep all bindings
				validBindings.getAllPossibleBindings().addAll(
						firstRunBindings.getAllPossibleBindings());
			}

			// do option group run and add them to found bindings (regard
			// additional switches in option groups)
			evaluationRunState = EvaluationRunMode.CHECK_OPTIONAL;
			for (OptionGroup optGroup : conditionsModel.getOptionGroups()) {
				if (activatedOptionGroups.contains(optGroup)) {
					// if optGroup is already activated don't evaluate it again
					continue;
				}
				currentOptionGroup = optGroup;
				Set<ITemplateBinding> bindingsToReplace = new HashSet<ITemplateBinding>();
				for (ITemplateBinding templateBinding : validBindings
						.getAllPossibleBindings()) {
					ITemplateBindings optBinding = templateBindingCreator
							.findTemplateBindings(templateBinding);
					if (optBinding.validate().isOK()) {
						// add or replace found bindings
						if (optGroup.isReplace()) {
							bindingsToReplace.add(templateBinding);
							// FIXME we should add current optgroup to activated
							// when re-evaluating this binding for future
							// optgroups (otherwise it would always be optional
							// since evaluation runs through without current
							// optgroup)
						}
						validBindings.getAllPossibleBindings().addAll(
								optBinding.getAllPossibleBindings());
					}
				}
				validBindings.getAllPossibleBindings().removeAll(
						bindingsToReplace);
				currentOptionGroup = null;
			}

			// reset evaluation run mode
			evaluationRunState = EvaluationRunMode.FIRST_RUN;

			// create evaluation result
			boolean isSatisfied = validBindings.getAllPossibleBindings().size() > 0;
			EvaluationResult evaluationResult = createEvaluationResult(isSatisfied);
			if (!isSatisfied) {
				evaluationResult.getSubResults().addAll(evaluationResults);
			}
			validBindings.setEvaluationResult(evaluationResult);

			return validBindings;
		} catch (BindingException e) {
			TemplateBindingsImpl templateBinding = new TemplateBindingsImpl(
					null);
			templateBinding.setEvaluationResult(createEvaluationResult(e));
			return templateBinding;
		} catch (ParserException e) {
			TemplateBindingsImpl templateBinding = new TemplateBindingsImpl(
					null);
			templateBinding.setEvaluationResult(createEvaluationResult(e));
			return templateBinding;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see IConditionEvaluationEngine#getConditionLanguage()
	 * 
	 */
	@Override
	public String getConditionLanguage() {
		return OCLLiterals.CONDITION_LANGUAGE;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see IConditionEvaluationEngine#evaluate(Condition, EObject)
	 * 
	 */
	@Override
	public EvaluationResult evaluate(Condition condition, EObject eObject) {
		try {
			// create result
			EvaluationResult evaluationResult = createEvaluationResult(isSatisfied(
					condition, eObject));

			// set failed conditions if any
			if (!evaluationResult.isOK()) {
				evaluationResult.setFailedCondition(condition);
			}

			// return created result
			return evaluationResult;
		} catch (ParserException e) {
			return createEvaluationResult(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws ParserException
	 *             if a constraint is invalid.
	 * 
	 * @see IConditionEvaluationEngine#evaluate(Template, EObject, boolean)
	 * 
	 */
	@Override
	public EvaluationResult evaluate(Template template, EObject eObject,
			boolean localOnly) {
		if (isTypeAware
				&& !org.eclipse.emf.ecore.util.EcoreUtil
						.equals(eObject.eClass(), template.getRepresentative()
								.eClass())) {
			return createEvaluationResult(false);
		}
		if (!localOnly) {
			return findTemplateBinding(template, eObject).validate();
		} else {
			// extract conditions to evaluate (only active and local)
			Set<Condition> activeConditions = ConditionsUtil
					.getActiveConditions(template);
			Set<Condition> conditionToEvaluate = new HashSet<Condition>();
			for (Condition condition : activeConditions) {
				if (isLocal(condition)) {
					conditionToEvaluate.add(condition);
				}
			}

			// evaluate all conditions
			boolean isSatisfied = true;
			boolean isError = false;
			Set<Condition> failingConditions = new HashSet<Condition>();
			for (Condition condition : conditionToEvaluate) {
				EvaluationResult conditionResult = evaluate(condition, eObject);
				if (!conditionResult.isOK()) {
					failingConditions.add(condition);
					if (conditionResult.getStatus().equals(
							EvaluationStatus.ERROR)) {
						isError = true;
					}
					if (conditionResult.getStatus().equals(
							EvaluationStatus.UNSATISFIED)) {
						isSatisfied = false;
					}
				}
			}

			// create root result
			EvaluationResult evalResult = createEvaluationResult(isSatisfied);
			if (isError) {
				evalResult.setStatus(EvaluationStatus.ERROR);
			}
			if (failingConditions.size() > 0) {
				evalResult.setFailedCondition(failingConditions.iterator()
						.next());
			}
			return evalResult;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see IConditionEvaluationEngine#findTemplateBinding(Template, EObject)
	 * 
	 */
	@Override
	public ITemplateBindings findTemplateBinding(Template template,
			EObject eObject) {

		ITemplateBinding preBinding = this.templateBindingCreator
				.createEmptyTemplateBinding();
		preBinding.add(template, eObject);

		return findTemplateBinding(preBinding);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see IConditionEvaluationEngine#findTemplateBinding(ITemplateBinding)
	 * 
	 */
	@Override
	public ITemplateBindings findTemplateBinding(ITemplateBinding preBinding) {
		return findTemplateBinding(preBinding, false);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITemplateBindings findTemplateBinding(ITemplateBinding preBinding,
			boolean reEvaluatePreBound) {
		if (reEvaluatePreBound) {
			return doFindTemplateBinding(new TemplateBindingImpl(), preBinding);
		} else {
			return doFindTemplateBinding(preBinding, null);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see IConditionEvaluationEngine#findTemplateBinding(ITemplateBinding,
	 *      ITemplateBinding)
	 * 
	 */
	@Override
	public ITemplateBindings findTemplateBinding(ITemplateBinding preBinding,
			ITemplateBinding candidateBinding) {
		return doFindTemplateBinding(preBinding, candidateBinding);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see IConditionEvaluationEngine#validate(Condition)
	 * 
	 */
	@Override
	public EvaluationResult validate(Condition condition) {
		try {
			// create expression for condition using the conditions model
			String oclExpression = createReplacedOCLExpression(condition
					.getTemplate().getRepresentative(),
					createTemplateToObjectMap(this.conditionsModel), condition);

			// create constraint and check it
			this.getOclInstance(condition.getTemplate()).validate(
					createQueryForExpression(condition.getTemplate(),
							oclExpression));
			return createEvaluationResult(true);
		} catch (Exception e) {
			return createEvaluationResult(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see IConditionEvaluationEngine#validate(Template)
	 * 
	 */
	@Override
	public EvaluationResult validate(Template template) {
		EList<EvaluationResult> subResults = new BasicEList<EvaluationResult>();

		// check each condition of the Template
		for (Condition condition : template.getSpecifications()) {
			subResults.add(validate(condition));
		}

		return createEvaluationResult(subResults);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EvaluationResult evaluate(Condition condition, EObject eObject,
			ITemplateBinding preBinding) {
		try {
			return createEvaluationResult(isSatisfied(condition, eObject,
					preBinding));
		} catch (ParserException e) {
			return createEvaluationResult(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getValidValue(FeatureCondition condition, EObject eObject,
			ITemplateBinding binding) {
		try {
			String oclExpression = createReplacedOCLExpression(eObject,
					binding.getSingleBindingMap(), condition);

			oclExpression = oclExpression.replace(OCLLiterals.SELF
					+ OCLLiterals.DOT + condition.getFeature().getName()
					+ OCLLiterals.EQUAL, ""); //$NON-NLS-1$

			OCLExpression<EClassifier> query = (OCLExpression<EClassifier>) getOclHelperInstance(
					condition.getTemplate()).createQuery(oclExpression);
			return getOclInstance(condition.getTemplate()).createQuery(query)
					.evaluate(eObject);
		} catch (ParserException e) {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String replaceTemplateValues(String string,
			ITemplateBindings templateBinding) {

		// replace prefixed templates by the values in the prefixed binding
		Map<String, Set<Template>> prefixedTemplates = extractInvolvedPrefixedTemplatesFromString(string);
		for (String prefix : prefixedTemplates.keySet()) {
			for (Template template : prefixedTemplates.get(prefix)) {
				// replace each occurrence of template
				for (String featureName : ConditionsUtil
						.getReferencedFeatureNames(prefix, template, string)) {
					// extract referenced feature
					EStructuralFeature feature = template.getRepresentative()
							.eClass().getEStructuralFeature(featureName);
					if (this.relatedBindings.get(prefix)
							.getBoundObjects(template).size() > 0) {
						// try to extract value of bound object
						EObject object = this.relatedBindings.get(prefix)
								.getBoundObjects(template).iterator().next();
						Object value = object.eGet(feature);
						String replacement = ""; //$NON-NLS-1$
						if (value == null) {
							replacement = OCLLiterals.NULL;
						} else {
							replacement = value.toString();
						}
						string = string
								.replace(
										TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
												+ prefix
												+ TemplateMaskLiterals.PREFIX_SEP
												+ template.getName()
												+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END
												+ OCLLiterals.DOT + featureName,
										replacement);
					}
				}
			}
		}

		// replace unprefixed templates by the values in the prefixed binding
		Set<Template> templates = extractInvolvedTemplates(string);
		for (Template template : templates) {
			// replace each occurrence of template
			for (String featureName : ConditionsUtil.getReferencedFeatureNames(
					template, string)) {
				// extract referenced feature
				EStructuralFeature feature = template.getRepresentative()
						.eClass().getEStructuralFeature(featureName);
				if (templateBinding.getBoundObjects(template).size() > 0) {
					// try to extract value of bound object
					EObject object = templateBinding.getBoundObjects(template)
							.iterator().next();
					Object value = object.eGet(feature);
					String replacement = ""; //$NON-NLS-1$
					if (value == null) {
						replacement = OCLLiterals.NULL;
					} else {
						replacement = value.toString();
					}
					string = string
							.replace(
									TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
											+ template.getName()
											+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END
											+ OCLLiterals.DOT + featureName,
									replacement);
				}
			}
		}

		return string;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void registerRelatedTemplateBinding(String prefix,
			ITemplateBindings templateBinding) {
		this.relatedBindings.put(prefix, templateBinding);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void unregisterRelatedTemplateBinding(String prefix) {
		this.relatedBindings.remove(prefix);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getConditionPrefix(Condition condition) {
		String prefix = "";
		if (condition instanceof FeatureCondition) {
			prefix += OCLLiterals.SELF + OCLLiterals.DOT
					+ ((FeatureCondition) condition).getFeature().getName();
		} else if (condition instanceof CustomCondition) {
			prefix += OCLLiterals.SELF + OCLLiterals.DOT;
		}
		return prefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Choice[] getContentProposals(String contents, final int position,
			Condition condition) {
		// get trimmed ocl expression
		String txtCondition = contents.substring(0, position).trim();

		// create binding for representatives
		ConditionsModel conditionsModel = ConditionsUtil
				.getContainingConditionsModel(condition.getTemplate());
		Map<Template, EObject> eObjectMap = ConditionsUtil
				.getTemplateToRepresentativeMap(conditionsModel);

		List<Choice> choices = new ArrayList<Choice>();
		String templateMatch = ConditionsUtil
				.getUnclosedTemplateNames(txtCondition);
		if (templateMatch != null) {
			choices.addAll(createTemplateChoices(templateMatch));
		} else {
			// get content assist from helper
			choices.addAll(this.getOclHelperInstance(condition.getTemplate())
					.getSyntaxHelp(
							ConstraintKind.INVARIANT,
							createReplacedOCLExpression(condition.getTemplate()
									.getRepresentative(), eObjectMap,
									txtCondition)));
		}

		return choices.toArray(new Choice[choices.size()]);
	}

	/**
	 * Returns template completion choices for the specified string.
	 * 
	 * @param templateMatch
	 *            to get choices for.
	 * @return the choices.
	 */
	private List<Choice> createTemplateChoices(final String templateMatch) {
		List<Choice> choices = new ArrayList<Choice>();
		// add templates of main conditions model
		for (final Template template : ConditionsUtil
				.getAllTemplates(this.conditionsModel)) {
			if (template.isActive()) {
				final String templateName = TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
						+ template.getName()
						+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END;
				if (templateName.startsWith(templateMatch)) {
					choices.add(newChoice(template, templateName));
				}
			}
		}
		// add templates additionally set conditions model
		for (String key : relatedBindings.keySet()) {
			for (final Template template : relatedBindings.get(key)
					.getTemplates()) {
				if (template.isActive()) {
					final String templateName = TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
							+ key
							+ TemplateMaskLiterals.PREFIX_SEP
							+ template.getName()
							+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END;
					if (templateName.startsWith(templateMatch)) {
						choices.add(newChoice(template, templateName));
					}
				}
			}
		}
		return choices;
	}

	/**
	 * Creates a new {@link Choice} for the specified <code>template</code> and
	 * <code>templateName</code>.
	 * 
	 * @param template
	 *            to create Choice for.
	 * @param templateName
	 *            name to use.
	 * @return the created choice.
	 */
	private Choice newChoice(final Template template, final String templateName) {
		return new Choice() {

			@Override
			public String getName() {
				return templateName.substring(1);
			}

			@Override
			public ChoiceKind getKind() {
				return ChoiceKind.VARIABLE;
			}

			@Override
			public Object getElement() {
				return template;
			}

			@Override
			public String getDescription() {
				return template.getTitle() + " (" //$NON-NLS-1$
						+ template.getRepresentative().eClass() + ")"; //$NON-NLS-1$
			}
		};
	}
}