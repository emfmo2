/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines.impl;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.compare.diff.metamodel.AttributeChange;
import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.diff.metamodel.ReferenceChange;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.ocl.OCL;
import org.modelversioning.core.conditions.ConditionsFactory;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.RefinementTemplate;
import org.modelversioning.core.conditions.State;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.IConditionGenerationEngine;
import org.modelversioning.core.conditions.engines.IFeatureConditionGenerator;
import org.modelversioning.core.conditions.impl.FeatureConditionImpl;
import org.modelversioning.core.conditions.util.ConditionsUtil;
import org.modelversioning.core.diff.util.DiffUtil;
import org.modelversioning.core.match.util.MatchUtil;

/**
 * Implements the {@link IConditionGenerationEngine} for the use with
 * {@link OCL}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ConditionsGenerationEngineImpl implements
		IConditionGenerationEngine {

	/**
	 * Error Message: No general feature condition is configured.
	 */
	private static final String NO_GENERAL_FEATURE_CONDITION_GENERATOR_CONFIGURED = "No general feature condition generator configured.";

	/**
	 * Prefix for refinement mode
	 */
	private static final String REFINEMENT_PREFIX = "NAC_"; //$NON-NLS-1$

	/**
	 * Saves the {@link IFeatureConditionGenerator}s to use.
	 */
	private EList<IFeatureConditionGenerator> featureConditionGenerators = new BasicEList<IFeatureConditionGenerator>();

	/**
	 * The {@link TemplateNameService} to use.
	 */
	private TemplateNameService templateNameService = null;

	/**
	 * The current template generation mode.
	 */
	private TemplateGenerationMode templateGenerationMode = TemplateGenerationMode.STANDALONE;

	/**
	 * The base condition model if in refinement mode.
	 */
	private ConditionsModel baseConditionsModel = null;

	/**
	 * The differences if in refinement mode.
	 */
	private ComparisonResourceSnapshot refinementDifferences = null;

	/**
	 * Mode to generate the templates.
	 * 
	 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
	 * 
	 */
	private enum TemplateGenerationMode {
		STANDALONE, REFINEMENT
	}

	/**
	 * Empty default constructor.
	 * 
	 * <p>
	 * Prepares the default {@link IFeatureConditionGenerator}s (
	 * {@link GenericFeatureConditionGenerator} and
	 * {@link UML2FeatureConditionGenerator}).
	 * </p>
	 */
	public ConditionsGenerationEngineImpl() {
		super();
		this.featureConditionGenerators
				.add(new GenericFeatureConditionGenerator());
		this.featureConditionGenerators
				.add(new UML2FeatureConditionGenerator());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.modelversioning.core.conditions.engines.IConditionEvaluationEngine
	 * #getConditionLanguage()
	 */
	@Override
	public String getConditionLanguage() {
		return OCLLiterals.CONDITION_LANGUAGE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.modelversioning.core.conditions.engines.IConditionGenerationEngine
	 * #addFeatureConditionGenerator
	 * (org.modelversioning.core.conditions.engines.IFeatureConditionGenerator)
	 */
	@Override
	public void addFeatureConditionGenerator(
			IFeatureConditionGenerator featureConditionGenerator) {
		if (OCLLiterals.CONDITION_LANGUAGE.equals(featureConditionGenerator
				.getConditionLanguage())) {
			return;
		} else {
			this.featureConditionGenerators.add(featureConditionGenerator);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.modelversioning.core.conditions.engines.IConditionGenerationEngine
	 * #getFeatureConditionGenerators()
	 */
	@Override
	public List<IFeatureConditionGenerator> getFeatureConditionGenerators() {
		return Collections.unmodifiableList(this.featureConditionGenerators);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.modelversioning.core.conditions.engines.IConditionGenerationEngine
	 * #removeFeatureConditionGenerator
	 * (org.modelversioning.core.conditions.engines.IFeatureConditionGenerator)
	 */
	@Override
	public void removeFeatureConditionGenerator(
			IFeatureConditionGenerator featureConditionGenerator) {
		this.featureConditionGenerators.remove(featureConditionGenerator);
	}

	/**
	 * Implements
	 * {@link IConditionGenerationEngine#generateConditionsModel(EObject)}.
	 * 
	 * <p>
	 * First, this method creates a root {@link Template} for
	 * <code>object</code>. Then, for each element returned by
	 * {@link #eGet(EStructuralFeature)} with each {@link EReference} in
	 * <code>object.eClass().getEAllContainments()</code> a new sub template
	 * will be created. This is repeated for each newly created {@link Template}
	 * to create the template hierarchy until there are no children left. As a
	 * consequence, there will be a {@link Template} for each {@link EObject}
	 * contained by the specified <code>object</code>.
	 * </p>
	 * 
	 * <p>
	 * After the {@link Template} hierarchy is created, for each created
	 * {@link Template} {@link FeatureCondition}s will be derived.
	 * </p>
	 * 
	 * @param object
	 *            {@link EObject} to create a {@link ConditionsModel} for.
	 * @return the created {@link ConditionsModel} with the containing
	 *         {@link Template}s and their {@link FeatureCondition}s.
	 */
	@Override
	public ConditionsModel generateConditionsModel(EObject object) {
		// set template generation mode
		this.templateGenerationMode = TemplateGenerationMode.STANDALONE;

		// create ConditionsModel
		ConditionsModel conditionsModel = createEmptyConditionsModel(object);

		// create root template
		Template rootTemplate = createTemplate(object);

		// add root template to conditions model
		conditionsModel.setRootTemplate(rootTemplate);

		// create sub templates
		deriveSubTemplates(rootTemplate, true);

		// create conditions
		deriveConditions(rootTemplate,
				getBestFeatureConditionGenerator(object), true);

		clearEmptyConditions(rootTemplate);

		// return conditions model
		return conditionsModel;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConditionsModel generateRefinedConditionsModel(EObject object,
			ConditionsModel baseConditionsModel,
			ComparisonResourceSnapshot differences) {
		// set fields
		this.baseConditionsModel = baseConditionsModel;
		this.refinementDifferences = differences;

		// set template generation mode
		this.templateGenerationMode = TemplateGenerationMode.REFINEMENT;

		// create ConditionsModel
		ConditionsModel conditionsModel = createEmptyConditionsModel(object);

		// create root template
		Template rootTemplate = createRefinementTemplate(object,
				baseConditionsModel.getRootTemplate());

		// add root template to conditions model
		conditionsModel.setRootTemplate(rootTemplate);

		// create sub templates
		deriveSubTemplates(rootTemplate, true);

		// create conditions
		deriveRefinementConditions(rootTemplate,
				getBestFeatureConditionGenerator(object), true);

		clearEmptyConditions(rootTemplate);

		return conditionsModel;
	}

	/**
	 * Creates an empty {@link ConditionsModel} for the specified root
	 * <code>object</code>.
	 * 
	 * @param object
	 *            the root object.
	 * @return the empty {@link ConditionsModel}.
	 */
	private ConditionsModel createEmptyConditionsModel(EObject object) {
		ConditionsModel conditionsModel = ConditionsFactory.eINSTANCE
				.createConditionsModel();
		conditionsModel.setCreatedAt(new Date());
		conditionsModel.setLanguage(OCLLiterals.CONDITION_LANGUAGE);
		if (object.eResource() != null) {
			conditionsModel
					.setModelName(object.eResource().getURI().toString());
		}
		return conditionsModel;
	}

	/**
	 * Returns the best {@link IFeatureConditionGenerator} for the specified
	 * <code>object</code>.
	 * 
	 * @param object
	 *            {@link EObject} to get best {@link IFeatureConditionGenerator}
	 *            for.
	 * @return best {@link IFeatureConditionGenerator}.
	 */
	private IFeatureConditionGenerator getBestFeatureConditionGenerator(
			EObject object) {
		IFeatureConditionGenerator fcgToUse = null;
		IFeatureConditionGenerator generalFcgToUse = null;

		// iterate through all configured Feature Condition Generators and
		// choose matching and general
		for (IFeatureConditionGenerator featureConditionGenerator : this.featureConditionGenerators) {
			for (String uri : featureConditionGenerator.getMetaModelNamespace()) {
				if (uri.equals(object.eClass().getEPackage().getNsURI())
						&& featureConditionGenerator.getConditionLanguage()
								.equals(this.getConditionLanguage())) {
					fcgToUse = featureConditionGenerator;
				} else if (uri.equals(IFeatureConditionGenerator.GENERAL)
						&& featureConditionGenerator.getConditionLanguage()
								.equals(this.getConditionLanguage())) {
					generalFcgToUse = featureConditionGenerator;
				}
			}
		}

		// if specific feature condition generator is found return it
		// if not return general one
		if (fcgToUse != null) {
			return fcgToUse;
		} else if (generalFcgToUse != null) {
			return generalFcgToUse;
		} else {
			throw new RuntimeException(
					NO_GENERAL_FEATURE_CONDITION_GENERATOR_CONFIGURED);
		}
	}

	/**
	 * Derives the {@link FeatureCondition}s for the specified
	 * <code>template</code>.
	 * 
	 * <p>
	 * For each {@link EAttribute} returned by
	 * <code>template.getRepresentative().eClass().getEAllAttributes()</code>
	 * and each {@link EReference} returned by
	 * <code>template.getRepresentative().eClass().getEAllReferences()</code>
	 * (excluding all {@link EReference}s of
	 * <code>template.getRepresentative().eClass().getEAllContainments()</code>)
	 * a {@link FeatureConditionImpl} will be created.
	 * </p>
	 * 
	 * <p>
	 * If <code>recursive</code> is <code>true</code>, for each sub template
	 * {@link #deriveConditions(Template, boolean)} will be called to create the
	 * conditions of sub templates recursively.
	 * </p>
	 * 
	 * @param template
	 *            {@link Template} to create {@link FeatureCondition}s for.
	 * @param featureConditionGenerator
	 *            {@link IFeatureConditionGenerator} to use.
	 * @param recursive
	 *            if <code>true</code> this method derives
	 *            {@link FeatureCondition}s for all sub conditions recursively.
	 */
	private void deriveConditions(Template template,
			IFeatureConditionGenerator featureConditionGenerator,
			boolean recursive) {
		// guard null representatives
		if (template.getRepresentative() == null)
			return;

		// create a list of features
		EList<EStructuralFeature> features = new BasicEList<EStructuralFeature>();

		// add attributes
		features.addAll(template.getRepresentative().eClass()
				.getEAllAttributes());

		// add references which are not included as containment
		features.addAll(template.getRepresentative().eClass()
				.getEAllReferences());
		features.removeAll(template.getRepresentative().eClass()
				.getEAllContainments());

		// create conditions for each feature in list
		for (EStructuralFeature feature : features) {
			List<FeatureCondition> condition = featureConditionGenerator
					.generateFeatureCondition(feature, template);
			if (condition != null) {
				template.getSpecifications().addAll(condition);
			}
		}

		// recurse if told to do so
		if (recursive) {
			for (Template subTemplate : template.getSubTemplates()) {
				deriveConditions(subTemplate, featureConditionGenerator,
						recursive);
			}
		}
	}

	/**
	 * Derives the refinement {@link FeatureCondition}s for the specified
	 * <code>template</code>.
	 * 
	 * <p>
	 * If the specified template is a {@link RefinementTemplate}, this method
	 * uses {@link #refinementDifferences} as trigger for creating the
	 * conditions. If it is a usual {@link Template}, the generation works
	 * equally to
	 * {@link #deriveConditions(Template, IFeatureConditionGenerator, boolean)}.
	 * </p>
	 * 
	 * <p>
	 * If <code>recursive</code> is <code>true</code>, for each sub template
	 * {@link #deriveConditions(Template, boolean)} will be called to create the
	 * conditions of sub templates recursively.
	 * </p>
	 * 
	 * @param template
	 *            {@link Template} to create {@link FeatureCondition}s for.
	 * @param featureConditionGenerator
	 *            {@link IFeatureConditionGenerator} to use.
	 * @param recursive
	 *            if <code>true</code> this method derives
	 *            {@link FeatureCondition}s for all sub conditions recursively.
	 */
	private void deriveRefinementConditions(Template template,
			IFeatureConditionGenerator featureConditionGenerator,
			boolean recursive) {
		// guard null representatives
		if (template.getRepresentative() == null)
			return;

		// redirect if in standalone mode
		if (TemplateGenerationMode.STANDALONE.equals(templateGenerationMode)) {
			deriveConditions(template, featureConditionGenerator, recursive);
			return;
		}

		// generate conditions (refinement or normal)
		if (template instanceof RefinementTemplate) {
			EList<DiffElement> diffElements = DiffUtil
					.getEffectiveDiffElements(template.getRepresentative(),
							refinementDifferences.getDiff());
			for (DiffElement diffElement : diffElements) {
				if (diffElement instanceof AttributeChange) {
					AttributeChange attributeChange = (AttributeChange) diffElement;
					EAttribute attribute = attributeChange.getAttribute();
					List<FeatureCondition> conditions = featureConditionGenerator
							.generateFeatureCondition(attribute, template);
					if (conditions != null) {
						ConditionsUtil.setActive(conditions);
						template.getSpecifications().addAll(conditions);
					}
				} else if (diffElement instanceof ReferenceChange) {
					ReferenceChange referenceChange = (ReferenceChange) diffElement;
					EReference reference = referenceChange.getReference();
					List<FeatureCondition> conditions = featureConditionGenerator
							.generateFeatureCondition(reference, template);
					if (conditions != null) {
						ConditionsUtil.setActive(conditions);
						template.getSpecifications().addAll(conditions);
					}
				}
			}
		} else {
			deriveConditions(template, featureConditionGenerator, false);
		}

		// recurse if told to do so
		if (recursive) {
			for (Template subTemplate : template.getSubTemplates()) {
				deriveRefinementConditions(subTemplate,
						featureConditionGenerator, recursive);
			}
		}

	}

	/**
	 * Derives the sub {@link Template}s for the specified <code>template</code>
	 * .
	 * 
	 * <p>
	 * For each element returned by {@link #eGet(EStructuralFeature)} with each
	 * {@link EReference} in
	 * <code>representative.eClass().getEAllContainments()</code> a new sub
	 * template will be created.
	 * </p>
	 * 
	 * <p>
	 * If <code>recursive</code> is <code>true</code>, for each sub template
	 * {@link #deriveSubTemplates(Template, boolean)} will be called to create
	 * sub sub templates recursively.
	 * </p>
	 * 
	 * @param template
	 *            {@link Template} to create sub {@link Template}s for.
	 * @param recursive
	 *            if <code>true</code> this method will add sub sub
	 *            {@link Template}s recursively.
	 */
	@SuppressWarnings("unchecked")
	private void deriveSubTemplates(Template template, boolean recursive) {
		// guard null representatives
		if (template.getRepresentative() == null)
			return;

		// loop through all features of representative
		for (EReference feature : template.getRepresentative().eClass()
				.getEAllContainments()) {

			// get value(s) of object at the current feature
			Object value = template.getRepresentative().eGet(feature);
			// guard if value is not null
			if (value != null) {
				if (feature.isMany()) {
					EList<EObject> valueList;

					// guard if value is a list with more than 0 elements
					if (value instanceof EList
							&& (valueList = (EList<EObject>) value).size() > 0) {

						// create sub template for each object in value list
						for (EObject object : valueList) {
							if (!(object instanceof EGenericType)) {
								Template subTemplate = createSubTemplate(
										object, template, feature);
								if (recursive) {
									deriveSubTemplates(subTemplate, recursive);
								}
							}
						}
					}
				} else {
					if (!(value instanceof EGenericType)) {
						EObject object = (EObject) value;
						Template subTemplate = createSubTemplate(object,
								template, feature);
						if (recursive) {
							deriveSubTemplates(subTemplate, recursive);
						}
					}

				}
			}
		}
	}

	/**
	 * Creates a {@link Template} for the specified <code>object</code> as
	 * subtemplate of the specified <code>parentTemplate</code>.
	 * 
	 * <p>
	 * This method also sets the {@link State}, the
	 * {@link Template#setName(String)}, the
	 * {@link Template#setRepresentative(EObject)}, the
	 * {@link Template#setTitle(String)} as well as the
	 * {@link Template#setParentsFeature(EStructuralFeature)} and adds it self
	 * to its parent template.
	 * </p>
	 * 
	 * <p>
	 * If this generator is in {@link TemplateGenerationMode#REFINEMENT
	 * refinement mode} it will try to find its base template and generates a
	 * {@link RefinementTemplate}.
	 * 
	 * @param object
	 *            the {@link EObject} to create {@link Template} for.
	 * @param parentTemplate
	 *            the parent template
	 * @param feature
	 *            the parent feature.
	 * @return the created {@link Template}.
	 */
	private Template createSubTemplate(EObject object, Template parentTemplate,
			EReference feature) {
		Template subTemplate = null;
		Template baseTemplate = null;

		if ((baseTemplate = getTemplateToRefine(object)) != null) {
			subTemplate = createRefinementTemplate(object, baseTemplate);
		} else {
			subTemplate = createTemplate(object);
		}
		// add sub template to template
		parentTemplate.getSubTemplates().add(subTemplate);
		// set the parents feature
		subTemplate.setParentsFeature(feature);
		return subTemplate;
	}

	/**
	 * Tries to find the base template, i.e., the template to be refined. If no
	 * base template could be found, this method returns <code>null</code>.
	 * 
	 * @param object
	 *            to get the template to refine for.
	 * @return the base template or <code>null</code>.
	 */
	private Template getTemplateToRefine(EObject object) {
		if (TemplateGenerationMode.REFINEMENT
				.equals(this.templateGenerationMode)
				&& this.baseConditionsModel != null
				&& this.refinementDifferences != null) {
			EObject matchingObject = MatchUtil.getMatchingObject(object,
					refinementDifferences.getMatch());
			if (matchingObject != null) {
				Template baseTemplate = ConditionsUtil
						.getTemplateByRepresentative(matchingObject,
								baseConditionsModel);
				if (baseTemplate != null) {
					return baseTemplate;
				}
			}

		}
		return null;
	}

	/**
	 * Clears the empty conditions out of the conditions of the specified
	 * <code>rootTemplate</code> and its children conditions.
	 * 
	 * @param rootTemplate
	 *            to clear empty conditions.
	 */
	private void clearEmptyConditions(Template rootTemplate) {
		EList<FeatureCondition> conditionsToRemove = new BasicEList<FeatureCondition>();

		// find empty conditions
		TreeIterator<EObject> objectIter = rootTemplate.eAllContents();
		while (objectIter.hasNext()) {
			EObject object = objectIter.next();
			if (object instanceof FeatureCondition) {
				FeatureCondition featureCondition = (FeatureCondition) object;
				if (featureCondition.getExpression().length() == 0) {
					conditionsToRemove.add(featureCondition);
				}
			}
		}

		// remove empty conditions
		for (FeatureCondition featureCondition : conditionsToRemove) {
			featureCondition.getTemplate().getSpecifications()
					.remove(featureCondition);
		}

	}

	/**
	 * Creates a {@link Template} for the specified <code>object</code>.
	 * 
	 * <p>
	 * This method also sets the {@link State}, the
	 * {@link Template#setName(String)}, the
	 * {@link Template#setRepresentative(EObject)} and the
	 * {@link Template#setTitle(String)}.
	 * </p>
	 * 
	 * @param object
	 *            the {@link EObject} to create {@link Template} for.
	 * @return the created {@link Template}.
	 */
	private Template createTemplate(EObject object) {
		Template template = ConditionsFactory.eINSTANCE.createTemplate();
		template.setActive(true);
		template.setRepresentative(object);
		template.setState(State.GENERATED);
		template.setName(createTemplateName(object));
		template.setTitle(createTemplateTitle(object));
		return template;
	}

	/**
	 * Creates a {@link RefinementTemplate} for the specified
	 * <code>object</code> refining the specified <code>baseTemplate</code>.
	 * 
	 * @param object
	 *            the {@link EObject} to create {@link RefinementTemplate} for.
	 * @param baseTemplate
	 *            the base template of the refinement template.
	 * @return the created refinement template.
	 */
	private RefinementTemplate createRefinementTemplate(EObject object,
			Template baseTemplate) {
		RefinementTemplate template = ConditionsFactory.eINSTANCE
				.createRefinementTemplate();
		template.setActive(true);
		template.setRefinedTemplate(baseTemplate);
		template.setRepresentative(object);
		template.setState(State.GENERATED);
		template.setName(baseTemplate.getName());
		template.setTitle(createTemplateTitle(object));
		return template;
	}

	/**
	 * Creates a name for a {@link Template} using the
	 * {@link TemplateNameService}.
	 * 
	 * @param object
	 *            {@link EObject} to create name for.
	 * @return the created name.
	 */
	private String createTemplateName(EObject object) {
		if (this.templateNameService == null) {
			this.templateNameService = new TemplateNameService();
		}
		String name = this.templateNameService.createName(object);
		if (TemplateGenerationMode.REFINEMENT.equals(templateGenerationMode)) {
			name = REFINEMENT_PREFIX + name;
		}
		return name;
	}

	/**
	 * Creates a title for a {@link Template} using the
	 * {@link TemplateNameService}.
	 * 
	 * @param object
	 *            {@link EObject} to create title for.
	 * @return the created title.
	 */
	private String createTemplateTitle(EObject object) {
		if (this.templateNameService == null) {
			this.templateNameService = new TemplateNameService();
		}
		return this.templateNameService.createTitle(object);
	}
}
