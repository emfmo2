/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines.impl;

/**
 * Contains literals for OCL generation and evaluation.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OCLLiterals {

	/**
	 * Supported conditions language.
	 */
	public static final String CONDITION_LANGUAGE = "OCL";

	public static final String EQUAL = " = ";

	public static final String ENUM_SEP = "::";

	public static final String APO = "'";

	public static final String TRUE = "true";

	public static final String IS_EMPTY = "->isEmpty()";

	public static final String INCLUDES_START = "->includes(";

	public static final String INCLUDES_END = ")";

	// public static final String INCLUDES_ALL_START = "->includesAll(";

	// public static final String INCLUDES_ALL_END = ")";

	public static final String EXISTS_START = "->exists(";

	public static final String EXISTS_END = ")";

	public static final String TYPE_DELIM = " : ";

	public static final String SELF = "self";

	public static final String AS_SEQUENCE = "->asSequence()";

	public static final String DOT = ".";

	public static final String SEQUENCE_START = "Sequence(";

	public static final String SEQUENCE_END = ")";

	public static final String AS_TYPE_START = "oclAsType(";

	public static final String AS_TYPE_END = ")";

	public static final String SELECT_START = "->select(";

	public static final String SELECT_END = ")";

	public static final String AND = " and ";

	public static final String E_CONTAINER = "eContainer()";

	public static final String E_CONTENTS = "eContents()";

	public static final String AS_EOBJECT = "";

	public static final String OCL_IS_TYPE_OF_START = "oclIsTypeOf(";

	public static final String OCL_IS_TYPE_OF_END = ")";

	public static final String FIRST = "->first()";

	public static final String AT_START = "->at(";

	public static final String AT_END = ")";

	public static final String NULL = "null";

	public static final String OR = " or ";

	public static final String PACKAGE_SEP = "::";

}
