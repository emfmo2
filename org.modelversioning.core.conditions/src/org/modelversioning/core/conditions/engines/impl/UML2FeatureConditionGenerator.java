/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines.impl;

import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.ocl.ecore.OCL;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.IFeatureConditionGenerator;

/**
 * {@link IFeatureConditionGenerator} for UML2 models.
 * 
 * <p>
 * Basically this generator produces the same {@link FeatureCondition}s like its
 * super class {@link GenericFeatureConditionGenerator} but omits the generation
 * of conditions for the feature <code>package</code> and <code>context</code>
 * as these features cannot be evaluated directly using {@link OCL}.
 * </p>
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class UML2FeatureConditionGenerator extends
		GenericFeatureConditionGenerator implements IFeatureConditionGenerator {

	/**
	 * Namespace URI of the meta model this generator is spezialized for.
	 */
	private static final List<String> metaModelNamespace = Arrays.asList(
			"http://www.eclipse.org/uml2/2.1.0/UML", "http://www.eclipse.org/uml2/3.0.0/UML");

	/**
	 * Names of features to omit.
	 */
	private static final List<String> OMIT_FEATURE_NAMES = Arrays.asList(
			"package", "context");

	/**
	 * Overrides the default behavior of the super class
	 * {@link GenericFeatureConditionGenerator#generateFeatureCondition(EStructuralFeature, Template)}
	 * in order to omit some features for which a {@link FeatureCondition} must
	 * not be generated in the case of the UML2 metamodel.
	 * 
	 * @see GenericFeatureConditionGenerator#generateFeatureCondition(org.eclipse.emf.ecore.EStructuralFeature,
	 *      org.modelversioning.core.conditions.Template)
	 */
	@Override
	public List<FeatureCondition> generateFeatureCondition(
			EStructuralFeature feature, Template template) {
		if (OMIT_FEATURE_NAMES.contains(feature.getName())) {
			return null;
		} else {
			return super.generateFeatureCondition(feature, template);
		}
	}

	/**
	 * Returns the UML2 metamodel namespace URIs.
	 * 
	 * @return the UML2 metamodel namespace URIs.
	 */
	@Override
	public List<String> getMetaModelNamespace() {
		return metaModelNamespace;
	}

}
