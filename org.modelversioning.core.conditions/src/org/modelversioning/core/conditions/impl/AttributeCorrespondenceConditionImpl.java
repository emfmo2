/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.modelversioning.core.conditions.AttributeCorrespondenceCondition;
import org.modelversioning.core.conditions.ConditionsPackage;
import org.modelversioning.core.conditions.Template;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Correspondence Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.impl.AttributeCorrespondenceConditionImpl#getCorrespondingTemplates <em>Corresponding Templates</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.AttributeCorrespondenceConditionImpl#getCorrespondingFeatures <em>Corresponding Features</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeCorrespondenceConditionImpl extends FeatureConditionImpl implements AttributeCorrespondenceCondition {
	/**
	 * The cached value of the '{@link #getCorrespondingTemplates() <em>Corresponding Templates</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondingTemplates()
	 * @generated
	 * @ordered
	 */
	protected EList<Template> correspondingTemplates;

	/**
	 * The cached value of the '{@link #getCorrespondingFeatures() <em>Corresponding Features</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondingFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<EStructuralFeature> correspondingFeatures;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeCorrespondenceConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.Literals.ATTRIBUTE_CORRESPONDENCE_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Template> getCorrespondingTemplates() {
		if (correspondingTemplates == null) {
			correspondingTemplates = new EObjectResolvingEList<Template>(Template.class, this, ConditionsPackage.ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_TEMPLATES);
		}
		return correspondingTemplates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EStructuralFeature> getCorrespondingFeatures() {
		if (correspondingFeatures == null) {
			correspondingFeatures = new EObjectResolvingEList<EStructuralFeature>(EStructuralFeature.class, this, ConditionsPackage.ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_FEATURES);
		}
		return correspondingFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_TEMPLATES:
				return getCorrespondingTemplates();
			case ConditionsPackage.ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_FEATURES:
				return getCorrespondingFeatures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_TEMPLATES:
				getCorrespondingTemplates().clear();
				getCorrespondingTemplates().addAll((Collection<? extends Template>)newValue);
				return;
			case ConditionsPackage.ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_FEATURES:
				getCorrespondingFeatures().clear();
				getCorrespondingFeatures().addAll((Collection<? extends EStructuralFeature>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_TEMPLATES:
				getCorrespondingTemplates().clear();
				return;
			case ConditionsPackage.ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_FEATURES:
				getCorrespondingFeatures().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_TEMPLATES:
				return correspondingTemplates != null && !correspondingTemplates.isEmpty();
			case ConditionsPackage.ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_FEATURES:
				return correspondingFeatures != null && !correspondingFeatures.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AttributeCorrespondenceConditionImpl
