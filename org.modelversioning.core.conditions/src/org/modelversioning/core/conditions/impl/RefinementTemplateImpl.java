/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package org.modelversioning.core.conditions.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.modelversioning.core.conditions.ConditionsPackage;
import org.modelversioning.core.conditions.RefinementTemplate;
import org.modelversioning.core.conditions.Template;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Refinement Template</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.impl.RefinementTemplateImpl#getRefinedTemplate <em>Refined Template</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RefinementTemplateImpl extends TemplateImpl implements RefinementTemplate {
	/**
	 * The cached value of the '{@link #getRefinedTemplate() <em>Refined Template</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefinedTemplate()
	 * @generated
	 * @ordered
	 */
	protected Template refinedTemplate;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RefinementTemplateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.Literals.REFINEMENT_TEMPLATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template getRefinedTemplate() {
		if (refinedTemplate != null && refinedTemplate.eIsProxy()) {
			InternalEObject oldRefinedTemplate = (InternalEObject)refinedTemplate;
			refinedTemplate = (Template)eResolveProxy(oldRefinedTemplate);
			if (refinedTemplate != oldRefinedTemplate) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.REFINEMENT_TEMPLATE__REFINED_TEMPLATE, oldRefinedTemplate, refinedTemplate));
			}
		}
		return refinedTemplate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template basicGetRefinedTemplate() {
		return refinedTemplate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefinedTemplate(Template newRefinedTemplate) {
		Template oldRefinedTemplate = refinedTemplate;
		refinedTemplate = newRefinedTemplate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.REFINEMENT_TEMPLATE__REFINED_TEMPLATE, oldRefinedTemplate, refinedTemplate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.REFINEMENT_TEMPLATE__REFINED_TEMPLATE:
				if (resolve) return getRefinedTemplate();
				return basicGetRefinedTemplate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.REFINEMENT_TEMPLATE__REFINED_TEMPLATE:
				setRefinedTemplate((Template)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.REFINEMENT_TEMPLATE__REFINED_TEMPLATE:
				setRefinedTemplate((Template)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.REFINEMENT_TEMPLATE__REFINED_TEMPLATE:
				return refinedTemplate != null;
		}
		return super.eIsSet(featureID);
	}

} //RefinementTemplateImpl
