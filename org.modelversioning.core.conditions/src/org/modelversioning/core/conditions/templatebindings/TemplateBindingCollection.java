/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions.templatebindings;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Binding Collection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection#getBindings <em>Bindings</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.core.conditions.templatebindings.TemplatebindingsPackage#getTemplateBindingCollection()
 * @model
 * @generated
 */
public interface TemplateBindingCollection extends EObject {
	/**
	 * Returns the value of the '<em><b>Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.modelversioning.core.conditions.templatebindings.TemplateBinding}.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bindings</em>' containment reference list.
	 * @see org.modelversioning.core.conditions.templatebindings.TemplatebindingsPackage#getTemplateBindingCollection_Bindings()
	 * @see org.modelversioning.core.conditions.templatebindings.TemplateBinding#getCollection
	 * @model opposite="collection" containment="true"
	 * @generated
	 */
	EList<TemplateBinding> getBindings();

} // TemplateBindingCollection
