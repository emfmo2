/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions.templatebindings;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.modelversioning.core.conditions.templatebindings.TemplatebindingsFactory
 * @model kind="package"
 * @generated
 */
public interface TemplatebindingsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "templatebindings";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://modelversioning.org/core/templateBindings/metamodel/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "templatebindings";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TemplatebindingsPackage eINSTANCE = org.modelversioning.core.conditions.templatebindings.impl.TemplatebindingsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingCollectionImpl <em>Template Binding Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingCollectionImpl
	 * @see org.modelversioning.core.conditions.templatebindings.impl.TemplatebindingsPackageImpl#getTemplateBindingCollection()
	 * @generated
	 */
	int TEMPLATE_BINDING_COLLECTION = 0;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_BINDING_COLLECTION__BINDINGS = 0;

	/**
	 * The number of structural features of the '<em>Template Binding Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_BINDING_COLLECTION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingImpl <em>Template Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingImpl
	 * @see org.modelversioning.core.conditions.templatebindings.impl.TemplatebindingsPackageImpl#getTemplateBinding()
	 * @generated
	 */
	int TEMPLATE_BINDING = 1;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_BINDING__COLLECTION = 0;

	/**
	 * The feature id for the '<em><b>EObjects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_BINDING__EOBJECTS = 1;

	/**
	 * The feature id for the '<em><b>Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_BINDING__TEMPLATE = 2;

	/**
	 * The feature id for the '<em><b>Template Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_BINDING__TEMPLATE_NAME = 3;

	/**
	 * The number of structural features of the '<em>Template Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_BINDING_FEATURE_COUNT = 4;


	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection <em>Template Binding Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Template Binding Collection</em>'.
	 * @see org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection
	 * @generated
	 */
	EClass getTemplateBindingCollection();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection#getBindings <em>Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bindings</em>'.
	 * @see org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection#getBindings()
	 * @see #getTemplateBindingCollection()
	 * @generated
	 */
	EReference getTemplateBindingCollection_Bindings();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding <em>Template Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Template Binding</em>'.
	 * @see org.modelversioning.core.conditions.templatebindings.TemplateBinding
	 * @generated
	 */
	EClass getTemplateBinding();

	/**
	 * Returns the meta object for the container reference '{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Collection</em>'.
	 * @see org.modelversioning.core.conditions.templatebindings.TemplateBinding#getCollection()
	 * @see #getTemplateBinding()
	 * @generated
	 */
	EReference getTemplateBinding_Collection();

	/**
	 * Returns the meta object for the reference list '{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getEObjects <em>EObjects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>EObjects</em>'.
	 * @see org.modelversioning.core.conditions.templatebindings.TemplateBinding#getEObjects()
	 * @see #getTemplateBinding()
	 * @generated
	 */
	EReference getTemplateBinding_EObjects();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getTemplate <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Template</em>'.
	 * @see org.modelversioning.core.conditions.templatebindings.TemplateBinding#getTemplate()
	 * @see #getTemplateBinding()
	 * @generated
	 */
	EReference getTemplateBinding_Template();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getTemplateName <em>Template Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Template Name</em>'.
	 * @see org.modelversioning.core.conditions.templatebindings.TemplateBinding#getTemplateName()
	 * @see #getTemplateBinding()
	 * @generated
	 */
	EAttribute getTemplateBinding_TemplateName();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TemplatebindingsFactory getTemplatebindingsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingCollectionImpl <em>Template Binding Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingCollectionImpl
		 * @see org.modelversioning.core.conditions.templatebindings.impl.TemplatebindingsPackageImpl#getTemplateBindingCollection()
		 * @generated
		 */
		EClass TEMPLATE_BINDING_COLLECTION = eINSTANCE.getTemplateBindingCollection();

		/**
		 * The meta object literal for the '<em><b>Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_BINDING_COLLECTION__BINDINGS = eINSTANCE.getTemplateBindingCollection_Bindings();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingImpl <em>Template Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingImpl
		 * @see org.modelversioning.core.conditions.templatebindings.impl.TemplatebindingsPackageImpl#getTemplateBinding()
		 * @generated
		 */
		EClass TEMPLATE_BINDING = eINSTANCE.getTemplateBinding();

		/**
		 * The meta object literal for the '<em><b>Collection</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_BINDING__COLLECTION = eINSTANCE.getTemplateBinding_Collection();

		/**
		 * The meta object literal for the '<em><b>EObjects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_BINDING__EOBJECTS = eINSTANCE.getTemplateBinding_EObjects();

		/**
		 * The meta object literal for the '<em><b>Template</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE_BINDING__TEMPLATE = eINSTANCE.getTemplateBinding_Template();

		/**
		 * The meta object literal for the '<em><b>Template Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE_BINDING__TEMPLATE_NAME = eINSTANCE.getTemplateBinding_TemplateName();

	}

} //TemplatebindingsPackage
