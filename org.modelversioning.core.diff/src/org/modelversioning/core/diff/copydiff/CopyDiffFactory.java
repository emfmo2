/**
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.modelversioning.core.diff.copydiff;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.modelversioning.core.diff.copydiff.CopyDiffPackage
 * @generated
 */
public interface CopyDiffFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CopyDiffFactory eINSTANCE = org.modelversioning.core.diff.copydiff.impl.CopyDiffFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Copy Element Right Target</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Copy Element Right Target</em>'.
	 * @generated
	 */
	CopyElementRightTarget createCopyElementRightTarget();

	/**
	 * Returns a new object of class '<em>Copy Element Left Target</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Copy Element Left Target</em>'.
	 * @generated
	 */
	CopyElementLeftTarget createCopyElementLeftTarget();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CopyDiffPackage getCopyDiffPackage();

} //CopyDiffFactory
