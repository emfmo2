/**
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.modelversioning.core.diff.copydiff;

import org.eclipse.emf.compare.diff.metamodel.ModelElementChangeRightTarget;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Copy Element Right Target</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A "RightTarget" element copy describes a difference involving the left element/resource. In the case of CopyElements, this can describe either the copy of an element or the remote removal of the copy (for three way comparisons).
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.core.diff.copydiff.CopyElementRightTarget#getCopiedLeftElement <em>Copied Left Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.core.diff.copydiff.CopyDiffPackage#getCopyElementRightTarget()
 * @model
 * @generated
 */
public interface CopyElementRightTarget extends ModelElementChangeRightTarget {

	/**
	 * Returns the value of the '<em><b>Copied Left Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Copied Left Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copied Left Element</em>' reference.
	 * @see #setCopiedLeftElement(EObject)
	 * @see org.modelversioning.core.diff.copydiff.CopyDiffPackage#getCopyElementRightTarget_CopiedLeftElement()
	 * @model
	 * @generated
	 */
	EObject getCopiedLeftElement();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.diff.copydiff.CopyElementRightTarget#getCopiedLeftElement <em>Copied Left Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Copied Left Element</em>' reference.
	 * @see #getCopiedLeftElement()
	 * @generated
	 */
	void setCopiedLeftElement(EObject value);
} // CopyElementRightTarget
