/**
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.modelversioning.core.diff.copydiff.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.compare.diff.metamodel.impl.ModelElementChangeLeftTargetImpl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.modelversioning.core.diff.copydiff.CopyDiffPackage;
import org.modelversioning.core.diff.copydiff.CopyElementLeftTarget;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Copy Element Left Target</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.core.diff.copydiff.impl.CopyElementLeftTargetImpl#getCopiedRightElement <em>Copied Right Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CopyElementLeftTargetImpl extends ModelElementChangeLeftTargetImpl implements CopyElementLeftTarget {
	/**
	 * The cached value of the '{@link #getCopiedRightElement() <em>Copied Right Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCopiedRightElement()
	 * @generated
	 * @ordered
	 */
	protected EObject copiedRightElement;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CopyElementLeftTargetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CopyDiffPackage.Literals.COPY_ELEMENT_LEFT_TARGET;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getCopiedRightElement() {
		if (copiedRightElement != null && copiedRightElement.eIsProxy()) {
			InternalEObject oldCopiedRightElement = (InternalEObject)copiedRightElement;
			copiedRightElement = eResolveProxy(oldCopiedRightElement);
			if (copiedRightElement != oldCopiedRightElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyDiffPackage.COPY_ELEMENT_LEFT_TARGET__COPIED_RIGHT_ELEMENT, oldCopiedRightElement, copiedRightElement));
			}
		}
		return copiedRightElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetCopiedRightElement() {
		return copiedRightElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCopiedRightElement(EObject newCopiedRightElement) {
		EObject oldCopiedRightElement = copiedRightElement;
		copiedRightElement = newCopiedRightElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyDiffPackage.COPY_ELEMENT_LEFT_TARGET__COPIED_RIGHT_ELEMENT, oldCopiedRightElement, copiedRightElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CopyDiffPackage.COPY_ELEMENT_LEFT_TARGET__COPIED_RIGHT_ELEMENT:
				if (resolve) return getCopiedRightElement();
				return basicGetCopiedRightElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CopyDiffPackage.COPY_ELEMENT_LEFT_TARGET__COPIED_RIGHT_ELEMENT:
				setCopiedRightElement((EObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CopyDiffPackage.COPY_ELEMENT_LEFT_TARGET__COPIED_RIGHT_ELEMENT:
				setCopiedRightElement((EObject)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CopyDiffPackage.COPY_ELEMENT_LEFT_TARGET__COPIED_RIGHT_ELEMENT:
				return copiedRightElement != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @generated NOT
	 * @see org.eclipse.emf.compare.diff.metamodel.impl.DiffElementImpl#toString()
	 */
	@Override
	public String toString() {
		if (isRemote())
			return leftElement + " has been remotely removed";
		return "A copy of " + leftElement + " has been added";
	}

} //CopyElementLeftTargetImpl
