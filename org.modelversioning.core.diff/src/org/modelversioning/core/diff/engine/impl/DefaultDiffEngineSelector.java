/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.diff.engine.impl;

import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.modelversioning.core.diff.engine.IDiffEngine;
import org.modelversioning.core.diff.engine.IDiffEngineSelector;

/**
 * The default diff engine selector selecting only the {@link EMFCompareDiffEngine}.
 * 
 * <p>
 * This indirection class is built for the easy extensibility in the future.
 * </p>
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 *
 */
public class DefaultDiffEngineSelector implements IDiffEngineSelector {
	
	/**
	 * The only {@link IDiffEngine} currently used.
	 */
	private IDiffEngine emfCompareDiffEngine = new EMFCompareDiffEngine();

	/* (non-Javadoc)
	 * @see org.modelversioning.core.diff.engine.IDiffEngineSelector#selectEngine(org.eclipse.emf.compare.match.metamodel.MatchModel)
	 */
	@Override
	public IDiffEngine selectEngine(MatchModel matchModel) {
		return emfCompareDiffEngine;
	}

}
