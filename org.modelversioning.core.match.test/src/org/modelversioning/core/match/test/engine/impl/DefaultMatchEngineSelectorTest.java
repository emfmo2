/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.match.test.engine.impl;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;
import org.modelversioning.core.match.engine.IMatchEngine;
import org.modelversioning.core.match.engine.impl.DefaultMatchEngineSelector;
import org.modelversioning.core.match.engine.impl.EMFCompareMatchEngine;
import org.modelversioning.core.match.engine.impl.UUIDMatchEngine;

/**
 * Tests the class {@link DefaultMatchEngineSelector}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 *
 */
public class DefaultMatchEngineSelectorTest extends TestCase {

	Resource origin = null;
	Resource workingCopy1 = null;
	Resource workingCopy2 = null;
	Resource origin_id = null;
	Resource workingCopy1_id = null;
	Resource workingCopy2_id = null;
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		// load non id resources
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("ecore", new EcoreResourceFactoryImpl());
		URI fileURI = URI.createFileURI("models/origin.ecore");
		origin = resourceSet.getResource(fileURI, true);
		URI fileURI1 = URI.createFileURI("models/working_copy_1.ecore");
		workingCopy1 = resourceSet.getResource(fileURI1, true);
		URI fileURI2 = URI.createFileURI("models/working_copy_2.ecore");
		workingCopy2 = resourceSet.getResource(fileURI2, true);
		
		// load id resources
		ResourceSet resourceSet_id = new ResourceSetImpl();
		resourceSet_id.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("ecore", new UUIDResourceFactoryImpl());
		URI fileURI_id = URI.createFileURI("models/origin_ids.ecore");
		origin_id = resourceSet_id.getResource(fileURI_id, true);
		URI fileURI1_id = URI.createFileURI("models/working_copy_1_ids.ecore");
		workingCopy1_id = resourceSet_id.getResource(fileURI1_id, true);
		URI fileURI2_id = URI.createFileURI("models/working_copy_2_ids.ecore");
		workingCopy2_id = resourceSet_id.getResource(fileURI2_id, true);
	}
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	@Override
	protected void tearDown() throws Exception {
		this.origin.unload();
		this.workingCopy1.unload();
		this.workingCopy2.unload();
		this.origin_id.unload();
		this.workingCopy1_id.unload();
		this.workingCopy2_id.unload();
		super.tearDown();
	}

	/**
	 * Test method for {@link org.modelversioning.core.match.engine.impl.DefaultMatchEngineSelector#selectEngine(org.eclipse.emf.ecore.resource.Resource, org.eclipse.emf.ecore.resource.Resource)}.
	 */
	public void testSelectEngineResourceResource() {
		DefaultMatchEngineSelector selector = new DefaultMatchEngineSelector(false);
		// select engine for resources without IDs
		IMatchEngine engine_non_id = selector.selectEngine(workingCopy1, workingCopy2);
		// select engine for resources with IDs
		IMatchEngine engine_id = selector.selectEngine(workingCopy1_id, workingCopy2_id);
		// test whether the right engine is selected
		assertTrue(engine_non_id instanceof EMFCompareMatchEngine);
		assertTrue(engine_id instanceof UUIDMatchEngine);
	}

	/**
	 * Test method for {@link org.modelversioning.core.match.engine.impl.DefaultMatchEngineSelector#selectEngine(org.eclipse.emf.ecore.resource.Resource, org.eclipse.emf.ecore.resource.Resource, org.eclipse.emf.ecore.resource.Resource)}.
	 */
	public void testSelectEngineResourceResourceResource() {
		DefaultMatchEngineSelector selector = new DefaultMatchEngineSelector(false);
		// select engine for resources without IDs
		IMatchEngine engine_non_id = selector.selectEngine(origin, workingCopy1, workingCopy2);
		// select engine for resources with IDs
		IMatchEngine engine_id = selector.selectEngine(origin_id, workingCopy1_id, workingCopy2_id);
		// test whether the right engine is selected
		assertTrue(engine_non_id instanceof EMFCompareMatchEngine);
		assertTrue(engine_id instanceof UUIDMatchEngine);		
	}

}
