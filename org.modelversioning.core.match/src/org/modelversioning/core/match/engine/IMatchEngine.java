/**
 * <copyright>
 *
 * Copyright (c) ${year} modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.match.engine;

import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.modelversioning.core.match.MatchException;

/**
 * Provides interface for creating {@link MatchModel}s out of two or three
 * {@link Resource}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IMatchEngine {

	/**
	 * Specifies whether this match engine accepts to match the specified
	 * <code>eObjects</code>. This method returns <code>0</code> if it won't
	 * accept the <code>eObjects</code>. Otherwise, the higher the returned
	 * value, the higher is the priority to select this match engine.
	 * 
	 * @param eObjects
	 *            all root objects of the models to be matched for consideration
	 *            to decide whether to accept or decline.
	 * @return <code>0</code> if this match engine won't accept the specified
	 *         <code>eObjects</code>, otherwise the priority (the higher the
	 *         better).
	 */
	public int accept(EObject... eObjects);

	/**
	 * Generates a <em>two way</em> {@link MatchModel} between the models in the
	 * specified {@link Resource}s <code>resource1</code> and
	 * <code>resource2</code>.
	 * 
	 * @param resource1
	 *            {@link Resource} of the first model.
	 * @param resource2
	 *            {@link Resource} of the second model.
	 * @return the {@link MatchModel} matching both models.
	 * @throws MatchException
	 *             if the match could not be successfully executes.
	 */
	public MatchModel generateMatchModel(Resource resource1, Resource resource2)
			throws MatchException;

	/**
	 * Generates a <em>two way</em> {@link MatchModel} between the models in the
	 * specified {@link EObject}s <code>eObject1</code> and
	 * <code>eObject2</code>.
	 * 
	 * @param eObject1
	 *            {@link EObject} root of the first model.
	 * @param eObject2
	 *            {@link EObject} root of the second model.
	 * @return the {@link MatchModel} matching both models.
	 * @throws MatchException
	 *             if the match could not be successfully executes.
	 */
	public MatchModel generateMatchModel(EObject eObject1, EObject eObject2)
			throws MatchException;

	/**
	 * Generates a <em>three way</em> {@link MatchModel} between the models in
	 * the specified {@link Resource}s <code>resource1</code> and
	 * <code>resource2</code> as well as their common origin model
	 * <code>origin</code>.
	 * 
	 * @param origin
	 *            {@link Resource} of the common origin model.
	 * @param resource1
	 *            {@link Resource} of the first model.
	 * @param resource2
	 *            {@link Resource} of the second model.
	 * @return the {@link MatchModel} matching the two models and the common
	 *         origin model.
	 * @throws MatchException
	 *             if the match could not be successfully executes.
	 */
	public MatchModel generateMatchModel(Resource origin, Resource resource1,
			Resource resource2) throws MatchException;

}
