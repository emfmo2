/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.util;

import java.io.File;
import java.util.Collection;

import junit.framework.TestCase;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

/**
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ModelDiagramUtilTest extends TestCase {

	ResourceSet resourceSet = new ResourceSetImpl();
	Resource ecoreDiagramResource = null;
	Resource ecoreModelResource = null;
	Resource umlClassDiagramResource = null;
	Resource umlClassModelResource = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		File file = new File("models/test.ecore"); //$NON-NLS-1$
		ecoreModelResource = resourceSet.getResource(URI.createFileURI(file
				.getAbsolutePath()), true);

		file = new File("models/test.ecorediag"); //$NON-NLS-1$
		ecoreDiagramResource = resourceSet.getResource(URI.createFileURI(file
				.getAbsolutePath()), true);

		file = new File("models/test.uml"); //$NON-NLS-1$
		umlClassModelResource = resourceSet.getResource(URI.createFileURI(file
				.getAbsolutePath()), true); //$NON-NLS-1$

		file = new File("models/test.umlclass"); //$NON-NLS-1$
		umlClassDiagramResource = resourceSet.getResource(URI
				.createFileURI(file.getAbsolutePath()), true);
	}

	/**
	 * Test method for
	 * {@link org.modelversioning.core.util.ModelDiagramUtil#containsDiagram(org.eclipse.emf.ecore.resource.Resource)}
	 * .
	 */
	public void testContainsDiagram() {
		assertTrue(ModelDiagramUtil.containsDiagram(ecoreDiagramResource));
		assertFalse(ModelDiagramUtil.containsDiagram(ecoreModelResource));
		assertTrue(ModelDiagramUtil.containsDiagram(umlClassDiagramResource));
		assertFalse(ModelDiagramUtil.containsDiagram(umlClassModelResource));
	}

	/**
	 * Test method for
	 * {@link org.modelversioning.core.util.ModelDiagramUtil#getModelFilesFromDiagram(org.eclipse.emf.ecore.resource.Resource)}
	 * .
	 */
	public void testGetModelFilesFromDiagram() {

		Collection<IFile> modelFilesFromDiagram = ModelDiagramUtil
				.getModelFilesFromDiagram(ecoreDiagramResource);
		assertEquals(1, modelFilesFromDiagram.size());
		assertEquals(
				"test.ecore", modelFilesFromDiagram.iterator().next().getName()); //$NON-NLS-1$

		modelFilesFromDiagram = ModelDiagramUtil
				.getModelFilesFromDiagram(umlClassDiagramResource);
		assertEquals(1, modelFilesFromDiagram.size());
		assertEquals(
				"test.uml", modelFilesFromDiagram.iterator().next().getName()); //$NON-NLS-1$
	}

}
