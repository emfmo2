/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.detection;

import java.util.Set;

import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence;
import org.modelversioning.operations.repository.ModelOperationRepository;

/**
 * Engine detecting occurrences of {@link OperationSpecification}s in
 * {@link DiffModel}s. For detected occurrences instances of
 * {@link OperationOccurrence}s are returned.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IOperationDetectionEngine {

	/**
	 * Sets the repository of {@link OperationSpecification}s to be used for
	 * detecting their occurrences.
	 * 
	 * @param repo
	 *            to use.
	 */
	void setOperationsRepository(ModelOperationRepository repo);

	/**
	 * Sets the {@link IConditionEvaluationEngine} to use.
	 * 
	 * @return {@link IConditionEvaluationEngine} to use.
	 */
	public IConditionEvaluationEngine getEvaluationEngine();

	/**
	 * Returns the currently used {@link IConditionEvaluationEngine}.
	 * 
	 * @param evaluationEngine
	 *            currently used {@link IConditionEvaluationEngine}.
	 */
	public void setEvaluationEngine(IConditionEvaluationEngine evaluationEngine);

	/**
	 * Finds occurrences of {@link OperationSpecification}s contained by the set
	 * repository ({@link #setOperationsRepository(ModelOperationRepository)})
	 * in the specified <code>comparisonSnapshot</code> and returns
	 * {@link OperationOccurrence}s for each.
	 * 
	 * @param comparisonSnapshot
	 *            to search for occurrences in.
	 * @return found occurrences.
	 * @throws UnsupportedConditionLanguage 
	 */
	Set<OperationOccurrence> findOccurrences(
			ComparisonResourceSnapshot comparisonSnapshot) throws UnsupportedConditionLanguage;

}
