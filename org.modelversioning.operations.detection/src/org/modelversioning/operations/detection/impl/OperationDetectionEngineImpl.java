/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.detection.impl;

import java.net.BindException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.ITemplateBindings;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.conditions.engines.impl.ConditionsEvaluationEngineImpl;
import org.modelversioning.core.conditions.engines.impl.TemplateBindingImpl;
import org.modelversioning.core.conditions.templatebindings.util.TemplateBindingsUtil;
import org.modelversioning.core.diff.DiffElementTypeComparator;
import org.modelversioning.core.diff.DiffSignature;
import org.modelversioning.operations.OperationSignature;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.detection.IOperationDetectionEngine;
import org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence;
import org.modelversioning.operations.detection.operationoccurrence.OperationoccurrenceFactory;
import org.modelversioning.operations.repository.ModelOperationRepository;
import org.modelversioning.operations.util.OperationsUtil;

/**
 * Implementation of the {@link IOperationDetectionEngine}.
 * 
 * TODO add inactive, optional, and NEx template support
 * 
 * Changes that affect inactive and non-existence templates should not be
 * considered by the change pattern searching process. For optional templates,
 * we might infer separate operation specifications (the ones without changes
 * concerning option groups, and those including them).
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationDetectionEngineImpl implements IOperationDetectionEngine {

	/**
	 * The repository with {@link OperationSpecification}s to use.
	 */
	private ModelOperationRepository repository = null;

	/**
	 * The temporal (working) set of current operation signatures to check.
	 */
	private Set<OperationSignature> currentOperationSignatures = null;

	/**
	 * The current modeling language.
	 */
	private String currentModelingLanguage;

	/**
	 * The diff element comparator.
	 */
	private DiffElementTypeComparator comparator = DiffElementTypeComparator
			.getInstance();

	/**
	 * Used operation Occurrence Factory
	 */
	private OperationoccurrenceFactory operationOccurrenceFactory = OperationoccurrenceFactory.eINSTANCE;

	/**
	 * The condition evaluation engine to use.
	 */
	private IConditionEvaluationEngine evaluationEngine = null;

	/**
	 * Default constructor.
	 */
	public OperationDetectionEngineImpl() {
		evaluationEngine = new ConditionsEvaluationEngineImpl();
	}

	/**
	 * Constructs a new instance and sets the
	 * <code>conditionEvaluationEngine</code> to use.
	 * 
	 * @param conditionEvaluationEngine
	 *            to use.
	 */
	public OperationDetectionEngineImpl(
			IConditionEvaluationEngine conditionEvaluationEngine) {
		this.evaluationEngine = conditionEvaluationEngine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOperationsRepository(ModelOperationRepository repo) {
		repository = repo;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IConditionEvaluationEngine getEvaluationEngine() {
		return evaluationEngine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEvaluationEngine(IConditionEvaluationEngine evaluationEngine) {
		this.evaluationEngine = evaluationEngine;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws UnsupportedConditionLanguage
	 */
	@Override
	public Set<OperationOccurrence> findOccurrences(
			ComparisonResourceSnapshot comparisonSnapshot)
			throws UnsupportedConditionLanguage {
		// get modeling language
		currentModelingLanguage = getModelingLanguage(comparisonSnapshot);

		// guard null or empty repository for current modeling language
		if (repository == null
				|| repository.getRegisteredOperationSpecifications(
						currentModelingLanguage).size() < 1) {
			return Collections.emptySet();
		}

		// create operation signature for input diff
		DiffSignature inputSignature = new DiffSignature(
				comparisonSnapshot.getDiff());

		// make pre selection of potentially performed operations
		List<OperationSpecification> preSelection = makePreSelection(inputSignature);

		// check preselected occurrences
		Set<OperationOccurrence> performedOperations = checkPreselectedOccurrences(
				inputSignature, preSelection, comparisonSnapshot.getMatch());

		return performedOperations;
	}

	/**
	 * Checks the preselected potential occurrences enumerated in
	 * <code>preSelection</code> in the <code>inputSignature</code>.
	 * 
	 * @param inputSignature
	 *            to check for occurrences.
	 * @param preSelection
	 *            preselected list of potential occurrences.
	 * @param matchModel
	 *            to derive post condition binding from precondition binding.
	 * @return all found occurrences.
	 * @throws UnsupportedConditionLanguage
	 */
	private Set<OperationOccurrence> checkPreselectedOccurrences(
			DiffSignature inputSignature,
			List<OperationSpecification> preSelection, MatchModel matchModel)
			throws UnsupportedConditionLanguage {

		// create an ordered list of operation signatures for specifications in
		// preSelection
		List<OperationSignature> operationSignatures = new ArrayList<OperationSignature>();
		for (OperationSpecification os : preSelection) {
			OperationSignature operationSignature = new OperationSignature(os);
			operationSignatures.add(operationSignature);
		}
		Collections
				.sort(operationSignatures, new DiffSignatureSizeComparator());

		// initialize return set
		Set<OperationOccurrence> performedOperations = new HashSet<OperationOccurrence>();

		for (OperationSignature operationSignature : operationSignatures) {

			// create operation change map
			OperationChangeMap operationChangeMap = new OperationChangeMap(
					operationSignature, inputSignature, matchModel);

			// remove all changes already been used by other detected operations
			for (OperationOccurrence otherOccurrence : performedOperations) {
				operationChangeMap.removeChanges(otherOccurrence);
			}

			// skip if operation is not possible anymore
			if (!operationChangeMap.isOperationPossible()) {
				continue;
			}

			// derive pre condition binding
			ITemplateBinding preconditionCandidateBinding = operationChangeMap
					.derivePreConditionCandidateBinding();
			prepareEvaluationEngine(operationSignature
					.getOperationSpecification().getPreconditions());
			ITemplateBindings preConditionBinding = evaluationEngine
					.findTemplateBinding(createEmptyTemplateBinding(),
							preconditionCandidateBinding);

			// continue to next operation to check if preconditions are not
			// fulfilled
			if (!preConditionBinding.validate().isOK()) {
				continue;
			}

			// split found bindings to a set of unambiguous bindings (there
			// might be more than one occurrence to check)
			Set<ITemplateBindings> uniquePreConditionBindings = OperationsUtil
					.splitToUniqueBindings(preConditionBinding,
							operationSignature.getOperationSpecification());

			// check post conditions for each unique binding
			for (ITemplateBindings uniquePreConditionBinding : uniquePreConditionBindings) {

				// skip if operation is not possible anymore because another
				// unique binding already consumed the changes
				if (!operationChangeMap
						.isOperationPossibleExlcudingOperations(performedOperations)) {
					continue;
				}

				// derive for each the post condition template binding and
				// evaluate them separately
				try {
					ITemplateBinding postconditionCandidateBinding;
					postconditionCandidateBinding = operationChangeMap
							.derivePostConditionBinding(
									uniquePreConditionBinding, matchModel);
					prepareEvaluationEngine(operationSignature
							.getOperationSpecification().getPostconditions());
					
					// register pre-condition binding
					evaluationEngine.registerRelatedTemplateBinding(
							OperationsUtil.INITIAL_PREFIX,
							uniquePreConditionBinding);

					// Postcondition binding should be completely derivable from
					// precondition binding and the change map. Therefore, if it
					// is not complete, reject current occurrence and continue.
					if (!evaluationEngine
							.isComplete(postconditionCandidateBinding)) {
						//continue;
						// TODO there is an issue that sometimes refactorings are not detected.
						// however, for the prec-recall benchmark it was necessary
					}

					ITemplateBindings postConditionBinding = evaluationEngine
							.findTemplateBinding(createEmptyTemplateBinding(),
									postconditionCandidateBinding);

					// continue to next operation to check if postconditions are
					// not fulfilled
					if (!postConditionBinding.validate().isOK()) {
						continue;
					}

					// create operation occurrence
					OperationOccurrence opOccurrence = operationOccurrenceFactory
							.createOperationOccurrence();
					opOccurrence.setAppliedOperation(operationSignature
							.getOperationSpecification());
					opOccurrence.setAppliedOperationId(OperationsUtil
							.getId(operationSignature
									.getOperationSpecification()));
					opOccurrence.setAppliedOperationName(operationSignature
							.getOperationSpecification().getName());
					
					// handle hidden changes
					Collection<DiffElement> hiddenChanges = operationChangeMap
							.getChanges(uniquePreConditionBinding,
									postConditionBinding);
					opOccurrence.getHideElements().addAll(hiddenChanges);
					opOccurrence.getHiddenChanges().clear();
					opOccurrence.getHiddenChanges().addAll(hiddenChanges);
					for (DiffElement change : hiddenChanges) {
						change.getIsHiddenBy().add(opOccurrence);
					}

					opOccurrence.setPostConditionBinding(TemplateBindingsUtil
							.convert(postConditionBinding));
					opOccurrence.setPreConditionBinding(TemplateBindingsUtil
							.convert(uniquePreConditionBinding));
					// replace title template with object values from binding
					// register both condition models
					evaluationEngine.registerRelatedTemplateBinding(
							OperationsUtil.INITIAL_PREFIX,
							uniquePreConditionBinding);
					evaluationEngine
							.registerRelatedTemplateBinding(
									OperationsUtil.REVISED_PREFIX,
									postConditionBinding);
					// replace finally
					opOccurrence.setTitle(evaluationEngine
							.replaceTemplateValues(operationSignature
									.getOperationSpecification()
									.getTitleTemplate(),
									uniquePreConditionBinding));

					// remove "consumed changes to avoid double occurrences
					operationChangeMap.removeChanges(opOccurrence);
					
					// add it to performed operations
					performedOperations.add(opOccurrence);
				} catch (BindException e) {
					//e.printStackTrace();
					continue;
				}
			}
		}
		return performedOperations;
	}

	/**
	 * Prepares the {@link #evaluationEngine}.
	 * 
	 * @param conditionModel
	 *            {@link ConditionsModel} to prepare for.
	 * @throws UnsupportedConditionLanguage
	 *             if evaluation engine does not support conditions model.
	 */
	private void prepareEvaluationEngine(ConditionsModel conditionsModel)
			throws UnsupportedConditionLanguage {
		evaluationEngine.setConditionsModel(conditionsModel);
		// evaluationEngine.setContainmentAware(true);
	}

	/**
	 * Instantiates an empty {@link ITemplateBinding}.
	 * 
	 * @return empty {@link ITemplateBinding}.
	 */
	private ITemplateBinding createEmptyTemplateBinding() {
		return new TemplateBindingImpl();
	}

	/**
	 * Creates a pre-selection of potential operation specifications in
	 * repository on the base of the operation signatures.
	 * 
	 * @param inputSignature
	 *            to make pre selection for.
	 * @return potentially applied operation specifications.
	 */
	private List<OperationSpecification> makePreSelection(
			DiffSignature inputSignature) {

		// initialize working list of operation signatures to check
		initializeRepositoryOperationSignatures();

		// iterate over all elements of the input signature and strike out
		// impossible operation specifications
		for (DiffElement currentDiffElement : inputSignature
				.getSignatureElements()) {
			Set<DiffSignature> impossibleSpecifications = new HashSet<DiffSignature>();
			for (DiffSignature operationToCheck : currentOperationSignatures) {
				// get first diff element of operation to check
				if (!operationToCheck.getSignatureElements().isEmpty()) {
					DiffElement firstDiffElement = operationToCheck
							.getSignatureElements().iterator().next();
					// compare diff elements
					int comparation = comparator.compare(firstDiffElement,
							currentDiffElement);
					if (comparation < 0) {
						// if first diff element is smaller, it won't come in
						// input signature any more, so we can remove it from
						// possible operations
						impossibleSpecifications.add(operationToCheck);
					} else if (comparation == 0) {
						// if diff element is equal to current input diff
						// element, remove diff element to check next one in the
						// next iteration
						operationToCheck.getSignatureElements().remove(
								firstDiffElement);
					}
				}
			}
			// remove previously captured impossible operations from working
			// list
			currentOperationSignatures.removeAll(impossibleSpecifications);
		}

		// extract remaining (potentially applied) operation specifications and
		// return them
		List<OperationSpecification> preSelection = new ArrayList<OperationSpecification>();
		for (OperationSignature oSignature : currentOperationSignatures) {
			// only add signature if it is empty (if all diff elements are
			// existing in input diff and consequently each diff element in
			// signature has been removed)
			if (oSignature.getSignatureElements().size() == 0) {
				preSelection.add(oSignature.getOperationSpecification());
			}
		}
		return preSelection;
	}

	/**
	 * Initializes the temporal operation signature list.
	 */
	private void initializeRepositoryOperationSignatures() {
		// initialize empty set
		currentOperationSignatures = new HashSet<OperationSignature>();
		// iterate all specifications for current modeling language
		for (OperationSpecification specification : repository
				.getRegisteredOperationSpecifications(currentModelingLanguage)) {
			OperationSignature operationSignature = new OperationSignature(
					specification);
			currentOperationSignatures.add(operationSignature);
		}
	}

	/**
	 * Returns the modeling language (namespace of compared model) from the
	 * specified <code>comparisonSnapshot</code>.
	 * 
	 * @param comparisonSnapshot
	 *            to get modeling language from.
	 * @return the modeling language.
	 */
	private String getModelingLanguage(
			ComparisonResourceSnapshot comparisonSnapshot) {
		Assert.isTrue(comparisonSnapshot.getDiff().getLeftRoots().size() > 0,
				"Left model may not be empty");
		return comparisonSnapshot.getDiff().getLeftRoots().get(0).eClass()
				.getEPackage().getNsURI();
	}

}
