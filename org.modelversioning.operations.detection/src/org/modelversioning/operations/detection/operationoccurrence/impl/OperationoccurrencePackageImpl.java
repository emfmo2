/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.operations.detection.operationoccurrence.impl;

import org.eclipse.emf.compare.diff.metamodel.DiffPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.modelversioning.core.conditions.templatebindings.TemplatebindingsPackage;

import org.modelversioning.operations.OperationsPackage;

import org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence;
import org.modelversioning.operations.detection.operationoccurrence.OperationoccurrenceFactory;
import org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OperationoccurrencePackageImpl extends EPackageImpl implements OperationoccurrencePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationOccurrenceEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OperationoccurrencePackageImpl() {
		super(eNS_URI, OperationoccurrenceFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OperationoccurrencePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OperationoccurrencePackage init() {
		if (isInited) return (OperationoccurrencePackage)EPackage.Registry.INSTANCE.getEPackage(OperationoccurrencePackage.eNS_URI);

		// Obtain or create and register package
		OperationoccurrencePackageImpl theOperationoccurrencePackage = (OperationoccurrencePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OperationoccurrencePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OperationoccurrencePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		OperationsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theOperationoccurrencePackage.createPackageContents();

		// Initialize created meta-data
		theOperationoccurrencePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOperationoccurrencePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OperationoccurrencePackage.eNS_URI, theOperationoccurrencePackage);
		return theOperationoccurrencePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationOccurrence() {
		return operationOccurrenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationOccurrence_PreConditionBinding() {
		return (EReference)operationOccurrenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationOccurrence_AppliedOperationId() {
		return (EAttribute)operationOccurrenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationOccurrence_Title() {
		return (EAttribute)operationOccurrenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationOccurrence_AppliedOperationName() {
		return (EAttribute)operationOccurrenceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationOccurrence_AppliedOperation() {
		return (EReference)operationOccurrenceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationOccurrence_PostConditionBinding() {
		return (EReference)operationOccurrenceEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationOccurrence_HiddenChanges() {
		return (EReference)operationOccurrenceEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationOccurrence_OrderHint() {
		return (EAttribute)operationOccurrenceEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationoccurrenceFactory getOperationoccurrenceFactory() {
		return (OperationoccurrenceFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		operationOccurrenceEClass = createEClass(OPERATION_OCCURRENCE);
		createEReference(operationOccurrenceEClass, OPERATION_OCCURRENCE__PRE_CONDITION_BINDING);
		createEAttribute(operationOccurrenceEClass, OPERATION_OCCURRENCE__APPLIED_OPERATION_ID);
		createEAttribute(operationOccurrenceEClass, OPERATION_OCCURRENCE__TITLE);
		createEAttribute(operationOccurrenceEClass, OPERATION_OCCURRENCE__APPLIED_OPERATION_NAME);
		createEReference(operationOccurrenceEClass, OPERATION_OCCURRENCE__APPLIED_OPERATION);
		createEReference(operationOccurrenceEClass, OPERATION_OCCURRENCE__POST_CONDITION_BINDING);
		createEReference(operationOccurrenceEClass, OPERATION_OCCURRENCE__HIDDEN_CHANGES);
		createEAttribute(operationOccurrenceEClass, OPERATION_OCCURRENCE__ORDER_HINT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DiffPackage theDiffPackage = (DiffPackage)EPackage.Registry.INSTANCE.getEPackage(DiffPackage.eNS_URI);
		TemplatebindingsPackage theTemplatebindingsPackage = (TemplatebindingsPackage)EPackage.Registry.INSTANCE.getEPackage(TemplatebindingsPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		OperationsPackage theOperationsPackage = (OperationsPackage)EPackage.Registry.INSTANCE.getEPackage(OperationsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		operationOccurrenceEClass.getESuperTypes().add(theDiffPackage.getDiffElement());
		operationOccurrenceEClass.getESuperTypes().add(theDiffPackage.getAbstractDiffExtension());

		// Initialize classes and features; add operations and parameters
		initEClass(operationOccurrenceEClass, OperationOccurrence.class, "OperationOccurrence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationOccurrence_PreConditionBinding(), theTemplatebindingsPackage.getTemplateBindingCollection(), null, "preConditionBinding", null, 1, 1, OperationOccurrence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationOccurrence_AppliedOperationId(), theEcorePackage.getEString(), "appliedOperationId", null, 1, 1, OperationOccurrence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationOccurrence_Title(), theEcorePackage.getEString(), "title", null, 1, 1, OperationOccurrence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationOccurrence_AppliedOperationName(), theEcorePackage.getEString(), "appliedOperationName", null, 1, 1, OperationOccurrence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationOccurrence_AppliedOperation(), theOperationsPackage.getOperationSpecification(), null, "appliedOperation", null, 0, 1, OperationOccurrence.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationOccurrence_PostConditionBinding(), theTemplatebindingsPackage.getTemplateBindingCollection(), null, "postConditionBinding", null, 1, 1, OperationOccurrence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationOccurrence_HiddenChanges(), theDiffPackage.getDiffElement(), null, "hiddenChanges", null, 0, -1, OperationOccurrence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationOccurrence_OrderHint(), ecorePackage.getEInt(), "orderHint", "1", 1, 1, OperationOccurrence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //OperationoccurrencePackageImpl
