/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui;

import org.eclipse.ui.IEditorPart;
import org.modelversioning.operations.execution.IOperationBinding;

/**
 * Adapter for communicating with a specific editor to refresh it and set it to
 * dirty.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IEditorRefreshAdapter {

	/**
	 * Moves the editor to a dirty state indicating that the displayed resource
	 * should be saved.
	 * 
	 * @param editor
	 *            editor to set dirty.
	 */
	public void setDirty(IEditorPart editor);

	/**
	 * Refreshes the editor after the execution of the specified
	 * <code>binding</code> to let this adapter know which operations and which
	 * elements have been updated.
	 * 
	 * @param editor
	 *            editor to refresh.
	 * @param binding
	 *            executed binding.
	 */
	public void refresh(IEditorPart editor, IOperationBinding binding);

}
