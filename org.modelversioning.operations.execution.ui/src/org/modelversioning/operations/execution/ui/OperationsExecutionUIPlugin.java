package org.modelversioning.operations.execution.ui;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class OperationsExecutionUIPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.modelversioning.operations.execution.ui";

	/** Status code describing an internal error */
	public static final int INTERNAL_ERROR = 1;

	// The shared instance
	private static OperationsExecutionUIPlugin plugin;

	/**
	 * The icons path.
	 */
	private static final String iconPath = "icons/"; //$NON-NLS-1$

	/**
	 * The image for an operation specification.
	 */
	public static final String IMG_OPERATION = "operation"; //$NON-NLS-1$

	/**
	 * The image for execution.
	 */
	public static final String IMG_EXECUTE = "execute"; //$NON-NLS-1$

	/**
	 * The image for an register.
	 */
	public static final String IMG_REGISTER = "register"; //$NON-NLS-1$

	/**
	 * The constructor.
	 */
	public OperationsExecutionUIPlugin() {
	}

	/**
	 * {@inheritDoc}
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/**
	 * {@inheritDoc}
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance.
	 * 
	 * @return the shared instance
	 */
	public static OperationsExecutionUIPlugin getDefault() {
		return plugin;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void initializeImageRegistry(ImageRegistry registry) {
		registerImage(registry, IMG_OPERATION, "operationSpecification_16.png"); //$NON-NLS-1$
		registerImage(registry, IMG_EXECUTE, "play_16.png"); //$NON-NLS-1$
		registerImage(registry, IMG_REGISTER, "registerToRepository_16.png"); //$NON-NLS-1$
	}

	/**
	 * Registers the specified fileName under the specified key to the specified
	 * registry.
	 * 
	 * @param registry
	 *            registry to register image.
	 * @param key
	 *            key of image.
	 * @param fileName
	 *            file name.
	 */
	private void registerImage(ImageRegistry registry, String key,
			String fileName) {
		try {
			IPath path = new Path(iconPath + fileName);
			URL url = FileLocator.find(getBundle(), path, null);
			if (url != null) {
				ImageDescriptor desc = ImageDescriptor.createFromURL(url);
				registry.put(key, desc);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Returns the image for the specified image name.
	 * 
	 * @param img
	 *            image name.
	 * @return the image.
	 */
	public static Image getImage(String key) {
		return getDefault().getImageRegistry().get(key);
	}

	/**
	 * Returns an image descriptor for the image file identified by the given
	 * key.
	 * 
	 * @param key
	 *            the key
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String key) {
		return getDefault().getImageRegistry().getDescriptor(key);
	}

	/**
	 * Returns this plug-in's unique identifier.
	 * 
	 * @return the plugin's unique identifier
	 */
	public static String getPluginId() {
		return getDefault().getBundle().getSymbolicName();
	}

	/**
	 * Returns the SWT Shell of the active workbench window or <code>null</code>
	 * if no workbench window is active.
	 * 
	 * @return the SWT Shell of the active workbench window, or
	 *         <code>null</code> if no workbench window is active
	 */
	public static Shell getShell() {
		IWorkbenchWindow window = getActiveWorkbenchWindow();
		if (window == null)
			return null;
		return window.getShell();
	}

	/**
	 * Returns the active workbench window.
	 * 
	 * @return the active workbench window.
	 */
	public static IWorkbenchWindow getActiveWorkbenchWindow() {
		IWorkbench workbench = getActiveWorkbench();
		if (workbench == null)
			return null;
		return workbench.getActiveWorkbenchWindow();
	}

	/**
	 * Returns the active workbench.
	 * 
	 * @return the active workbench.
	 */
	public static IWorkbench getActiveWorkbench() {
		OperationsExecutionUIPlugin plugin = getDefault();
		if (plugin == null)
			return null;
		return plugin.getWorkbench();
	}

	/**
	 * Logs the specified error <code>message</code>.
	 * 
	 * @param message
	 *            message to log.
	 */
	public static void logErrorMessage(String message) {
		if (message == null)
			message = ""; //$NON-NLS-1$
		log(new Status(IStatus.ERROR, getPluginId(), INTERNAL_ERROR, message,
				null));
	}

	/**
	 * Logs the specified <code>exception</code>.
	 * 
	 * @param exception
	 *            to log.
	 */
	public static void log(Throwable exception) {
		log(new Status(IStatus.ERROR, getPluginId(), INTERNAL_ERROR,
				OperationsExecutionUIMessages.getString("internalError"),
				exception));
	}

	/**
	 * Logs the specified <code>status</code>.
	 * 
	 * @param status
	 *            to log.
	 */
	public static void log(IStatus status) {
		getDefault().getLog().log(status);
	}

	/**
	 * Shows error message for the specified {@link Exception} <code>e</code>.
	 * 
	 * @param e
	 *            to show.
	 */
	public static void showError(Exception e) {
		IStatus status = new Status(IStatus.ERROR, PLUGIN_ID,
				e.getLocalizedMessage(), e);
		// log error
		getDefault().getLog().log(status);
		// show error
		ErrorDialog.openError(getShell(), "Unhandled Exception Occurred",
				e.getLocalizedMessage(), status);
	}
}
