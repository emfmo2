/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui.impl;

import org.eclipse.emf.ecore.presentation.EcoreEditor;
import org.eclipse.ui.IEditorPart;
import org.modelversioning.operations.execution.IOperationBinding;
import org.modelversioning.operations.execution.ui.IEditorRefreshAdapter;

/**
 * Implements the {@link IEditorRefreshAdapter} for {@link EcoreEditor}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class EcoreEditorRefreshAdapter implements IEditorRefreshAdapter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void refresh(IEditorPart editor, IOperationBinding binding) {
		// nothing todo
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDirty(IEditorPart editor) {
		// nothing todo
	}
}