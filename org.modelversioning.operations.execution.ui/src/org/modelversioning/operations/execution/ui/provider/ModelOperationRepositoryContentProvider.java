/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui.provider;

import org.eclipse.jface.viewers.Viewer;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.repository.ModelOperationRepository;
import org.modelversioning.operations.ui.commons.provider.OperationSpecificationContentProvider;

/**
 * A content provider for a {@link ModelOperationRepository}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ModelOperationRepositoryContentProvider extends
		OperationSpecificationContentProvider {

	private ModelOperationRepository repository = null;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof ModelOperationRepository) {
			ModelOperationRepository modelOperationRepository = (ModelOperationRepository) inputElement;
			return modelOperationRepository.getRegisteredLanguages().toArray();
		} else if (inputElement instanceof String && repository != null) {
			return repository.getRegisteredOperationSpecifications(
					(String) inputElement).toArray();
		} else {
			return super.getElements(inputElement);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if (newInput instanceof ModelOperationRepository) {
			this.repository = (ModelOperationRepository) newInput;
			viewer.refresh();
		} else {
			super.inputChanged(viewer, oldInput, newInput);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof ModelOperationRepository) {
			ModelOperationRepository modelOperationRepository = (ModelOperationRepository) parentElement;
			return modelOperationRepository.getRegisteredLanguages().toArray();
		} else if (parentElement instanceof String && repository != null) {
			return repository.getRegisteredOperationSpecifications(
					(String) parentElement).toArray();
		} else {
			return super.getChildren(parentElement);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getParent(Object element) {
		if (element instanceof ModelOperationRepository) {
			return null;
		} else if (element instanceof String && repository != null) {
			return repository;
		} else if (element instanceof OperationSpecification
				&& repository != null) {
			return ((OperationSpecification) element).getModelingLanguage();
		} else {
			return super.getParent(element);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof ModelOperationRepository) {
			return getChildren(element).length > 0;
		} else if (element instanceof String && repository != null) {
			return getChildren(element).length > 0;
		} else {
			return super.hasChildren(element);
		}
	}

}
