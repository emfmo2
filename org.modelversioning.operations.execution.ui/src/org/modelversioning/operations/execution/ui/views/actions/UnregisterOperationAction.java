/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui.views.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIMessages;
import org.modelversioning.operations.repository.ModelOperationRepositoryPlugin;

/**
 * Unregisters the selected {@link OperationSpecification}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class UnregisterOperationAction extends Action {

	/**
	 * The viewer.
	 */
	private TreeViewer viewer;

	/**
	 * Constructor setting the viewer.
	 * 
	 * @param viewer
	 *            viewer to set.
	 */
	public UnregisterOperationAction(TreeViewer viewer) {
		this.viewer = viewer;
		this.setText(OperationsExecutionUIMessages
				.getString("MOManagement.unregisterModelOperation")); //$NON-NLS-1$
		this.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_ELCL_REMOVE));
		this.setToolTipText(OperationsExecutionUIMessages
				.getString("MOManagement.unregisterModelOperation_tooltip")); //$NON-NLS-1$
	}

	/**
	 * Removes the currently selected operation specification from model
	 * operation repository.
	 */
	@Override
	public void run() {
		ISelection selection = viewer.getSelection();
		if (!selection.isEmpty()) {
			for (Object o : ((IStructuredSelection) selection).toList()) {
				if (o instanceof OperationSpecification) {
					unregisterOperationSpecification((OperationSpecification) o);
				} else {
					// try to get operation specification of currently
					// selected template
					if (viewer.getContentProvider() instanceof ITreeContentProvider) {
						ITreeContentProvider treeContentProvider = (ITreeContentProvider) viewer
								.getContentProvider();
						Object parent = o;
						while (treeContentProvider.getParent(parent) != null) {
							parent = treeContentProvider.getParent(parent);
						}
						if (parent instanceof OperationSpecification) {
							unregisterOperationSpecification((OperationSpecification) parent);
						} else {
							showNoOperationMessage();
						}
					} else {
						showNoOperationMessage();
					}
				}
			}
		}
		super.run();
	}

	private void showNoOperationMessage() {
		MessageDialog.openInformation(viewer.getControl().getShell(),
				OperationsExecutionUIMessages
						.getString("MOManagement.messageTitle"), //$NON-NLS-1$
				OperationsExecutionUIMessages
						.getString("MOManagement.unregisterNoOperation")); //$NON-NLS-1$
	}

	private void unregisterOperationSpecification(
			OperationSpecification specification) {
		boolean question = MessageDialog
				.openQuestion(
						viewer.getControl().getShell(),
						OperationsExecutionUIMessages
								.getString("MOManagement.messageTitle"), //$NON-NLS-1$
						OperationsExecutionUIMessages
								.getString(
										"MOManagement.unregisterConfirmQuestion", specification.getName())); //$NON-NLS-1$
		if (question) {
			ModelOperationRepositoryPlugin.getDefault()
					.getOperationRepository().unregister(specification);
		}
	}

}
