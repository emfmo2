/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.engines.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.EvaluationResult;
import org.modelversioning.core.conditions.EvaluationStatus;
import org.modelversioning.core.conditions.RefinementTemplate;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.ITemplateBindings;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.conditions.engines.impl.ConditionsEvaluationEngineImpl;
import org.modelversioning.core.conditions.engines.impl.TemplateBindingImpl;
import org.modelversioning.core.conditions.templatebindings.util.TemplateBindingsUtil;
import org.modelversioning.core.conditions.util.ConditionsUtil;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.NegativeApplicationConditionResult;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.OperationsFactory;
import org.modelversioning.operations.execution.IOperationBinding;
import org.modelversioning.operations.execution.engines.IOperationBindingGenerator;

/**
 * Generates {@link OperationBindingImpl}s by analyzing the
 * {@link OperationSpecification} using the
 * {@link ConditionsEvaluationEngineImpl}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationBindingGeneratorImpl implements
		IOperationBindingGenerator {

	private static final String MSG_NAC_FAILS_PREFIX = "\"";
	private static final String MSG_NAC_FAILS_POSTFIX = "\" fails.";
	private static final String INITIAL = "initial";
	private static final String NAC_OK = "No Negative Application Condition violation";
	/**
	 * Used evaluation engine.
	 */
	private IConditionEvaluationEngine evaluationEngine = null;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IOperationBinding generateOperationBinding(
			OperationSpecification operationSpecification, EObject modelRoot)
			throws UnsupportedConditionLanguage {

		// create pre binding where only the root is bound
		ITemplateBinding templateBinding = new TemplateBindingImpl();
		templateBinding.add(operationSpecification.getPreconditions()
				.getRootTemplate(), modelRoot);

		// generate for created preBinding and return
		return generateOperationBinding(operationSpecification, templateBinding);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IOperationBinding generateOperationBinding(
			OperationSpecification operationSpecification,
			ITemplateBinding preBinding) throws UnsupportedConditionLanguage {

		// fetch evaluation engine and find template bindings for prebinding
		IConditionEvaluationEngine evalEngine = getInitializedEvaluationEngine(operationSpecification);
		ITemplateBindings templateBindings = evalEngine.findTemplateBinding(
				preBinding, true);

		// create and return operation binding
		return generateOperationBinding(operationSpecification,
				templateBindings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IOperationBinding generateOperationBinding(
			OperationSpecification operationSpecification,
			ITemplateBindings templateBindings)
			throws UnsupportedConditionLanguage {
		// create operation binding for operationSpecification with
		// templateBindings
		OperationBindingImpl operationBinding = new OperationBindingImpl();
		operationBinding.setOperationSpecification(operationSpecification);
		operationBinding.setTemplateBinding(templateBindings);

		// check NACs
		// saves all bindings invalidated by at least one NAC
		Set<ITemplateBinding> invalidBindings = new HashSet<ITemplateBinding>();
		Map<NegativeApplicationCondition, NegativeApplicationConditionResult> negativeNACResults = new HashMap<NegativeApplicationCondition, NegativeApplicationConditionResult>();
		for (NegativeApplicationCondition nac : operationSpecification
				.getNegativeApplicationConditions()) {
			IConditionEvaluationEngine engine = getInitializedEvaluationEngine(
					nac, templateBindings);
			boolean hadValidNAC = false;
			for (ITemplateBinding binding : templateBindings
					.getAllPossibleBindings()) {
				ITemplateBinding reWrittenBinding = rewriteBinding(nac, binding);
				ITemplateBindings nacBinding = engine.findTemplateBinding(
						reWrittenBinding, true);
				if (nacBinding.validate().isOK()) {
					hadValidNAC = true;
					String message = "";
					if (nac.getErrorMessage() != null
							&& nac.getErrorMessage().length() > 0) {
						message = evaluationEngine.replaceTemplateValues(
								nac.getErrorMessage(), nacBinding);
					} else {
						message = MSG_NAC_FAILS_PREFIX + nac.getName()
								+ MSG_NAC_FAILS_POSTFIX;
					}
					invalidBindings.add(binding);
					negativeNACResults.put(nac,
							createNACResult(nacBinding, message, nac));
				}
			}
			if (!hadValidNAC) {
				operationBinding.addNegativeApplicationConditionResult(nac,
						createPositiveNACResult(nac));
			}
		}

		// check if we have at least one binding left that is not invalidated by
		// a NAC
		if (templateBindings.getAllPossibleBindings().size() > invalidBindings
				.size()) {
			// clear all negative NAC results and go with valid bindings
			templateBindings.getAllPossibleBindings()
					.removeAll(invalidBindings);
			// TODO add warning that bindings have been rejected due to NAC
		} else {
			// add all negative results
			for (Entry<NegativeApplicationCondition, NegativeApplicationConditionResult> entry : negativeNACResults
					.entrySet()) {
				operationBinding.addNegativeApplicationConditionResult(
						entry.getKey(), entry.getValue());
			}
		}

		return operationBinding;
	}

	/**
	 * Creates a positive {@link NegativeApplicationConditionResult}.
	 * 
	 * @return created {@link NegativeApplicationConditionResult}
	 */
	private NegativeApplicationConditionResult createPositiveNACResult(
			NegativeApplicationCondition nac) {
		NegativeApplicationConditionResult result = OperationsFactory.eINSTANCE
				.createNegativeApplicationConditionResult();
		result.setMessage(NAC_OK);
		result.setNegativeApplicationCondition(nac);
		result.setStatus(EvaluationStatus.SATISFIED);
		return result;
	}

	/**
	 * Creates a violating {@link NegativeApplicationConditionResult}.
	 * 
	 * @param nacBinding
	 *            valid binding.
	 * @param message
	 *            message to set to the result.
	 * @param nac
	 *            {@link NegativeApplicationCondition} to create result for.
	 * @return the created {@link NegativeApplicationCondition}.
	 */
	private NegativeApplicationConditionResult createNACResult(
			ITemplateBindings nacBinding, String message,
			NegativeApplicationCondition nac) {
		NegativeApplicationConditionResult result = OperationsFactory.eINSTANCE
				.createNegativeApplicationConditionResult();
		EvaluationResult validation = nacBinding.validate();
		if (EvaluationStatus.ERROR.equals(validation.getStatus())) {
			result.setException(validation.getException());
			result.setMessage(validation.getMessage());
			result.setStatus(EvaluationStatus.ERROR);
			result.setEvaluator(validation.getEvaluator());
		} else {
			// there should be only one
			result.setBinding(TemplateBindingsUtil.convert(nacBinding));
			result.setMessage(message);
			result.setNegativeApplicationCondition(nac);
			result.setStatus(EvaluationStatus.UNSATISFIED);
			result.setEvaluator(validation.getEvaluator());
		}
		return result;
	}

	/**
	 * Rewrites the <code>binding</code> to the {@link ConditionsModel} in the
	 * <code>nac</code>.
	 * 
	 * @param nac
	 *            to rewrite binding for.
	 * @param binding
	 *            to rewrite.
	 * @return rewritten binding.
	 */
	private ITemplateBinding rewriteBinding(NegativeApplicationCondition nac,
			ITemplateBinding binding) {
		ITemplateBinding rewrittenBinding = new TemplateBindingImpl();
		EList<Template> allTemplates = ConditionsUtil.getAllTemplates(nac
				.getConditions());
		for (Template template : allTemplates) {
			if (template instanceof RefinementTemplate) {
				RefinementTemplate refinementTemplate = (RefinementTemplate) template;
				rewrittenBinding.add(refinementTemplate, binding
						.getBoundObjects(refinementTemplate
								.getRefinedTemplate()));
			}
		}
		return rewrittenBinding;
	}

	/**
	 * Returns an initialized evaluation engine. If there has been set an
	 * initialized evaluation engine it is returned. If not, this method creates
	 * a new one and initializes it.
	 * 
	 * @param operationSpecification
	 *            operation specification for which the evaluation engine is
	 *            needed.
	 * @return initialized evaluation engine.
	 * @throws UnsupportedConditionLanguage
	 *             if the evaluation engine cannot be used for the conditions
	 *             language of the specified operation specification.
	 */
	private IConditionEvaluationEngine getInitializedEvaluationEngine(
			OperationSpecification operationSpecification)
			throws UnsupportedConditionLanguage {
		// initialize evaluation engine if necessary
		if (!isEvaluationEngineInitialized(operationSpecification
				.getPreconditions())) {
			evaluationEngine = new ConditionsEvaluationEngineImpl();
			evaluationEngine.setConditionsModel(operationSpecification
					.getPreconditions());
		}
		return evaluationEngine;
	}

	/**
	 * Returns an initialized evaluation engine. If there has been set an
	 * initialized evaluation engine it is returned. If not, this method creates
	 * a new one and initializes it.
	 * 
	 * @param operationSpecification
	 *            operation specification for which the evaluation engine is
	 *            needed.
	 * @return initialized evaluation engine.
	 * @throws UnsupportedConditionLanguage
	 *             if the evaluation engine cannot be used for the conditions
	 *             language of the specified operation specification.
	 */
	private IConditionEvaluationEngine getInitializedEvaluationEngine(
			NegativeApplicationCondition nac,
			ITemplateBindings preConditionTemplateBinding)
			throws UnsupportedConditionLanguage {
		// initialize evaluation engine if necessary
		if (!isEvaluationEngineInitialized(nac.getConditions())) {
			evaluationEngine = new ConditionsEvaluationEngineImpl();
			evaluationEngine.setConditionsModel(nac.getConditions());
			evaluationEngine.registerRelatedTemplateBinding(INITIAL,
					preConditionTemplateBinding);
		}
		return evaluationEngine;
	}

	/**
	 * Returns <code>true</code> if evaluation engine to use is initialized
	 * already.
	 * 
	 * @param conditionsModel
	 *            the conditionsmodel to initialize for.
	 * 
	 * @return <code>true</code> if evaluation engine to use is initialized
	 *         already.
	 */
	private boolean isEvaluationEngineInitialized(
			ConditionsModel conditionsModel) {
		if (evaluationEngine == null
				|| !conditionsModel.equals(evaluationEngine
						.getConditionsModel())) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IConditionEvaluationEngine getEvaluationEngine() {
		return evaluationEngine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEvaluationEngine(IConditionEvaluationEngine evaluationEngine) {
		this.evaluationEngine = evaluationEngine;
	}

}
