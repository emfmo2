package org.modelversioning.operations.ui.dialogs;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.modelversioning.operations.OperationSpecification;

public class IterationsContentProvider implements ITreeContentProvider {

	@Override
	public void dispose() {
		// nothing todo
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {

	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof OperationSpecification) {
			return ((OperationSpecification) parentElement).getIterations()
					.toArray();
		}
		return null;
	}

	@Override
	public Object getParent(Object element) {
		// no parents
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof OperationSpecification) {
			return (((OperationSpecification) element).getIterations().size() != 0);
		}
		return false;

	}

	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

}
