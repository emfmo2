/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.pages;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.MasterDetailsBlock;
import org.eclipse.ui.forms.SectionPart;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.impl.NegativeApplicationConditionImpl;
import org.modelversioning.operations.ui.editors.OperationRecorderEditor;
import org.modelversioning.operations.ui.editors.OperationRecorderEditor.NACSpecificationState;
import org.modelversioning.operations.ui.provider.NegativeApplicationConditionContentProvider;
import org.modelversioning.operations.ui.provider.NegativeApplicationConditionLabelProvider;

/**
 * {@link MasterDetailsBlock} implementation for
 * {@link NegativeApplicationCondition}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class NegativeApplicationConditionsBlock extends MasterDetailsBlock {

	/**
	 * The containing editor.
	 */
	private OperationRecorderEditor editor;
	/**
	 * The add button.
	 */
	private Button btnAdd;
	/**
	 * The remove button.
	 */
	private Button btnRemove;
	/**
	 * The commit NAC Demo button.
	 */
	private Button btnCommitDemo;
	/**
	 * The details page.
	 */
	private NegativeApplicationConditionsDetailsPage detailsPage;
	/**
	 * The NAC view.
	 */
	private TreeViewer nacViewer;

	/**
	 * The constructor providing the containing editor.
	 * 
	 * @param editor
	 *            editor containing this block.
	 */
	public NegativeApplicationConditionsBlock(OperationRecorderEditor editor) {
		this.editor = editor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void createMasterPart(final IManagedForm managedForm,
			Composite parent) {
		FormToolkit toolkit = managedForm.getToolkit();
		Section section = toolkit.createSection(parent, Section.DESCRIPTION
				| Section.TITLE_BAR);
		section.setText("Negative Application Conditions");
		section.setDescription("The list of negative application conditions.");
		section.marginWidth = 10;
		section.marginHeight = 5;
		Composite client = toolkit.createComposite(section, SWT.WRAP);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		layout.marginWidth = 2;
		layout.marginHeight = 2;
		client.setLayout(layout);
		final Tree tree = toolkit.createTree(client, SWT.NULL);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 20;
		gd.widthHint = 100;
		tree.setLayoutData(gd);
		toolkit.paintBordersFor(client);

		// button area
		Composite buttons = toolkit.createComposite(client, SWT.WRAP);
		buttons.setLayout(new GridLayout());
		buttons.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		// add button
		btnAdd = toolkit.createButton(buttons, "Add NAC", SWT.PUSH);
		btnAdd.setEnabled(true);
		gd = new GridData(GridData.VERTICAL_ALIGN_BEGINNING
				| GridData.FILL_HORIZONTAL);
		btnAdd.setLayoutData(gd);
		btnAdd.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				editor.addNewNAC();
			}
		});

		// remove button
		btnRemove = toolkit.createButton(buttons, "Remove NAC", SWT.PUSH);
		btnRemove.setEnabled(false);
		btnRemove.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (tree.getSelectionCount() > 0) {
					TreeItem treeItem = tree.getSelection()[0];
					if (treeItem.getData() instanceof NegativeApplicationCondition) {
						editor.removeNAC((NegativeApplicationCondition) treeItem
								.getData());
						refresh();
					}
				}
			}
		});
		gd = new GridData(GridData.VERTICAL_ALIGN_END
				| GridData.FILL_HORIZONTAL);
		btnRemove.setLayoutData(gd);
		// commit demonstration button
		btnCommitDemo = toolkit.createButton(buttons, "Commit NAC", SWT.PUSH);
		btnCommitDemo.setEnabled(false);
		btnCommitDemo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				editor.commitNAC();
			}
		});
		gd = new GridData(GridData.VERTICAL_ALIGN_END
				| GridData.FILL_HORIZONTAL);
		btnCommitDemo.setLayoutData(gd);
		btnCommitDemo.setVisible(false);

		section.setClient(client);
		final SectionPart spart = new SectionPart(section);
		managedForm.addPart(spart);
		nacViewer = new TreeViewer(tree);
		nacViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				managedForm.fireSelectionChanged(spart, event.getSelection());
				if (!event.getSelection().isEmpty()) {
					btnRemove.setEnabled(true);
				} else {
					btnRemove.setEnabled(false);
				}
			}
		});
		nacViewer
				.setContentProvider(new NegativeApplicationConditionContentProvider());
		nacViewer
				.setLabelProvider(new NegativeApplicationConditionLabelProvider());
		nacViewer.setInput(editor.getOperationSpecification());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Currently no toolbar actions for rules.
	 */
	@Override
	protected void createToolBarActions(IManagedForm managedForm) {
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Registers the {@link NegativeApplicationConditionDetailsPage} for
	 * {@link NegativeApplicationCondition}s.
	 */
	@Override
	protected void registerPages(DetailsPart detailsPart) {
		this.detailsPage = new NegativeApplicationConditionsDetailsPage(editor,
				this);
		detailsPart.registerPage(NegativeApplicationConditionImpl.class,
				this.detailsPage);
	}

	/**
	 * Sets the state of this block.
	 * 
	 * @param state
	 *            to set.
	 */
	protected void setState(NACSpecificationState state) {
		if (NACSpecificationState.DEMONSTRATION.equals(state)) {
			this.btnAdd.setEnabled(false);
			this.btnRemove.setEnabled(false);
			this.nacViewer.getTree().setEnabled(false);
			this.btnCommitDemo.setVisible(true);
			this.btnCommitDemo.setEnabled(true);
		} else {
			this.nacViewer.getTree().setEnabled(true);
			this.btnAdd.setEnabled(true);
			this.btnRemove.setEnabled(true);
			this.btnCommitDemo.setVisible(false);
			this.btnCommitDemo.setEnabled(false);
		}
		this.detailsPage.setState(state);
	}

	/**
	 * Refreshes this block.
	 */
	protected void refresh() {
		this.nacViewer.refresh(true);
	}

	/**
	 * Selects the specified {@link NegativeApplicationCondition}.
	 * 
	 * @param nac
	 *            to select.
	 */
	protected void selectNAC(NegativeApplicationCondition nac) {
		this.nacViewer.setExpandedState(nac, true);
	}

}
