/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.pages;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.IDetailsPage;
import org.eclipse.ui.forms.IFormPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.ui.editors.OperationRecorderEditor;
import org.modelversioning.operations.ui.editors.OperationRecorderEditor.NACSpecificationState;

/**
 * Page displaying the details for {@link NegativeApplicationCondition}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class NegativeApplicationConditionsDetailsPage implements IDetailsPage,
		ModifyListener, SelectionListener {

	/**
	 * The containing editor.
	 */
	private OperationRecorderEditor editor;

	/**
	 * The NAC block.
	 */
	private NegativeApplicationConditionsBlock nacBlock;

	/**
	 * The form.
	 */
	private IManagedForm managedForm;
	/**
	 * The NAC to display.
	 */
	private NegativeApplicationCondition nac;
	/**
	 * Specifies whether is form is dirty.
	 */
	private boolean dirty;

	/**
	 * The name field.
	 */
	private Text txtName;
	/**
	 * The description field.
	 */
	private Text txtDescription;
	/**
	 * The message field.
	 */
	private Text txtErrorMessage;
	/**
	 * Button to show negative applicaiton conditions.
	 */
	private Button btnShowConditions;

	/**
	 * The constructor providing the containing editor.
	 * 
	 * @param editor
	 *            the containing editor.
	 * @param nacBlock
	 *            parent block
	 */
	public NegativeApplicationConditionsDetailsPage(
			OperationRecorderEditor editor,
			NegativeApplicationConditionsBlock nacBlock) {
		this.editor = editor;
		this.nacBlock = nacBlock;
	}

	/**
	 * {@inheritDoc}
	 */
	public void initialize(IManagedForm managedForm) {
		this.managedForm = managedForm;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createContents(Composite parent) {
		TableWrapLayout layout = new TableWrapLayout();
		layout.topMargin = 5;
		layout.leftMargin = 5;
		layout.rightMargin = 2;
		layout.bottomMargin = 2;
		parent.setLayout(layout);
		FormToolkit toolkit = managedForm.getToolkit();

		createNACDetailsSection(parent, toolkit);

		// template section
		createTemplateSection(parent, toolkit);
	}

	/**
	 * Creates the NAC details section and adds it to the specified
	 * <code>parent</code>.
	 * 
	 * @param parent
	 *            to add section to.
	 * @param toolkit
	 *            to use for creation.
	 */
	private void createNACDetailsSection(Composite parent, FormToolkit toolkit) {
		// NAC details section
		Section detailsSection = toolkit.createSection(parent,
				Section.DESCRIPTION | Section.TITLE_BAR);
		detailsSection.marginWidth = 10;
		detailsSection.setText("NAC Details");
		detailsSection
				.setDescription("Details of the selected Negative Application Condition.");
		TableWrapData td = new TableWrapData(TableWrapData.FILL,
				TableWrapData.TOP);
		td.grabHorizontal = true;
		detailsSection.setLayoutData(td);
		Composite client = toolkit.createComposite(detailsSection);
		GridLayout glayout = new GridLayout();
		glayout.marginWidth = glayout.marginHeight = 0;
		glayout.numColumns = 2;
		client.setLayout(glayout);

		// nac name
		toolkit.createLabel(client, "Name:");
		txtName = toolkit.createText(client,
				nac != null ? nac.getName() : "", SWT.BORDER | SWT.SINGLE); //$NON-NLS-1$
		txtName.addModifyListener(this);
		txtName.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// nac description
		toolkit.createLabel(client, "Description:");
		txtDescription = toolkit
				.createText(
						client,
						nac != null ? nac.getDescription() : "", SWT.BORDER | SWT.MULTI); //$NON-NLS-1$
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.grabExcessVerticalSpace = true;
		gd.heightHint = 100;
		txtDescription.setLayoutData(gd);
		txtDescription.addModifyListener(this);

		// nac message
		toolkit.createLabel(client, "Error Message:");
		txtErrorMessage = toolkit.createText(client,
				nac != null ? nac.getName() : "", SWT.BORDER | SWT.SINGLE); //$NON-NLS-1$
		txtErrorMessage.addModifyListener(this);
		txtErrorMessage.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		detailsSection.setClient(client);
	}

	/**
	 * Creates the template section and adds it to the specified
	 * <code>parent</code>.
	 * 
	 * @param parent
	 *            to add section to.
	 * @param toolkit
	 *            to use for creation.
	 */
	private void createTemplateSection(Composite parent, FormToolkit toolkit) {
		TableWrapData td;
		GridLayout glayout;
		GridData gd;
		Section templateSection = toolkit.createSection(parent,
				Section.DESCRIPTION | Section.TITLE_BAR);
		templateSection.marginWidth = 10;
		templateSection.marginHeight = 2;
		templateSection.setText("Templates");
		templateSection
				.setDescription("Configure source model and target model templates.");
		td = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP);
		td.grabHorizontal = true;
		templateSection.setLayoutData(td);
		Composite templateClient = toolkit.createComposite(templateSection);
		glayout = new GridLayout();
		glayout.marginWidth = glayout.marginHeight = 2;
		glayout.numColumns = 2;
		glayout.makeColumnsEqualWidth = true;
		templateClient.setLayout(glayout);

		// button for source conditions
		btnShowConditions = toolkit.createButton(templateClient,
				"Show Negative Application Conditions", SWT.PUSH);
		btnShowConditions.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				editor.showNegativeApplicationConditionView(nac);
			}
		});
		gd = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		btnShowConditions.setLayoutData(gd);

		templateSection.setClient(templateClient);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Writes the values to the underlying model.
	 */
	@Override
	public void modifyText(ModifyEvent e) {
		commit(false);
		setDirty(true);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Writes the values to the underlying model.
	 */
	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		commit(false);
		setDirty(true);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Writes the values to the underlying model.
	 */
	@Override
	public void widgetSelected(SelectionEvent e) {
		commit(false);
		setDirty(true);
	}

	/**
	 * Updates all controls with the currently set {@link #nac}.
	 */
	private void update() {
		// stop listening for the moment
		txtName.removeModifyListener(this);
		txtDescription.removeModifyListener(this);
		txtErrorMessage.removeModifyListener(this);
		// set values
		txtName.setText(nac != null ? nac.getName() != null ? nac.getName()
				: "" : ""); //$NON-NLS-1$ //$NON-NLS-2$
		txtDescription.setText(nac != null ? nac.getDescription() != null ? nac
				.getDescription() : "" : ""); //$NON-NLS-1$ //$NON-NLS-2$
		txtErrorMessage
				.setText(nac != null ? nac.getErrorMessage() != null ? nac
						.getErrorMessage() : "" : ""); //$NON-NLS-1$ //$NON-NLS-2$

		// add listener again
		txtName.addModifyListener(this);
		txtDescription.addModifyListener(this);
		txtErrorMessage.addSelectionListener(this);
	}

	/**
	 * Sets whether this form is dirty.
	 * 
	 * @param dirty
	 *            <code>true</code> if dirty, <code>false</code> otherwise.
	 */
	private void setDirty(boolean dirty) {
		this.dirty = dirty;
		if (dirty) {
			this.editor.setDirty(dirty);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void commit(boolean onSave) {
		if (nac != null) {
			nac.setName(txtName.getText());
			nac.setDescription(txtDescription.getText());
			nac.setErrorMessage(txtErrorMessage.getText());
			this.nacBlock.refresh();
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Does nothing.
	 */
	@Override
	public void dispose() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDirty() {
		return dirty;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStale() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void refresh() {
		update();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFocus() {
		this.txtName.setFocus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean setFormInput(Object input) {
		if (input instanceof NegativeApplicationCondition) {
			this.nac = (NegativeApplicationCondition) input;
			update();
			return true;
		} else {
			this.nac = null;
			update();
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void selectionChanged(IFormPart part, ISelection selection) {
		IStructuredSelection ssel = (IStructuredSelection) selection;
		if (ssel.size() == 1) {
			nac = (NegativeApplicationCondition) ssel.getFirstElement();
		} else {
			nac = null;
		}
		update();
	}

	/**
	 * Sets the state of this details page.
	 * 
	 * @param state
	 *            state to set.
	 */
	protected void setState(NACSpecificationState state) {
		if (txtDescription != null) {
			if (NACSpecificationState.DEMONSTRATION.equals(state)) {
				this.txtDescription.setEnabled(false);
				this.txtName.setEnabled(false);
				this.btnShowConditions.setEnabled(false);
			} else {
				this.txtDescription.setEnabled(true);
				this.txtName.setEnabled(true);
				this.btnShowConditions.setEnabled(true);
			}
		}
	}

}
