package org.modelversioning.operations.ui.provider;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.Policy;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableFontProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.CustomCondition;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.RefinementTemplate;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.impl.OCLLiterals;

/**
 * label provider for conditions view
 * 
 * @author Martina Seidl
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ConditionsViewLabelProvider implements ILabelProvider, ITableLabelProvider,
		ITableFontProvider, ITableColorProvider {

	private static final String INACTIVE_CONDITION_FG_COLOR = Policy.JFACE
			+ ".INACTIVE_CONDITION_FG_COLOR"; //$NON-NLS-1$
	private static final String INACTIVE_TEMPLATE_FG_COLOR = Policy.JFACE
			+ ".INACTIVE_CONDITION_FG_COLOR"; //$NON-NLS-1$
	private static final String NEC_TEMPLATE_FG_COLOR = Policy.JFACE
			+ ".NEC_CONDITION_FG_COLOR"; //$NON-NLS-1$
	private static final String OPTIONAL_TEMPLATE_FG_COLOR = Policy.JFACE
			+ ".OPTIONAL_CONDITION_FG_COLOR"; //$NON-NLS-1$

	public ConditionsViewLabelProvider() {
		JFaceResources.getColorRegistry().put(INACTIVE_CONDITION_FG_COLOR,
				new RGB(160, 160, 160));
		JFaceResources.getColorRegistry().put(INACTIVE_TEMPLATE_FG_COLOR,
				new RGB(160, 160, 160));
		JFaceResources.getColorRegistry().put(NEC_TEMPLATE_FG_COLOR,
				new RGB(204, 0, 51));
		JFaceResources.getColorRegistry().put(OPTIONAL_TEMPLATE_FG_COLOR,
				new RGB(0, 51, 204));
	}

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
//		if (element instanceof Template && columnIndex == 0) {
//			return OperationsUIPlugin
//					.getImage(OperationsUIPlugin.IMG_INTIALMODEL);
//		}
		// no image at the moment
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {

		switch (columnIndex) {

		case 0:
			if (element instanceof Template) {
				return ((Template) element).getName();
			}

			if (element instanceof CustomCondition) {
				return OCLLiterals.SELF + OCLLiterals.DOT;
			}

			if (element instanceof FeatureCondition) {
				return ((FeatureCondition) element).getFeature().getName();
			}

			break;

		case 1:

			if (element instanceof Template) {
				return ((Template) element).getTitle();
			}

			if (element instanceof CustomCondition) {
				return ((CustomCondition) element).getExpression();
			}

			if (element instanceof FeatureCondition) {
				return ((FeatureCondition) element).getExpression();
			}

			if (element instanceof String) {
				return (String) element;
			}
		}
		return null;
	}

	@Override
	public void addListener(ILabelProviderListener listener) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {

	}

	@Override
	public Font getFont(Object element, int columnIndex) {
		if (element instanceof RefinementTemplate && columnIndex == 0) {
			return JFaceResources.getFontRegistry().getItalic(
					JFaceResources.DIALOG_FONT);
		} else if (element instanceof Template && columnIndex == 0) {
			return JFaceResources.getFontRegistry().getBold(
					JFaceResources.DIALOG_FONT);
		}
		return null;
	}

	@Override
	public Color getForeground(Object element, int columnIndex) {
		if (element instanceof Condition) {
			Condition condition = (Condition) element;
			if (!condition.isActive() || !condition.getTemplate().isActive()) {
				return JFaceResources.getColorRegistry().get(
						INACTIVE_CONDITION_FG_COLOR);
			}
			if (!condition.getTemplate().isExistence()) {
				return JFaceResources.getColorRegistry().get(
						NEC_TEMPLATE_FG_COLOR);
			}
			if (!condition.getTemplate().isMandatory()) {
				return JFaceResources.getColorRegistry().get(
						OPTIONAL_TEMPLATE_FG_COLOR);
			}
		} else if (element instanceof Template) {
			Template template = (Template) element;
			if (!template.isActive()) {
				return JFaceResources.getColorRegistry().get(
						INACTIVE_TEMPLATE_FG_COLOR);
			}
			if (!template.isExistence()) {
				return JFaceResources.getColorRegistry().get(
						NEC_TEMPLATE_FG_COLOR);
			}
			if (!template.isMandatory()) {
				return JFaceResources.getColorRegistry().get(
						OPTIONAL_TEMPLATE_FG_COLOR);
			}
		}
		return null;
	}

	@Override
	public Color getBackground(Object element, int columnIndex) {
		return null;
	}

	@Override
	public Image getImage(Object element) {
		return getColumnImage(element, 0);
	}

	@Override
	public String getText(Object element) {
		return getColumnText(element, 1);
	}
}
