/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.views;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.part.ViewPart;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.CustomCondition;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.State;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.util.ConditionsUtil;
import org.modelversioning.operations.ui.dialogs.ConditionEditingDialog;
import org.modelversioning.operations.ui.provider.ConditionsViewContentProvider;
import org.modelversioning.operations.ui.provider.ConditionsViewLabelProvider;
import org.modelversioning.operations.ui.views.actions.AddConditionAction;
import org.modelversioning.operations.ui.views.actions.RemoveConditionAction;
import org.modelversioning.operations.ui.views.actions.ValidateConditionAction;

/**
 * View for {@link ConditionsModel}s.
 * 
 * TODO new CustomCondition is not checked although it is active
 * 
 * @author Martina Seidl
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ConditionView extends ViewPart {

	/**
	 * The tree.
	 */
	private Tree tree;
	/**
	 * The tree viewer.
	 */
	private CheckboxTreeViewer treeViewer;
	/**
	 * The tree editor.
	 */
	private TreeEditor editor;
	/**
	 * The content provider to use.
	 */
	private ITreeContentProvider contentProvider;
	/**
	 * The label provider to use.
	 */
	private ITableLabelProvider labelProvider;
	/**
	 * The action to add a new condition.
	 */
	private AddConditionAction addCondition;
	/**
	 * The action to remove a condition.
	 */
	private RemoveConditionAction removeCondition;
	/**
	 * The action to validate a condition.
	 */
	private ValidateConditionAction validateCondition;
	/**
	 * The registered change listeners.
	 */
	private List<ChangeListener> changeListener = new ArrayList<ChangeListener>();
	/**
	 * The composite for the buttons in the cell.
	 */
	private Composite cellComposite;
	/**
	 * The condition evaluation engine to use.
	 */
	private IConditionEvaluationEngine conditionEvaluationEngine;

	/**
	 * Adds the listener to the collection of listeners who will be notified
	 * when the receiver's state changed.
	 * 
	 * @param listener
	 *            to add.
	 */
	public void addChangeListener(ChangeListener listener) {
		this.changeListener.add(listener);
	}

	/**
	 * Removes the listener from the collection of listeners who will be
	 * notified when the receiver's state changed.
	 * 
	 * @param listener
	 *            to remove.
	 */
	public void removeChangeListener(ChangeListener listener) {
		this.changeListener.remove(listener);
	}

	/**
	 * Notifies the registered change listeners that the specified
	 * <code>event</code> occurred.
	 * 
	 * @param event
	 *            to notify.
	 */
	private void notifyChangeListener(EventObject event) {
		for (ChangeListener listener : this.changeListener) {
			listener.stateChanged(new ChangeEvent(event.getSource()));
		}
	}

	/**
	 * Returns the currently used {@link ITreeContentProvider}.
	 * 
	 * @return the content provider
	 */
	public ITreeContentProvider getContentProvider() {
		if (contentProvider == null) {
			contentProvider = new ConditionsViewContentProvider();
		}
		return contentProvider;
	}

	/**
	 * Sets the {@link ITreeContentProvider} to use.
	 * 
	 * @param contentProvider
	 *            the content provider to set
	 */
	public void setContentProvider(ITreeContentProvider contentProvider) {
		this.contentProvider = contentProvider;
		if (this.treeViewer != null) {
			this.treeViewer.setContentProvider(this.contentProvider);
			refresh();
		}
	}

	/**
	 * Returns the currently used label provider.
	 * 
	 * @return the label provider.
	 */
	public ITableLabelProvider getLabelProvider() {
		if (labelProvider == null) {
			labelProvider = new ConditionsViewLabelProvider();
		}
		return labelProvider;
	}

	/**
	 * Sets the {@link ITableLabelProvider} to use.
	 * 
	 * @param labelProvider
	 *            the label provider to set.
	 */
	public void setLabelProvider(ITableLabelProvider labelProvider) {
		this.labelProvider = labelProvider;
		if (this.treeViewer != null) {
			this.treeViewer.setLabelProvider(this.labelProvider);
			refresh();
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Sets up this control by adding the tree viewer.
	 */
	@Override
	public void createPartControl(Composite parent) {
		TableWrapData td = new TableWrapData(TableWrapData.FILL_GRAB);
		parent.setLayoutData(td);
		GridLayout gl = new GridLayout();
		parent.setLayout(gl);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL
				| GridData.FILL_VERTICAL);

		// create tree viewer
		treeViewer = new CheckboxTreeViewer(parent, SWT.CHECK | SWT.BORDER);
		treeViewer.setContentProvider(getContentProvider());
		treeViewer.setLabelProvider(getLabelProvider());
		tree = treeViewer.getTree();
		tree.setHeaderVisible(false);
		tree.setLayoutData(gd);

		// create tree editor
		editor = new TreeEditor(tree);
		editor.horizontalAlignment = SWT.LEFT;
		editor.grabHorizontal = true;
		TreeColumn column1 = new TreeColumn(tree, SWT.LEFT);
		column1.setText("Name");
		column1.setWidth(250);
		TreeColumn column2 = new TreeColumn(tree, SWT.LEFT);
		column2.setText("Value");
		column2.setWidth(250);

		// add check state listener
		addCheckStateListener();

		// add selection listener and editing facilities
		addEditingFeatures();

		// create and hook actions
		makeActions();
		hookContextMenu();
		contributeToActionBars();
	}

	/**
	 * Expands all templates.
	 */
	public void expandAll() {
		if (this.treeViewer != null) {
			treeViewer.expandAll();
		}
	}

	/**
	 * Adds editing features using an {@link ISelectionChangedListener} to
	 * {@link #treeViewer}.
	 */
	private void addEditingFeatures() {
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {

				if (tree.getSelectionCount() == 1) {
					// Determine the item to edit
					final TreeItem item = tree.getSelection()[0];

					if (cellComposite != null) {
						cellComposite.dispose();
					}

					if (item.getData() instanceof Template) {

						final Text text = new Text(tree, SWT.NONE);
						text.setText(item.getText(1));
						text.selectAll();
						text.setFocus();

						text.addFocusListener(new FocusAdapter() {
							public void focusLost(FocusEvent event) {
								updateText(item, text.getText());
								text.dispose();
							}
						});

						// If they hit Enter, set the text into the tree and
						// end the editing session. If they hit Escape, ignore
						// the text and end the editing session
						text.addKeyListener(new KeyAdapter() {
							public void keyPressed(KeyEvent event) {
								switch (event.keyCode) {
								case SWT.CR:
									// Enter hit--set the text into the tree and
									// drop through
									updateText(item, text.getText());
								case SWT.ESC:
									// End editing session
									text.dispose();
									break;
								}
							}
						});
						editor.setEditor(text, item, 1);

					} else {
						cellComposite = new Composite(tree, SWT.NONE);
						GridLayout gl = new GridLayout(2, false);
						gl.marginHeight = 0;
						gl.marginWidth = 0;
						gl.marginBottom = 0;
						gl.marginLeft = 1;
						gl.marginRight = 0;
						gl.marginTop = 0;
						gl.verticalSpacing = 0;
						gl.horizontalSpacing = 0;
						cellComposite.setLayout(gl);
						GridData gd = new GridData(GridData.FILL_BOTH);
						cellComposite.setLayoutData(gd);

						Text text = new Text(cellComposite, SWT.NONE);
						gd = new GridData(GridData.FILL_BOTH);
						text.setLayoutData(gd);
						text.setText(item.getText(1));
						text.setEditable(false);

						final Button button = new Button(cellComposite,
								SWT.PUSH);
						gd = new GridData(GridData.FILL_VERTICAL);
						gd.heightHint = 21;
						button.setLayoutData(gd);
						button.setText("...");
						button.addSelectionListener(new SelectionAdapter() {
							@Override
							public void widgetSelected(SelectionEvent e) {
								Condition condition = (Condition) item
										.getData();
								ConditionEditingDialog dialog = openConditionEditingDialog(condition);
								if (dialog.open() == Window.OK) {
									updateText(item, dialog.getConditionText());
									cellComposite.dispose();
								}
							}
						});

						editor.setEditor(cellComposite, item, 1);
					}
				}
			}
		});
	}

	/**
	 * Opens the {@link ConditionEditingDialog} for the specified
	 * <code>condition</code>.
	 * 
	 * @param condition
	 *            to open for editing.
	 * @return the opened dialog.
	 */
	public ConditionEditingDialog openConditionEditingDialog(Condition condition) {
		ConditionEditingDialog dialog = new ConditionEditingDialog(PlatformUI
				.getWorkbench().getDisplay().getActiveShell().getShell(),
				condition, conditionEvaluationEngine);
		return dialog;
	}

	/**
	 * Adds the {@link ICheckStateListener} to {@link #treeViewer}.
	 */
	private void addCheckStateListener() {
		treeViewer.addCheckStateListener(new ICheckStateListener() {
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				if (event.getElement() instanceof Condition) {
					((Condition) event.getElement()).setActive(event
							.getChecked());
					notifyChangeListener(event);
					Object source = event.getSource();
					if (source instanceof CheckboxTreeViewer) {
						CheckboxTreeViewer checkboxTreeViewer = (CheckboxTreeViewer) source;
						checkboxTreeViewer.refresh(event.getElement(), true);
					}
				} else if (event.getElement() instanceof Template) {
					((Template) event.getElement()).setActive(event
							.getChecked());
					notifyChangeListener(event);
				}
			}
		});
	}

	/**
	 * Instantiates the actions.
	 */
	private void makeActions() {
		this.addCondition = new AddConditionAction(getTreeViewer(), this);
		this.removeCondition = new RemoveConditionAction(getTreeViewer());
		this.validateCondition = new ValidateConditionAction(getTreeViewer());
	}

	/**
	 * Hooks the context menus.
	 */
	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				ConditionView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(treeViewer.getControl());
		treeViewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, treeViewer);
	}

	/**
	 * Fills the context menu with actions.
	 * 
	 * @param manager
	 *            to add actions to.
	 */
	private void fillContextMenu(IMenuManager manager) {
		manager.add(addCondition);
		manager.add(removeCondition);
		manager.add(new Separator());
		manager.add(validateCondition);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	/**
	 * Contributes to action bars.
	 */
	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		// fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	/**
	 * Fills the tool bar with actions.
	 * 
	 * @param manager
	 *            to add actions to.
	 */
	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(addCondition);
		manager.add(removeCondition);
		manager.add(new Separator());
		manager.add(validateCondition);
	}

	/**
	 * Sets <code>text</code> according to the <code>item</code> type to the
	 * data object behind <code>item</code>.
	 * 
	 * @param item
	 *            to set <code>text</code> accordingly.
	 * @param text
	 *            to set.
	 */
	private void updateText(TreeItem item, String text) {
		// update data object
		Object element = item.getData();
		if (element instanceof Template) {
			((Template) element).setTitle(text);
		} else if (element instanceof Condition) {
			if (!text.equals(ConditionsUtil.getExpression((Condition) element))) {
				if (element instanceof CustomCondition) {
					((CustomCondition) element).setExpression(text);
				} else if (element instanceof FeatureCondition) {
					((FeatureCondition) element).setExpression(text);
				}
				((Condition) element).setState(State.EDITED);
				((Condition) element).setActive(true);
				treeViewer.setChecked(element, true);
			}
		}

		// update listener
		notifyChangeListener(new EventObject(element));

		// update tree viewer
		refresh();
	}

	/**
	 * Refreshes this view.
	 */
	public void refresh() {
		if (this.treeViewer != null) {
			this.treeViewer.refresh(true);
		}
	}

	/**
	 * Sets the <code>title</code> of this view, the {@link ConditionsModel} and
	 * the <code>checkedItems</code>.
	 * 
	 * @param title
	 *            to set.
	 * @param conditionsModel
	 *            to visualize.
	 * @param checkedItems
	 *            checked items in checked tree.
	 */
	public void show(String title, ConditionsModel conditionsModel,
			Object[] checkedItems) {

		this.setPartName(title);

		treeViewer.setInput(conditionsModel);
		treeViewer.setCheckedElements(checkedItems);
		refresh();
	}

	/**
	 * Sets the <code>title</code> of this view, the {@link ConditionsModel} and
	 * the <code>checkedItems</code>.
	 * 
	 * @param title
	 *            to set.
	 * @param titleImage
	 *            to set.
	 * @param conditionsModel
	 *            to visualize.
	 * @param checkedItems
	 *            checked items in checked tree.
	 */
	public void show(String title, Image titleImage,
			ConditionsModel conditionsModel, Object[] checkedItems) {

		this.setPartName(title);
		this.setTitleImage(titleImage);

		treeViewer.setInput(conditionsModel);
		treeViewer.setCheckedElements(checkedItems);
		refresh();
	}

	/**
	 * Sets the condition evaluation engine to use.
	 * 
	 * @param conditionEvaluationEngine
	 *            to set.
	 */
	public void setConditionEvaluationEngine(
			IConditionEvaluationEngine conditionEvaluationEngine) {
		this.conditionEvaluationEngine = conditionEvaluationEngine;
	}

	/**
	 * Does nothing.
	 */
	@Override
	public void setFocus() {
	}

	/**
	 * Returns the condition tree viewer.
	 * 
	 * @return the condition tree viewer.
	 */
	protected CheckboxTreeViewer getTreeViewer() {
		return this.treeViewer;
	}

}
