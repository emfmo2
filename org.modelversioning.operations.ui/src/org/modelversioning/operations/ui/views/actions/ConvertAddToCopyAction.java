/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.views.actions;

import org.eclipse.emf.compare.diff.metamodel.DiffFactory;
import org.eclipse.emf.compare.diff.metamodel.ModelElementChangeLeftTarget;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.diff.copydiff.CopyElementLeftTarget;
import org.modelversioning.operations.ui.commons.OperationsUICommonsPlugin;
import org.modelversioning.operations.ui.wizards.ConvertAddToCopyWizard;

/**
 * Converts a {@link ModelElementChangeLeftTarget} into a
 * {@link CopyElementLeftTarget} or vice versa.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ConvertAddToCopyAction extends Action {

	/**
	 * The {@link ModelElementChangeLeftTarget} to convert.
	 */
	private ModelElementChangeLeftTarget addDiffElement;
	/**
	 * The {@link CopyElementLeftTarget} to convert.
	 */
	private CopyElementLeftTarget copyModelElementLeftTarget;
	/**
	 * The {@link ConditionsModel} to get templates from.
	 */
	private ConditionsModel conditionsModel;

	/**
	 * Synchronizes the specified <code>runnable</code>.
	 * 
	 * @param runnable
	 *            to synchronize.
	 */
	protected void syncExec(Runnable runnable) {
		if (Display.getCurrent() == null) {
			Display.getDefault().syncExec(runnable);
		} else {
			runnable.run();
		}
	}

	/**
	 * Converts the {@link ModelElementChangeLeftTarget} into a
	 * {@link CopyElementLeftTarget} if {@link #addDiffElement} is set. If
	 * {@link #copyModelElementLeftTarget} is set instead, this method converts
	 * it into a {@link ModelElementChangeLeftTarget}.
	 */
	@Override
	public void run() {
		if (addDiffElement != null) {
			Runnable runnable = new Runnable() {
				public void run() {
					final ConvertAddToCopyWizard wizard = new ConvertAddToCopyWizard(
							conditionsModel, addDiffElement);
					WizardDialog wizardDialog = new WizardDialog(
							OperationsUICommonsPlugin.getShell(), wizard);
					wizardDialog.open();
				}
			};
			syncExec(runnable);
			addDiffElement = null;
		} else if (copyModelElementLeftTarget != null) {
			// create new ModelElement
			ModelElementChangeLeftTarget modelElementChangeLeftTarget = DiffFactory.eINSTANCE
					.createModelElementChangeLeftTarget();
			modelElementChangeLeftTarget
					.setLeftElement(copyModelElementLeftTarget.getLeftElement());
			modelElementChangeLeftTarget
					.setRightParent(copyModelElementLeftTarget.getRightParent());
			modelElementChangeLeftTarget.setRemote(copyModelElementLeftTarget
					.isRemote());
			EcoreUtil.replace(copyModelElementLeftTarget,
					modelElementChangeLeftTarget);
			copyModelElementLeftTarget = null;
		}
		super.run();
	}

	/**
	 * Sets the conditions model to get templates from.
	 * 
	 * @param conditionsModel
	 *            the {@link ConditionsModel} to set.
	 */
	public void setConditionsModel(ConditionsModel conditionsModel) {
		this.conditionsModel = conditionsModel;
	}

	/**
	 * Sets the {@link ModelElementChangeLeftTarget} to convert.
	 * 
	 * @param addDiffElement
	 *            to convert.
	 */
	public void setAddModelElement(ModelElementChangeLeftTarget addDiffElement) {
		this.addDiffElement = addDiffElement;
	}

	/**
	 * Sets the {@link CopyElementLeftTarget} to convert.
	 * 
	 * @param copyModelElementLeftTarget
	 *            to convert.
	 */
	public void setCopyModelElement(
			CopyElementLeftTarget copyModelElementLeftTarget) {
		this.copyModelElementLeftTarget = copyModelElementLeftTarget;
	}

	

}
