/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.views.actions;

import java.util.Iterator;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;

/**
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * @author Felix Rinker
 * 
 */
public class RemoveConditionAction extends AbstractConditionsModelAction {

	/**
	 * Constructing a new {@link RemoveConditionAction} for the specified
	 * <code>treeViewer</code>.
	 * 
	 * @param treeViewer
	 *            for which this action is made for.
	 */
	public RemoveConditionAction(TreeViewer treeViewer) {
		super(treeViewer);
		super.setSelectionCountMin(1);
		super.setSelectionCountMax(-1);

		this.setText("Remove Condition");
		this.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_ELCL_REMOVE));
	}

	/**
	 * Initiates removing of the selected condition.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		if (getTreeViewer().getInput() instanceof ConditionsModel) {
			IStructuredSelection selection = getSelection();
			if (selection != null) {
				Iterator<Object> iterator = selection.iterator();
				// TODO ask if user is sure to delete
				while (iterator.hasNext()) {
					Object next = iterator.next();
					if (next instanceof Condition) {
						Condition condition = (Condition) next;
						condition.getTemplate().getSpecifications().remove(
								condition);
					}
				}
				getTreeViewer().refresh();
			}
		}
	}

}
