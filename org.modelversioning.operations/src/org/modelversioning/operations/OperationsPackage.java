/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.modelversioning.operations;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.modelversioning.core.conditions.ConditionsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.modelversioning.operations.OperationsFactory
 * @model kind="package"
 * @generated
 */
public interface OperationsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "operations";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://modelversioning.org/operations/metamodel/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "operations";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OperationsPackage eINSTANCE = org.modelversioning.operations.impl.OperationsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.modelversioning.operations.impl.OperationSpecificationImpl <em>Operation Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.operations.impl.OperationSpecificationImpl
	 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getOperationSpecification()
	 * @generated
	 */
	int OPERATION_SPECIFICATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Title Template</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__TITLE_TEMPLATE = 2;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__VERSION = 3;

	/**
	 * The feature id for the '<em><b>Is Refactoring</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__IS_REFACTORING = 4;

	/**
	 * The feature id for the '<em><b>Modeling Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__MODELING_LANGUAGE = 5;

	/**
	 * The feature id for the '<em><b>Model File Extension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__MODEL_FILE_EXTENSION = 6;

	/**
	 * The feature id for the '<em><b>Diagram File Extension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__DIAGRAM_FILE_EXTENSION = 7;

	/**
	 * The feature id for the '<em><b>Editor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__EDITOR_ID = 8;

	/**
	 * The feature id for the '<em><b>Working Model Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__WORKING_MODEL_ROOT = 9;

	/**
	 * The feature id for the '<em><b>Working Diagram</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__WORKING_DIAGRAM = 10;

	/**
	 * The feature id for the '<em><b>Origin Model Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT = 11;

	/**
	 * The feature id for the '<em><b>Origin Diagram</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__ORIGIN_DIAGRAM = 12;

	/**
	 * The feature id for the '<em><b>Difference Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__DIFFERENCE_MODEL = 13;

	/**
	 * The feature id for the '<em><b>Iterations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__ITERATIONS = 14;

	/**
	 * The feature id for the '<em><b>User Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__USER_INPUTS = 15;

	/**
	 * The feature id for the '<em><b>Preconditions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__PRECONDITIONS = 16;

	/**
	 * The feature id for the '<em><b>Postconditions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__POSTCONDITIONS = 17;

	/**
	 * The feature id for the '<em><b>Negative Application Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__NEGATIVE_APPLICATION_CONDITIONS = 18;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION__STATE = 19;

	/**
	 * The number of structural features of the '<em>Operation Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_SPECIFICATION_FEATURE_COUNT = 20;

	/**
	 * The meta object id for the '{@link org.modelversioning.operations.impl.IterationImpl <em>Iteration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.operations.impl.IterationImpl
	 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getIteration()
	 * @generated
	 */
	int ITERATION = 1;

	/**
	 * The feature id for the '<em><b>Iteration Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION__ITERATION_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Diff Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION__DIFF_ELEMENTS = 1;

	/**
	 * The feature id for the '<em><b>Sub Iterations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION__SUB_ITERATIONS = 2;

	/**
	 * The feature id for the '<em><b>Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION__TEMPLATE = 3;

	/**
	 * The number of structural features of the '<em>Iteration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.modelversioning.operations.impl.UserInputImpl <em>User Input</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.operations.impl.UserInputImpl
	 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getUserInput()
	 * @generated
	 */
	int USER_INPUT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_INPUT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_INPUT__FEATURE = 1;

	/**
	 * The feature id for the '<em><b>Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_INPUT__TEMPLATE = 2;

	/**
	 * The number of structural features of the '<em>User Input</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_INPUT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.modelversioning.operations.impl.NegativeApplicationConditionImpl <em>Negative Application Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.operations.impl.NegativeApplicationConditionImpl
	 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getNegativeApplicationCondition()
	 * @generated
	 */
	int NEGATIVE_APPLICATION_CONDITION = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Error Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION__ERROR_MESSAGE = 2;

	/**
	 * The feature id for the '<em><b>Model Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT = 3;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION__DIAGRAM = 4;

	/**
	 * The feature id for the '<em><b>Difference Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL = 5;

	/**
	 * The feature id for the '<em><b>Conditions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION__CONDITIONS = 6;

	/**
	 * The feature id for the '<em><b>Operation Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION = 7;

	/**
	 * The number of structural features of the '<em>Negative Application Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_FEATURE_COUNT = 8;

	/**
	 * The meta object id for the '{@link org.modelversioning.operations.impl.NegativeApplicationConditionResultImpl <em>Negative Application Condition Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.operations.impl.NegativeApplicationConditionResultImpl
	 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getNegativeApplicationConditionResult()
	 * @generated
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT = 4;

	/**
	 * The feature id for the '<em><b>Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT__MESSAGE = ConditionsPackage.EVALUATION_RESULT__MESSAGE;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT__STATUS = ConditionsPackage.EVALUATION_RESULT__STATUS;

	/**
	 * The feature id for the '<em><b>Exception</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT__EXCEPTION = ConditionsPackage.EVALUATION_RESULT__EXCEPTION;

	/**
	 * The feature id for the '<em><b>Evaluator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT__EVALUATOR = ConditionsPackage.EVALUATION_RESULT__EVALUATOR;

	/**
	 * The feature id for the '<em><b>Sub Results</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT__SUB_RESULTS = ConditionsPackage.EVALUATION_RESULT__SUB_RESULTS;

	/**
	 * The feature id for the '<em><b>Parent Result</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT__PARENT_RESULT = ConditionsPackage.EVALUATION_RESULT__PARENT_RESULT;

	/**
	 * The feature id for the '<em><b>Failed Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT__FAILED_CONDITION = ConditionsPackage.EVALUATION_RESULT__FAILED_CONDITION;

	/**
	 * The feature id for the '<em><b>Failed Candidate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT__FAILED_CANDIDATE = ConditionsPackage.EVALUATION_RESULT__FAILED_CANDIDATE;

	/**
	 * The feature id for the '<em><b>Negative Application Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT__NEGATIVE_APPLICATION_CONDITION = ConditionsPackage.EVALUATION_RESULT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING = ConditionsPackage.EVALUATION_RESULT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Negative Application Condition Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_APPLICATION_CONDITION_RESULT_FEATURE_COUNT = ConditionsPackage.EVALUATION_RESULT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.modelversioning.operations.IterationType <em>Iteration Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.operations.IterationType
	 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getIterationType()
	 * @generated
	 */
	int ITERATION_TYPE = 5;


	/**
	 * The meta object id for the '{@link org.modelversioning.operations.SpecificationState <em>Specification State</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.operations.SpecificationState
	 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getSpecificationState()
	 * @generated
	 */
	int SPECIFICATION_STATE = 6;


	/**
	 * Returns the meta object for class '{@link org.modelversioning.operations.OperationSpecification <em>Operation Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Specification</em>'.
	 * @see org.modelversioning.operations.OperationSpecification
	 * @generated
	 */
	EClass getOperationSpecification();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.OperationSpecification#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getName()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EAttribute getOperationSpecification_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.OperationSpecification#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getDescription()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EAttribute getOperationSpecification_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.OperationSpecification#getTitleTemplate <em>Title Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title Template</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getTitleTemplate()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EAttribute getOperationSpecification_TitleTemplate();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.OperationSpecification#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getVersion()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EAttribute getOperationSpecification_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.OperationSpecification#getModelingLanguage <em>Modeling Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Modeling Language</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getModelingLanguage()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EAttribute getOperationSpecification_ModelingLanguage();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.OperationSpecification#getModelFileExtension <em>Model File Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model File Extension</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getModelFileExtension()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EAttribute getOperationSpecification_ModelFileExtension();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.OperationSpecification#getDiagramFileExtension <em>Diagram File Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Diagram File Extension</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getDiagramFileExtension()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EAttribute getOperationSpecification_DiagramFileExtension();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.OperationSpecification#getEditorId <em>Editor Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Editor Id</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getEditorId()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EAttribute getOperationSpecification_EditorId();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.OperationSpecification#isIsRefactoring <em>Is Refactoring</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Refactoring</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#isIsRefactoring()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EAttribute getOperationSpecification_IsRefactoring();

	/**
	 * Returns the meta object for the containment reference '{@link org.modelversioning.operations.OperationSpecification#getWorkingModelRoot <em>Working Model Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Working Model Root</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getWorkingModelRoot()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EReference getOperationSpecification_WorkingModelRoot();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.operations.OperationSpecification#getWorkingDiagram <em>Working Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Working Diagram</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getWorkingDiagram()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EReference getOperationSpecification_WorkingDiagram();

	/**
	 * Returns the meta object for the containment reference '{@link org.modelversioning.operations.OperationSpecification#getOriginModelRoot <em>Origin Model Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Origin Model Root</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getOriginModelRoot()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EReference getOperationSpecification_OriginModelRoot();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.operations.OperationSpecification#getOriginDiagram <em>Origin Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Origin Diagram</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getOriginDiagram()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EReference getOperationSpecification_OriginDiagram();

	/**
	 * Returns the meta object for the containment reference '{@link org.modelversioning.operations.OperationSpecification#getDifferenceModel <em>Difference Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Difference Model</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getDifferenceModel()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EReference getOperationSpecification_DifferenceModel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.operations.OperationSpecification#getIterations <em>Iterations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Iterations</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getIterations()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EReference getOperationSpecification_Iterations();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.operations.OperationSpecification#getUserInputs <em>User Inputs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>User Inputs</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getUserInputs()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EReference getOperationSpecification_UserInputs();

	/**
	 * Returns the meta object for the containment reference '{@link org.modelversioning.operations.OperationSpecification#getPreconditions <em>Preconditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Preconditions</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getPreconditions()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EReference getOperationSpecification_Preconditions();

	/**
	 * Returns the meta object for the containment reference '{@link org.modelversioning.operations.OperationSpecification#getPostconditions <em>Postconditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Postconditions</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getPostconditions()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EReference getOperationSpecification_Postconditions();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.operations.OperationSpecification#getNegativeApplicationConditions <em>Negative Application Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Negative Application Conditions</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getNegativeApplicationConditions()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EReference getOperationSpecification_NegativeApplicationConditions();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.OperationSpecification#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see org.modelversioning.operations.OperationSpecification#getState()
	 * @see #getOperationSpecification()
	 * @generated
	 */
	EAttribute getOperationSpecification_State();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.operations.Iteration <em>Iteration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iteration</em>'.
	 * @see org.modelversioning.operations.Iteration
	 * @generated
	 */
	EClass getIteration();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.Iteration#getIterationType <em>Iteration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Iteration Type</em>'.
	 * @see org.modelversioning.operations.Iteration#getIterationType()
	 * @see #getIteration()
	 * @generated
	 */
	EAttribute getIteration_IterationType();

	/**
	 * Returns the meta object for the reference list '{@link org.modelversioning.operations.Iteration#getDiffElements <em>Diff Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Diff Elements</em>'.
	 * @see org.modelversioning.operations.Iteration#getDiffElements()
	 * @see #getIteration()
	 * @generated
	 */
	EReference getIteration_DiffElements();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.operations.Iteration#getSubIterations <em>Sub Iterations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Iterations</em>'.
	 * @see org.modelversioning.operations.Iteration#getSubIterations()
	 * @see #getIteration()
	 * @generated
	 */
	EReference getIteration_SubIterations();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.operations.Iteration#getTemplate <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Template</em>'.
	 * @see org.modelversioning.operations.Iteration#getTemplate()
	 * @see #getIteration()
	 * @generated
	 */
	EReference getIteration_Template();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.operations.UserInput <em>User Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Input</em>'.
	 * @see org.modelversioning.operations.UserInput
	 * @generated
	 */
	EClass getUserInput();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.UserInput#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.modelversioning.operations.UserInput#getName()
	 * @see #getUserInput()
	 * @generated
	 */
	EAttribute getUserInput_Name();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.operations.UserInput#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see org.modelversioning.operations.UserInput#getFeature()
	 * @see #getUserInput()
	 * @generated
	 */
	EReference getUserInput_Feature();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.operations.UserInput#getTemplate <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Template</em>'.
	 * @see org.modelversioning.operations.UserInput#getTemplate()
	 * @see #getUserInput()
	 * @generated
	 */
	EReference getUserInput_Template();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.operations.NegativeApplicationCondition <em>Negative Application Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Negative Application Condition</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationCondition
	 * @generated
	 */
	EClass getNegativeApplicationCondition();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.NegativeApplicationCondition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationCondition#getName()
	 * @see #getNegativeApplicationCondition()
	 * @generated
	 */
	EAttribute getNegativeApplicationCondition_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.NegativeApplicationCondition#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationCondition#getDescription()
	 * @see #getNegativeApplicationCondition()
	 * @generated
	 */
	EAttribute getNegativeApplicationCondition_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.operations.NegativeApplicationCondition#getErrorMessage <em>Error Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Error Message</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationCondition#getErrorMessage()
	 * @see #getNegativeApplicationCondition()
	 * @generated
	 */
	EAttribute getNegativeApplicationCondition_ErrorMessage();

	/**
	 * Returns the meta object for the containment reference '{@link org.modelversioning.operations.NegativeApplicationCondition#getModelRoot <em>Model Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Model Root</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationCondition#getModelRoot()
	 * @see #getNegativeApplicationCondition()
	 * @generated
	 */
	EReference getNegativeApplicationCondition_ModelRoot();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.operations.NegativeApplicationCondition#getDiagram <em>Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Diagram</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationCondition#getDiagram()
	 * @see #getNegativeApplicationCondition()
	 * @generated
	 */
	EReference getNegativeApplicationCondition_Diagram();

	/**
	 * Returns the meta object for the containment reference '{@link org.modelversioning.operations.NegativeApplicationCondition#getDifferenceModel <em>Difference Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Difference Model</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationCondition#getDifferenceModel()
	 * @see #getNegativeApplicationCondition()
	 * @generated
	 */
	EReference getNegativeApplicationCondition_DifferenceModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.modelversioning.operations.NegativeApplicationCondition#getConditions <em>Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Conditions</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationCondition#getConditions()
	 * @see #getNegativeApplicationCondition()
	 * @generated
	 */
	EReference getNegativeApplicationCondition_Conditions();

	/**
	 * Returns the meta object for the container reference '{@link org.modelversioning.operations.NegativeApplicationCondition#getOperationSpecification <em>Operation Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Operation Specification</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationCondition#getOperationSpecification()
	 * @see #getNegativeApplicationCondition()
	 * @generated
	 */
	EReference getNegativeApplicationCondition_OperationSpecification();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.operations.NegativeApplicationConditionResult <em>Negative Application Condition Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Negative Application Condition Result</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationConditionResult
	 * @generated
	 */
	EClass getNegativeApplicationConditionResult();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.operations.NegativeApplicationConditionResult#getNegativeApplicationCondition <em>Negative Application Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Negative Application Condition</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationConditionResult#getNegativeApplicationCondition()
	 * @see #getNegativeApplicationConditionResult()
	 * @generated
	 */
	EReference getNegativeApplicationConditionResult_NegativeApplicationCondition();

	/**
	 * Returns the meta object for the containment reference '{@link org.modelversioning.operations.NegativeApplicationConditionResult#getBinding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Binding</em>'.
	 * @see org.modelversioning.operations.NegativeApplicationConditionResult#getBinding()
	 * @see #getNegativeApplicationConditionResult()
	 * @generated
	 */
	EReference getNegativeApplicationConditionResult_Binding();

	/**
	 * Returns the meta object for enum '{@link org.modelversioning.operations.IterationType <em>Iteration Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Iteration Type</em>'.
	 * @see org.modelversioning.operations.IterationType
	 * @generated
	 */
	EEnum getIterationType();

	/**
	 * Returns the meta object for enum '{@link org.modelversioning.operations.SpecificationState <em>Specification State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Specification State</em>'.
	 * @see org.modelversioning.operations.SpecificationState
	 * @generated
	 */
	EEnum getSpecificationState();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OperationsFactory getOperationsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.modelversioning.operations.impl.OperationSpecificationImpl <em>Operation Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.operations.impl.OperationSpecificationImpl
		 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getOperationSpecification()
		 * @generated
		 */
		EClass OPERATION_SPECIFICATION = eINSTANCE.getOperationSpecification();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_SPECIFICATION__NAME = eINSTANCE.getOperationSpecification_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_SPECIFICATION__DESCRIPTION = eINSTANCE.getOperationSpecification_Description();

		/**
		 * The meta object literal for the '<em><b>Title Template</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_SPECIFICATION__TITLE_TEMPLATE = eINSTANCE.getOperationSpecification_TitleTemplate();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_SPECIFICATION__VERSION = eINSTANCE.getOperationSpecification_Version();

		/**
		 * The meta object literal for the '<em><b>Modeling Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_SPECIFICATION__MODELING_LANGUAGE = eINSTANCE.getOperationSpecification_ModelingLanguage();

		/**
		 * The meta object literal for the '<em><b>Model File Extension</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_SPECIFICATION__MODEL_FILE_EXTENSION = eINSTANCE.getOperationSpecification_ModelFileExtension();

		/**
		 * The meta object literal for the '<em><b>Diagram File Extension</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_SPECIFICATION__DIAGRAM_FILE_EXTENSION = eINSTANCE.getOperationSpecification_DiagramFileExtension();

		/**
		 * The meta object literal for the '<em><b>Editor Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_SPECIFICATION__EDITOR_ID = eINSTANCE.getOperationSpecification_EditorId();

		/**
		 * The meta object literal for the '<em><b>Is Refactoring</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_SPECIFICATION__IS_REFACTORING = eINSTANCE.getOperationSpecification_IsRefactoring();

		/**
		 * The meta object literal for the '<em><b>Working Model Root</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_SPECIFICATION__WORKING_MODEL_ROOT = eINSTANCE.getOperationSpecification_WorkingModelRoot();

		/**
		 * The meta object literal for the '<em><b>Working Diagram</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_SPECIFICATION__WORKING_DIAGRAM = eINSTANCE.getOperationSpecification_WorkingDiagram();

		/**
		 * The meta object literal for the '<em><b>Origin Model Root</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT = eINSTANCE.getOperationSpecification_OriginModelRoot();

		/**
		 * The meta object literal for the '<em><b>Origin Diagram</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_SPECIFICATION__ORIGIN_DIAGRAM = eINSTANCE.getOperationSpecification_OriginDiagram();

		/**
		 * The meta object literal for the '<em><b>Difference Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_SPECIFICATION__DIFFERENCE_MODEL = eINSTANCE.getOperationSpecification_DifferenceModel();

		/**
		 * The meta object literal for the '<em><b>Iterations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_SPECIFICATION__ITERATIONS = eINSTANCE.getOperationSpecification_Iterations();

		/**
		 * The meta object literal for the '<em><b>User Inputs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_SPECIFICATION__USER_INPUTS = eINSTANCE.getOperationSpecification_UserInputs();

		/**
		 * The meta object literal for the '<em><b>Preconditions</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_SPECIFICATION__PRECONDITIONS = eINSTANCE.getOperationSpecification_Preconditions();

		/**
		 * The meta object literal for the '<em><b>Postconditions</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_SPECIFICATION__POSTCONDITIONS = eINSTANCE.getOperationSpecification_Postconditions();

		/**
		 * The meta object literal for the '<em><b>Negative Application Conditions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_SPECIFICATION__NEGATIVE_APPLICATION_CONDITIONS = eINSTANCE.getOperationSpecification_NegativeApplicationConditions();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_SPECIFICATION__STATE = eINSTANCE.getOperationSpecification_State();

		/**
		 * The meta object literal for the '{@link org.modelversioning.operations.impl.IterationImpl <em>Iteration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.operations.impl.IterationImpl
		 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getIteration()
		 * @generated
		 */
		EClass ITERATION = eINSTANCE.getIteration();

		/**
		 * The meta object literal for the '<em><b>Iteration Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ITERATION__ITERATION_TYPE = eINSTANCE.getIteration_IterationType();

		/**
		 * The meta object literal for the '<em><b>Diff Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITERATION__DIFF_ELEMENTS = eINSTANCE.getIteration_DiffElements();

		/**
		 * The meta object literal for the '<em><b>Sub Iterations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITERATION__SUB_ITERATIONS = eINSTANCE.getIteration_SubIterations();

		/**
		 * The meta object literal for the '<em><b>Template</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITERATION__TEMPLATE = eINSTANCE.getIteration_Template();

		/**
		 * The meta object literal for the '{@link org.modelversioning.operations.impl.UserInputImpl <em>User Input</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.operations.impl.UserInputImpl
		 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getUserInput()
		 * @generated
		 */
		EClass USER_INPUT = eINSTANCE.getUserInput();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_INPUT__NAME = eINSTANCE.getUserInput_Name();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER_INPUT__FEATURE = eINSTANCE.getUserInput_Feature();

		/**
		 * The meta object literal for the '<em><b>Template</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER_INPUT__TEMPLATE = eINSTANCE.getUserInput_Template();

		/**
		 * The meta object literal for the '{@link org.modelversioning.operations.impl.NegativeApplicationConditionImpl <em>Negative Application Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.operations.impl.NegativeApplicationConditionImpl
		 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getNegativeApplicationCondition()
		 * @generated
		 */
		EClass NEGATIVE_APPLICATION_CONDITION = eINSTANCE.getNegativeApplicationCondition();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NEGATIVE_APPLICATION_CONDITION__NAME = eINSTANCE.getNegativeApplicationCondition_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NEGATIVE_APPLICATION_CONDITION__DESCRIPTION = eINSTANCE.getNegativeApplicationCondition_Description();

		/**
		 * The meta object literal for the '<em><b>Error Message</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NEGATIVE_APPLICATION_CONDITION__ERROR_MESSAGE = eINSTANCE.getNegativeApplicationCondition_ErrorMessage();

		/**
		 * The meta object literal for the '<em><b>Model Root</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT = eINSTANCE.getNegativeApplicationCondition_ModelRoot();

		/**
		 * The meta object literal for the '<em><b>Diagram</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEGATIVE_APPLICATION_CONDITION__DIAGRAM = eINSTANCE.getNegativeApplicationCondition_Diagram();

		/**
		 * The meta object literal for the '<em><b>Difference Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL = eINSTANCE.getNegativeApplicationCondition_DifferenceModel();

		/**
		 * The meta object literal for the '<em><b>Conditions</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEGATIVE_APPLICATION_CONDITION__CONDITIONS = eINSTANCE.getNegativeApplicationCondition_Conditions();

		/**
		 * The meta object literal for the '<em><b>Operation Specification</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION = eINSTANCE.getNegativeApplicationCondition_OperationSpecification();

		/**
		 * The meta object literal for the '{@link org.modelversioning.operations.impl.NegativeApplicationConditionResultImpl <em>Negative Application Condition Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.operations.impl.NegativeApplicationConditionResultImpl
		 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getNegativeApplicationConditionResult()
		 * @generated
		 */
		EClass NEGATIVE_APPLICATION_CONDITION_RESULT = eINSTANCE.getNegativeApplicationConditionResult();

		/**
		 * The meta object literal for the '<em><b>Negative Application Condition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEGATIVE_APPLICATION_CONDITION_RESULT__NEGATIVE_APPLICATION_CONDITION = eINSTANCE.getNegativeApplicationConditionResult_NegativeApplicationCondition();

		/**
		 * The meta object literal for the '<em><b>Binding</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING = eINSTANCE.getNegativeApplicationConditionResult_Binding();

		/**
		 * The meta object literal for the '{@link org.modelversioning.operations.IterationType <em>Iteration Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.operations.IterationType
		 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getIterationType()
		 * @generated
		 */
		EEnum ITERATION_TYPE = eINSTANCE.getIterationType();

		/**
		 * The meta object literal for the '{@link org.modelversioning.operations.SpecificationState <em>Specification State</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.operations.SpecificationState
		 * @see org.modelversioning.operations.impl.OperationsPackageImpl#getSpecificationState()
		 * @generated
		 */
		EEnum SPECIFICATION_STATE = eINSTANCE.getSpecificationState();

	}

} //OperationsPackage
