/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.engines;

import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.resource.Resource;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.engines.IConditionGenerationEngine;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.OperationSpecification;

/**
 * Interface to conveniently generate an initial {@link OperationSpecification} definition.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IOperationGenerationEngine {

	/**
	 * Returns the currently used {@link IConditionGenerationEngine}.
	 * 
	 * @return the condition generation engine currently used.
	 */
	public IConditionGenerationEngine getConditionGenerationEngine();

	/**
	 * Sets the {@link IConditionGenerationEngine} to use.
	 * 
	 * @return the condition generation engine to use.
	 */
	public void setConditionGenerationEngine(
			IConditionGenerationEngine conditionGenerationEngine);

	/**
	 * Generates an initial {@link OperationSpecification} with an initial model
	 * only and the specified <code>initialDiagram</code> which may be
	 * <code>null</code> if no diagram information shall be saved..
	 * 
	 * @param initial
	 *            the {@link Resource} of the initial model.
	 * @param initialDiagram
	 *            the {@link Resource} of the initial diagram (may be
	 *            <code>null</code> if not applicable).
	 * @return an initial {@link OperationSpecification} with the initial model.
	 */
	public OperationSpecification generateInitialOperationSpecification(
			Resource origin, Resource initialDiagram);

	/**
	 * Adds the revised model to the specified existing
	 * <code>operationSpecification</code> by setting the revised model
	 * specified by the <code>matchModel</code> and generates the revised
	 * {@link ConditionsModel}.
	 * 
	 * @param matchModel
	 *            matching the currently set initial model in the specified
	 *            <code>operationSpecification</code> and the revised model to
	 *            put into the specified <code>operationSpecification</code>.
	 * @param revisedDiagramResource
	 *            the {@link Resource} of the revised diagram (may be
	 *            <code>null</code> if not applicable).
	 * @param operationSpecification
	 *            to put revised model (referenced by the match model) to.
	 */
	public void putRevisedModel(MatchModel matchModel,
			Resource revisedDiagramResource,
			OperationSpecification operationSpecification);

	/**
	 * Generates an {@link OperationSpecification} for the models matched by the
	 * specified {@link MatchModel}.
	 * 
	 * @param matchModel
	 *            {@link MatchModel} matching the initial and revised model.
	 * @return generated initial {@link OperationSpecification}.
	 */
	public OperationSpecification generateOperationSpecification(
			MatchModel matchModel);

	/**
	 * Generates an {@link OperationSpecification} for the specified
	 * <code>origin</code> and <code>working</code> resource.
	 * 
	 * @param initial
	 *            the {@link Resource} of the initial model.
	 * @param revised
	 *            the {@link Resource} of the revised model.
	 * @return generated initial {@link OperationSpecification}.
	 */
	public OperationSpecification generateOperationSpecification(
			Resource initial, Resource revised) throws MatchException;

	/**
	 * Creates a new {@link NegativeApplicationCondition} for the demonstration
	 * specified by the <code>matchModel</code> and adds it to the specified
	 * <code>operationSpecification</code>.
	 * 
	 * Optionally, you may provide a diagram which will also be added to the
	 * created {@link NegativeApplicationCondition}.
	 * 
	 * @param matchModel
	 *            representing the demonstration.
	 * @param diagramResource
	 *            optional diagram resource (may be <code>null</code>).
	 * @param operationSpecification
	 *            the specification to add the created NAC to.
	 * @return the created NAC.
	 */
	public NegativeApplicationCondition createNegativeApplicationCondition(
			MatchModel matchModel, Resource diagramResource,
			OperationSpecification operationSpecification);

}
