/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.modelversioning.operations.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.operations.Iteration;
import org.modelversioning.operations.IterationType;
import org.modelversioning.operations.OperationsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iteration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.operations.impl.IterationImpl#getIterationType <em>Iteration Type</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.IterationImpl#getDiffElements <em>Diff Elements</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.IterationImpl#getSubIterations <em>Sub Iterations</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.IterationImpl#getTemplate <em>Template</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IterationImpl extends EObjectImpl implements Iteration {
	/**
	 * The default value of the '{@link #getIterationType() <em>Iteration Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIterationType()
	 * @generated
	 * @ordered
	 */
	protected static final IterationType ITERATION_TYPE_EDEFAULT = IterationType.FOR_ALL;

	/**
	 * The cached value of the '{@link #getIterationType() <em>Iteration Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIterationType()
	 * @generated
	 * @ordered
	 */
	protected IterationType iterationType = ITERATION_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDiffElements() <em>Diff Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiffElements()
	 * @generated
	 * @ordered
	 */
	protected EList<DiffElement> diffElements;

	/**
	 * The cached value of the '{@link #getSubIterations() <em>Sub Iterations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubIterations()
	 * @generated
	 * @ordered
	 */
	protected EList<Iteration> subIterations;

	/**
	 * The cached value of the '{@link #getTemplate() <em>Template</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemplate()
	 * @generated
	 * @ordered
	 */
	protected Template template;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IterationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperationsPackage.Literals.ITERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IterationType getIterationType() {
		return iterationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIterationType(IterationType newIterationType) {
		IterationType oldIterationType = iterationType;
		iterationType = newIterationType == null ? ITERATION_TYPE_EDEFAULT : newIterationType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.ITERATION__ITERATION_TYPE, oldIterationType, iterationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiffElement> getDiffElements() {
		if (diffElements == null) {
			diffElements = new EObjectResolvingEList<DiffElement>(DiffElement.class, this, OperationsPackage.ITERATION__DIFF_ELEMENTS);
		}
		return diffElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Iteration> getSubIterations() {
		if (subIterations == null) {
			subIterations = new EObjectContainmentEList<Iteration>(Iteration.class, this, OperationsPackage.ITERATION__SUB_ITERATIONS);
		}
		return subIterations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template getTemplate() {
		if (template != null && template.eIsProxy()) {
			InternalEObject oldTemplate = (InternalEObject)template;
			template = (Template)eResolveProxy(oldTemplate);
			if (template != oldTemplate) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OperationsPackage.ITERATION__TEMPLATE, oldTemplate, template));
			}
		}
		return template;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template basicGetTemplate() {
		return template;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemplate(Template newTemplate) {
		Template oldTemplate = template;
		template = newTemplate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.ITERATION__TEMPLATE, oldTemplate, template));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperationsPackage.ITERATION__SUB_ITERATIONS:
				return ((InternalEList<?>)getSubIterations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperationsPackage.ITERATION__ITERATION_TYPE:
				return getIterationType();
			case OperationsPackage.ITERATION__DIFF_ELEMENTS:
				return getDiffElements();
			case OperationsPackage.ITERATION__SUB_ITERATIONS:
				return getSubIterations();
			case OperationsPackage.ITERATION__TEMPLATE:
				if (resolve) return getTemplate();
				return basicGetTemplate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperationsPackage.ITERATION__ITERATION_TYPE:
				setIterationType((IterationType)newValue);
				return;
			case OperationsPackage.ITERATION__DIFF_ELEMENTS:
				getDiffElements().clear();
				getDiffElements().addAll((Collection<? extends DiffElement>)newValue);
				return;
			case OperationsPackage.ITERATION__SUB_ITERATIONS:
				getSubIterations().clear();
				getSubIterations().addAll((Collection<? extends Iteration>)newValue);
				return;
			case OperationsPackage.ITERATION__TEMPLATE:
				setTemplate((Template)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperationsPackage.ITERATION__ITERATION_TYPE:
				setIterationType(ITERATION_TYPE_EDEFAULT);
				return;
			case OperationsPackage.ITERATION__DIFF_ELEMENTS:
				getDiffElements().clear();
				return;
			case OperationsPackage.ITERATION__SUB_ITERATIONS:
				getSubIterations().clear();
				return;
			case OperationsPackage.ITERATION__TEMPLATE:
				setTemplate((Template)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperationsPackage.ITERATION__ITERATION_TYPE:
				return iterationType != ITERATION_TYPE_EDEFAULT;
			case OperationsPackage.ITERATION__DIFF_ELEMENTS:
				return diffElements != null && !diffElements.isEmpty();
			case OperationsPackage.ITERATION__SUB_ITERATIONS:
				return subIterations != null && !subIterations.isEmpty();
			case OperationsPackage.ITERATION__TEMPLATE:
				return template != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (iterationType: ");
		result.append(iterationType);
		result.append(')');
		return result.toString();
	}

} //IterationImpl
