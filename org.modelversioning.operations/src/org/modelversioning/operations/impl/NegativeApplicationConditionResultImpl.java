/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.modelversioning.operations.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.modelversioning.core.conditions.impl.EvaluationResultImpl;
import org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.NegativeApplicationConditionResult;
import org.modelversioning.operations.OperationsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Negative Application Condition Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.operations.impl.NegativeApplicationConditionResultImpl#getNegativeApplicationCondition <em>Negative Application Condition</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.NegativeApplicationConditionResultImpl#getBinding <em>Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NegativeApplicationConditionResultImpl extends EvaluationResultImpl implements NegativeApplicationConditionResult {
	/**
	 * The cached value of the '{@link #getNegativeApplicationCondition() <em>Negative Application Condition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNegativeApplicationCondition()
	 * @generated
	 * @ordered
	 */
	protected NegativeApplicationCondition negativeApplicationCondition;

	/**
	 * The cached value of the '{@link #getBinding() <em>Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinding()
	 * @generated
	 * @ordered
	 */
	protected TemplateBindingCollection binding;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NegativeApplicationConditionResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperationsPackage.Literals.NEGATIVE_APPLICATION_CONDITION_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NegativeApplicationCondition getNegativeApplicationCondition() {
		if (negativeApplicationCondition != null && negativeApplicationCondition.eIsProxy()) {
			InternalEObject oldNegativeApplicationCondition = (InternalEObject)negativeApplicationCondition;
			negativeApplicationCondition = (NegativeApplicationCondition)eResolveProxy(oldNegativeApplicationCondition);
			if (negativeApplicationCondition != oldNegativeApplicationCondition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__NEGATIVE_APPLICATION_CONDITION, oldNegativeApplicationCondition, negativeApplicationCondition));
			}
		}
		return negativeApplicationCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NegativeApplicationCondition basicGetNegativeApplicationCondition() {
		return negativeApplicationCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNegativeApplicationCondition(NegativeApplicationCondition newNegativeApplicationCondition) {
		NegativeApplicationCondition oldNegativeApplicationCondition = negativeApplicationCondition;
		negativeApplicationCondition = newNegativeApplicationCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__NEGATIVE_APPLICATION_CONDITION, oldNegativeApplicationCondition, negativeApplicationCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateBindingCollection getBinding() {
		return binding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBinding(TemplateBindingCollection newBinding, NotificationChain msgs) {
		TemplateBindingCollection oldBinding = binding;
		binding = newBinding;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING, oldBinding, newBinding);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBinding(TemplateBindingCollection newBinding) {
		if (newBinding != binding) {
			NotificationChain msgs = null;
			if (binding != null)
				msgs = ((InternalEObject)binding).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING, null, msgs);
			if (newBinding != null)
				msgs = ((InternalEObject)newBinding).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING, null, msgs);
			msgs = basicSetBinding(newBinding, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING, newBinding, newBinding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING:
				return basicSetBinding(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__NEGATIVE_APPLICATION_CONDITION:
				if (resolve) return getNegativeApplicationCondition();
				return basicGetNegativeApplicationCondition();
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING:
				return getBinding();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__NEGATIVE_APPLICATION_CONDITION:
				setNegativeApplicationCondition((NegativeApplicationCondition)newValue);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING:
				setBinding((TemplateBindingCollection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__NEGATIVE_APPLICATION_CONDITION:
				setNegativeApplicationCondition((NegativeApplicationCondition)null);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING:
				setBinding((TemplateBindingCollection)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__NEGATIVE_APPLICATION_CONDITION:
				return negativeApplicationCondition != null;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING:
				return binding != null;
		}
		return super.eIsSet(featureID);
	}

} //NegativeApplicationConditionResultImpl
