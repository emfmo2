/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.modelversioning.operations.impl;

import org.eclipse.emf.compare.diff.metamodel.DiffPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.modelversioning.core.conditions.ConditionsPackage;
import org.modelversioning.core.conditions.templatebindings.TemplatebindingsPackage;
import org.modelversioning.operations.Iteration;
import org.modelversioning.operations.IterationType;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.NegativeApplicationConditionResult;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.OperationsFactory;
import org.modelversioning.operations.OperationsPackage;
import org.modelversioning.operations.SpecificationState;
import org.modelversioning.operations.UserInput;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OperationsPackageImpl extends EPackageImpl implements OperationsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iterationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userInputEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass negativeApplicationConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass negativeApplicationConditionResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum iterationTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum specificationStateEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.modelversioning.operations.OperationsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OperationsPackageImpl() {
		super(eNS_URI, OperationsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OperationsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OperationsPackage init() {
		if (isInited) return (OperationsPackage)EPackage.Registry.INSTANCE.getEPackage(OperationsPackage.eNS_URI);

		// Obtain or create and register package
		OperationsPackageImpl theOperationsPackage = (OperationsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OperationsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OperationsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		TemplatebindingsPackage.eINSTANCE.eClass();
		DiffPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theOperationsPackage.createPackageContents();

		// Initialize created meta-data
		theOperationsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOperationsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OperationsPackage.eNS_URI, theOperationsPackage);
		return theOperationsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationSpecification() {
		return operationSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationSpecification_Name() {
		return (EAttribute)operationSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationSpecification_Description() {
		return (EAttribute)operationSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationSpecification_TitleTemplate() {
		return (EAttribute)operationSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationSpecification_Version() {
		return (EAttribute)operationSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationSpecification_ModelingLanguage() {
		return (EAttribute)operationSpecificationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationSpecification_ModelFileExtension() {
		return (EAttribute)operationSpecificationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationSpecification_DiagramFileExtension() {
		return (EAttribute)operationSpecificationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationSpecification_EditorId() {
		return (EAttribute)operationSpecificationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationSpecification_IsRefactoring() {
		return (EAttribute)operationSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationSpecification_WorkingModelRoot() {
		return (EReference)operationSpecificationEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationSpecification_WorkingDiagram() {
		return (EReference)operationSpecificationEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationSpecification_OriginModelRoot() {
		return (EReference)operationSpecificationEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationSpecification_OriginDiagram() {
		return (EReference)operationSpecificationEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationSpecification_DifferenceModel() {
		return (EReference)operationSpecificationEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationSpecification_Iterations() {
		return (EReference)operationSpecificationEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationSpecification_UserInputs() {
		return (EReference)operationSpecificationEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationSpecification_Preconditions() {
		return (EReference)operationSpecificationEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationSpecification_Postconditions() {
		return (EReference)operationSpecificationEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationSpecification_NegativeApplicationConditions() {
		return (EReference)operationSpecificationEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationSpecification_State() {
		return (EAttribute)operationSpecificationEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIteration() {
		return iterationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIteration_IterationType() {
		return (EAttribute)iterationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIteration_DiffElements() {
		return (EReference)iterationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIteration_SubIterations() {
		return (EReference)iterationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIteration_Template() {
		return (EReference)iterationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUserInput() {
		return userInputEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUserInput_Name() {
		return (EAttribute)userInputEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUserInput_Feature() {
		return (EReference)userInputEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUserInput_Template() {
		return (EReference)userInputEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNegativeApplicationCondition() {
		return negativeApplicationConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNegativeApplicationCondition_Name() {
		return (EAttribute)negativeApplicationConditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNegativeApplicationCondition_Description() {
		return (EAttribute)negativeApplicationConditionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNegativeApplicationCondition_ErrorMessage() {
		return (EAttribute)negativeApplicationConditionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNegativeApplicationCondition_ModelRoot() {
		return (EReference)negativeApplicationConditionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNegativeApplicationCondition_Diagram() {
		return (EReference)negativeApplicationConditionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNegativeApplicationCondition_DifferenceModel() {
		return (EReference)negativeApplicationConditionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNegativeApplicationCondition_Conditions() {
		return (EReference)negativeApplicationConditionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNegativeApplicationCondition_OperationSpecification() {
		return (EReference)negativeApplicationConditionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNegativeApplicationConditionResult() {
		return negativeApplicationConditionResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNegativeApplicationConditionResult_NegativeApplicationCondition() {
		return (EReference)negativeApplicationConditionResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNegativeApplicationConditionResult_Binding() {
		return (EReference)negativeApplicationConditionResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getIterationType() {
		return iterationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSpecificationState() {
		return specificationStateEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationsFactory getOperationsFactory() {
		return (OperationsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		operationSpecificationEClass = createEClass(OPERATION_SPECIFICATION);
		createEAttribute(operationSpecificationEClass, OPERATION_SPECIFICATION__NAME);
		createEAttribute(operationSpecificationEClass, OPERATION_SPECIFICATION__DESCRIPTION);
		createEAttribute(operationSpecificationEClass, OPERATION_SPECIFICATION__TITLE_TEMPLATE);
		createEAttribute(operationSpecificationEClass, OPERATION_SPECIFICATION__VERSION);
		createEAttribute(operationSpecificationEClass, OPERATION_SPECIFICATION__IS_REFACTORING);
		createEAttribute(operationSpecificationEClass, OPERATION_SPECIFICATION__MODELING_LANGUAGE);
		createEAttribute(operationSpecificationEClass, OPERATION_SPECIFICATION__MODEL_FILE_EXTENSION);
		createEAttribute(operationSpecificationEClass, OPERATION_SPECIFICATION__DIAGRAM_FILE_EXTENSION);
		createEAttribute(operationSpecificationEClass, OPERATION_SPECIFICATION__EDITOR_ID);
		createEReference(operationSpecificationEClass, OPERATION_SPECIFICATION__WORKING_MODEL_ROOT);
		createEReference(operationSpecificationEClass, OPERATION_SPECIFICATION__WORKING_DIAGRAM);
		createEReference(operationSpecificationEClass, OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT);
		createEReference(operationSpecificationEClass, OPERATION_SPECIFICATION__ORIGIN_DIAGRAM);
		createEReference(operationSpecificationEClass, OPERATION_SPECIFICATION__DIFFERENCE_MODEL);
		createEReference(operationSpecificationEClass, OPERATION_SPECIFICATION__ITERATIONS);
		createEReference(operationSpecificationEClass, OPERATION_SPECIFICATION__USER_INPUTS);
		createEReference(operationSpecificationEClass, OPERATION_SPECIFICATION__PRECONDITIONS);
		createEReference(operationSpecificationEClass, OPERATION_SPECIFICATION__POSTCONDITIONS);
		createEReference(operationSpecificationEClass, OPERATION_SPECIFICATION__NEGATIVE_APPLICATION_CONDITIONS);
		createEAttribute(operationSpecificationEClass, OPERATION_SPECIFICATION__STATE);

		iterationEClass = createEClass(ITERATION);
		createEAttribute(iterationEClass, ITERATION__ITERATION_TYPE);
		createEReference(iterationEClass, ITERATION__DIFF_ELEMENTS);
		createEReference(iterationEClass, ITERATION__SUB_ITERATIONS);
		createEReference(iterationEClass, ITERATION__TEMPLATE);

		userInputEClass = createEClass(USER_INPUT);
		createEAttribute(userInputEClass, USER_INPUT__NAME);
		createEReference(userInputEClass, USER_INPUT__FEATURE);
		createEReference(userInputEClass, USER_INPUT__TEMPLATE);

		negativeApplicationConditionEClass = createEClass(NEGATIVE_APPLICATION_CONDITION);
		createEAttribute(negativeApplicationConditionEClass, NEGATIVE_APPLICATION_CONDITION__NAME);
		createEAttribute(negativeApplicationConditionEClass, NEGATIVE_APPLICATION_CONDITION__DESCRIPTION);
		createEAttribute(negativeApplicationConditionEClass, NEGATIVE_APPLICATION_CONDITION__ERROR_MESSAGE);
		createEReference(negativeApplicationConditionEClass, NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT);
		createEReference(negativeApplicationConditionEClass, NEGATIVE_APPLICATION_CONDITION__DIAGRAM);
		createEReference(negativeApplicationConditionEClass, NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL);
		createEReference(negativeApplicationConditionEClass, NEGATIVE_APPLICATION_CONDITION__CONDITIONS);
		createEReference(negativeApplicationConditionEClass, NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION);

		negativeApplicationConditionResultEClass = createEClass(NEGATIVE_APPLICATION_CONDITION_RESULT);
		createEReference(negativeApplicationConditionResultEClass, NEGATIVE_APPLICATION_CONDITION_RESULT__NEGATIVE_APPLICATION_CONDITION);
		createEReference(negativeApplicationConditionResultEClass, NEGATIVE_APPLICATION_CONDITION_RESULT__BINDING);

		// Create enums
		iterationTypeEEnum = createEEnum(ITERATION_TYPE);
		specificationStateEEnum = createEEnum(SPECIFICATION_STATE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		DiffPackage theDiffPackage = (DiffPackage)EPackage.Registry.INSTANCE.getEPackage(DiffPackage.eNS_URI);
		ConditionsPackage theConditionsPackage = (ConditionsPackage)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI);
		TemplatebindingsPackage theTemplatebindingsPackage = (TemplatebindingsPackage)EPackage.Registry.INSTANCE.getEPackage(TemplatebindingsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		negativeApplicationConditionResultEClass.getESuperTypes().add(theConditionsPackage.getEvaluationResult());

		// Initialize classes and features; add operations and parameters
		initEClass(operationSpecificationEClass, OperationSpecification.class, "OperationSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOperationSpecification_Name(), ecorePackage.getEString(), "name", null, 1, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationSpecification_Description(), ecorePackage.getEString(), "description", null, 0, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationSpecification_TitleTemplate(), ecorePackage.getEString(), "titleTemplate", null, 0, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationSpecification_Version(), ecorePackage.getEString(), "version", "1.0.0", 0, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationSpecification_IsRefactoring(), ecorePackage.getEBoolean(), "isRefactoring", null, 0, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationSpecification_ModelingLanguage(), ecorePackage.getEString(), "modelingLanguage", null, 0, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationSpecification_ModelFileExtension(), ecorePackage.getEString(), "modelFileExtension", null, 1, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationSpecification_DiagramFileExtension(), ecorePackage.getEString(), "diagramFileExtension", null, 0, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationSpecification_EditorId(), ecorePackage.getEString(), "editorId", null, 1, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationSpecification_WorkingModelRoot(), theEcorePackage.getEObject(), null, "workingModelRoot", null, 0, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationSpecification_WorkingDiagram(), theEcorePackage.getEObject(), null, "workingDiagram", null, 0, -1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationSpecification_OriginModelRoot(), theEcorePackage.getEObject(), null, "originModelRoot", null, 1, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationSpecification_OriginDiagram(), theEcorePackage.getEObject(), null, "originDiagram", null, 0, -1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationSpecification_DifferenceModel(), theDiffPackage.getComparisonResourceSnapshot(), null, "differenceModel", null, 1, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationSpecification_Iterations(), this.getIteration(), null, "iterations", null, 0, -1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationSpecification_UserInputs(), this.getUserInput(), null, "userInputs", null, 0, -1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationSpecification_Preconditions(), theConditionsPackage.getConditionsModel(), null, "preconditions", null, 1, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationSpecification_Postconditions(), theConditionsPackage.getConditionsModel(), null, "postconditions", null, 0, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationSpecification_NegativeApplicationConditions(), this.getNegativeApplicationCondition(), this.getNegativeApplicationCondition_OperationSpecification(), "negativeApplicationConditions", null, 0, -1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationSpecification_State(), this.getSpecificationState(), "state", "0", 0, 1, OperationSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iterationEClass, Iteration.class, "Iteration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIteration_IterationType(), this.getIterationType(), "iterationType", null, 0, 1, Iteration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIteration_DiffElements(), theDiffPackage.getDiffElement(), null, "diffElements", null, 1, -1, Iteration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIteration_SubIterations(), this.getIteration(), null, "subIterations", null, 0, -1, Iteration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIteration_Template(), theConditionsPackage.getTemplate(), null, "template", null, 1, 1, Iteration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(userInputEClass, UserInput.class, "UserInput", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUserInput_Name(), theEcorePackage.getEString(), "name", null, 0, 1, UserInput.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUserInput_Feature(), theEcorePackage.getEStructuralFeature(), null, "feature", null, 1, 1, UserInput.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUserInput_Template(), theConditionsPackage.getTemplate(), null, "template", null, 0, 1, UserInput.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(negativeApplicationConditionEClass, NegativeApplicationCondition.class, "NegativeApplicationCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNegativeApplicationCondition_Name(), ecorePackage.getEString(), "name", null, 1, 1, NegativeApplicationCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNegativeApplicationCondition_Description(), ecorePackage.getEString(), "description", null, 0, 1, NegativeApplicationCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNegativeApplicationCondition_ErrorMessage(), ecorePackage.getEString(), "errorMessage", null, 0, 1, NegativeApplicationCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNegativeApplicationCondition_ModelRoot(), theEcorePackage.getEObject(), null, "modelRoot", null, 1, 1, NegativeApplicationCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNegativeApplicationCondition_Diagram(), theEcorePackage.getEObject(), null, "diagram", null, 0, -1, NegativeApplicationCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNegativeApplicationCondition_DifferenceModel(), theDiffPackage.getComparisonResourceSnapshot(), null, "differenceModel", null, 1, 1, NegativeApplicationCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNegativeApplicationCondition_Conditions(), theConditionsPackage.getConditionsModel(), null, "conditions", null, 1, 1, NegativeApplicationCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNegativeApplicationCondition_OperationSpecification(), this.getOperationSpecification(), this.getOperationSpecification_NegativeApplicationConditions(), "operationSpecification", null, 1, 1, NegativeApplicationCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(negativeApplicationConditionResultEClass, NegativeApplicationConditionResult.class, "NegativeApplicationConditionResult", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNegativeApplicationConditionResult_NegativeApplicationCondition(), this.getNegativeApplicationCondition(), null, "negativeApplicationCondition", null, 1, 1, NegativeApplicationConditionResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNegativeApplicationConditionResult_Binding(), theTemplatebindingsPackage.getTemplateBindingCollection(), null, "binding", null, 0, 1, NegativeApplicationConditionResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(iterationTypeEEnum, IterationType.class, "IterationType");
		addEEnumLiteral(iterationTypeEEnum, IterationType.FOR_ALL);
		addEEnumLiteral(iterationTypeEEnum, IterationType.FOR_SOME);

		initEEnum(specificationStateEEnum, SpecificationState.class, "SpecificationState");
		addEEnumLiteral(specificationStateEEnum, SpecificationState.INITIAL);
		addEEnumLiteral(specificationStateEEnum, SpecificationState.CONFIGURATION);

		// Create resource
		createResource(eNS_URI);
	}

} //OperationsPackageImpl
