package org.modelversioning.operations.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.operations.OperationSpecification;

/**
 * {@link OperationSpecification} printer to support debugging and so on.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationModelToTextGenerator {

	private static final String H1_PREFIX = "======= ";
	private static final String H1_POSTFIX = " =======";

	private static final String H2_PREFIX = "====== ";
	private static final String H2_POSTFIX = " ======";

	private static final String NEW_LINE = "\n";

	public void generateText(OperationSpecification operation, OutputStream out)
			throws IOException {
		// create writer
		OutputStreamWriter writer = new OutputStreamWriter(out);

		// output general data
		writer.write(H1_PREFIX);
		writer.write(operation.getName());
		writer.write(H1_POSTFIX);
		writer.write(NEW_LINE);
		writer.write(NEW_LINE);

		// output differences
		writer.write(H2_PREFIX);
		writer.write("Differences");
		writer.write(H2_POSTFIX);
		writer.write(NEW_LINE);
		TreeIterator<EObject> diffIter = operation.getDifferenceModel()
				.getDiff().eAllContents();
		while (diffIter.hasNext()) {
			EObject currentDiff = diffIter.next();
			writer.write("-" + currentDiff.toString());
			writer.write(NEW_LINE);
		}
		writer.write(NEW_LINE);

		// output pre conditions
		writer.write(H2_PREFIX);
		writer.write("Pre conditions");
		writer.write(H2_POSTFIX);
		writer.write(NEW_LINE);
		// generateText(operation.getPreconditions(), writer);

		// output differences
		writer.write(NEW_LINE);
		writer.write(H2_PREFIX);
		writer.write("Post conditions");
		writer.write(H2_POSTFIX);
		writer.write(NEW_LINE);
		// generateText(operation.getPostconditions(), writer);

		writer.flush();
	}

//	private void generateText(ConditionsModel conditionsModel,
//			OutputStreamWriter writer) throws IOException {
//		TreeIterator<EObject> postConditionIter = conditionsModel
//				.eAllContents();
//		while (postConditionIter.hasNext()) {
//			EObject currentObject = postConditionIter.next();
//			// if (currentObject instanceof Symbol) {
//			// Symbol symbol = (Symbol) currentObject;
//			// writer.write(NEW_LINE);
//			// writer.write(H3_PREFIX);
//			// writer.write(symbol.getTitle() + ": " + symbol.getName());
//			// writer.write(H3_POSTFIX);
//			// writer.write(NEW_LINE);
//			// writer.write(NEW_LINE);
//			// for (Condition condition : symbol.getSpecifications()) {
//			// if (condition instanceof FeatureCondition) {
//			// FeatureCondition featureCondition = (FeatureCondition) condition;
//			// String activityString = "";
//			// if (featureCondition.isActive()) {
//			// activityString = ACTIVE_PREFIX;
//			// } else {
//			// activityString = INACTIVE_PREFIX;
//			// }
//			// for (String oclString : featureCondition.getExpressions()) {
//			// writer.write(BULLET + activityString +
//			// featureCondition.getFeature().getName() + oclString);
//			// writer.write(NEW_LINE);
//			// }
//			// }
//			// }
//			// }
//		}
//	}

}
